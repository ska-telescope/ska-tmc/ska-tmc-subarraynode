ska\_tmc\_subarraynode package
==============================

Subpackages
===========

.. toctree::
   :maxdepth: 4

   ska_tmc_subarraynode.commands
   ska_tmc_subarraynode.manager
   ska_tmc_subarraynode.model

Submodules
-----------

*subarraynode\_node*
---------------------
**Path:** *src/ska\_tmc\_subarraynode/subarray\_node.py*

.. automodule:: ska_tmc_subarraynode.subarray_node
   :members:
   :undoc-members:
   :show-inheritance:

*subarraynode\_node\_low*
-------------------------
**Path:** *src/ska\_tmc\_subarraynode/subarray\_node\_low.py*

.. automodule:: ska_tmc_subarraynode.subarray_node_low
   :members:
   :undoc-members:
   :show-inheritance:

*subarraynode\_node\_mid*
-------------------------
**Path:** *src/ska\_tmc\_subarraynode/subarray\_node\_mid.py*

.. automodule:: ska_tmc_subarraynode.subarray_node_mid
   :members:
   :undoc-members:
   :show-inheritance:

*exceptions*
------------
**Path:** *src/ska\_tmc\_subarraynode/exceptions.py*


.. automodule:: ska_tmc_subarraynode.exceptions
   :members:
   :undoc-members:
   :show-inheritance:

*release*
---------
**Path:** *src/ska\_tmc\_subarraynode/release.py*

.. automodule:: ska_tmc_subarraynode.release
   :members:
   :undoc-members:
   :show-inheritance:

*transaction\_id*
-----------------
**Path:** *src/ska\_tmc\_subarraynode/transaction\_id.py*

.. automodule:: ska_tmc_subarraynode.transaction_id
   :members:
   :undoc-members:
   :show-inheritance:
   

Module contents
---------------

.. automodule:: ska_tmc_subarraynode
   :members:
   :undoc-members:
   :show-inheritance:


###########################
Properties in Subarray Node
###########################


+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| Property Name                     | Data Type       | Description                                                                    |
+===================================+=================+================================================================================+
| CspSubarrayLNFQDN                 | DevString       | FQDN of the CSP Subarray Leaf Node device                                      |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| SdpSubarrayLNFQDN                 | DevString       | FQDN of the SDP Subarray Leaf Node device                                      |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| CspSubarrayFQDN                   | DevString       | FQDN of the CSP Subarray device                                                |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| SdpSubarrayFQDN                   | DevString       | FQDN of the SDP Subarray device                                                |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| AbortCommandTimeOut               | DevUShort       | Timeout for the Subarray ABORTED obsState transition. Once                     |
|                                   |                 | the AbortCommandTimeOut exceeds, SubarrayNode transitions to obsState FAULT.   |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| CspScanInterfaceURL               | DevString       | Interface URL of the CSP sub-system for Scan command                           |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| SdpScanInterfaceURL               | DevString       | Interface URL of the SDP sub-system for Scan command                           |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| CspAssignResourcesInterfaceURL    | DevString       | Interface URL of the CSP sub-system for AssignResources command                |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| CommandTimeOut                    | DevUShort       | Timeout for the command execution                                              |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| LivelinessCheckPeriod             | DevFloat        | Period for the liveliness probe to monitor each device in a loop               |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+
| EventSubscriptionCheckPeriod      | DevFloat        | Period for the event subscriber to check the device subscriptions in a loop    |
+-----------------------------------+-----------------+--------------------------------------------------------------------------------+


##########################################
Additional Properties in Subarray Node Mid
##########################################


+-------------------------------------+----------------+--------------------------------------------------------------------------------+
| Property Name                       | Data Type      | Description                                                                    |
+=====================================+================+================================================================================+
| DishLeafNodePrefix                  | DevString      | Device name prefix for Dish Leaf Node. This property is for internal use.      |
+-------------------------------------+----------------+--------------------------------------------------------------------------------+
| DishMasterIdentifier                | DevString      | Device name tag/identifier for Dish Master device. This property is for        |
|                                     |                | internal use.                                                                  |
+-------------------------------------+----------------+--------------------------------------------------------------------------------+
| DishMasterFQDNs                     | DevStringArray | List of Dish Master devices. This property derived from the values of          |
|                                     |                | properties DishIDs and DishMasterIdentifier. It is for internal use.           |
+-------------------------------------+----------------+--------------------------------------------------------------------------------+

##########################################
Additional Properties in Subarray Node Low
##########################################


+-------------------------------+---------------+--------------------------------------------------------------------------------+
| Property Name                 | Data Type     | Description                                                                    |
+===============================+===============+================================================================================+
| MccsSubarrayLNFQDN            | DevString     | FQDN of the MCCS Subarray Leaf Node Tango Device Server                        |
+-------------------------------+---------------+--------------------------------------------------------------------------------+
| MccsSubarrayFQDN              | DevString     | FQDN of the MCCS Subarray device                                               |
+-------------------------------+---------------+--------------------------------------------------------------------------------+
| MccsConfigureInterfaceURL     | DevString     | Interface URL of the MCCS sub-system for Configure command                     |
+-------------------------------+---------------+--------------------------------------------------------------------------------+
| MccsScanInterfaceURL          | DevString     | Interface URL of the MCCS sub-system for Scan command                          |
+-------------------------------+---------------+--------------------------------------------------------------------------------+
| JonesURI                      | DevString     | URI for Jones Matrix                                                           |
+-------------------------------+---------------+--------------------------------------------------------------------------------+