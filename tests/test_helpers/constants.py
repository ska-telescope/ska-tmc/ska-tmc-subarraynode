"""
Define all constants required for testing
"""
MID_SUBARRAY_NODE_NAME = "mid-tmc/subarray/01"
LOW_SUBARRAY_NODE_NAME = "low-tmc/subarray/01"

MID_CSPSUBARRAY_LEAF_NODE = "mid-tmc/subarray-leaf-node-csp/01"
MID_SDPSUBARRAY_LEAF_NODE = "mid-tmc/subarray-leaf-node-sdp/01"

LOW_CSPSUBARRAY_LEAF_NODE = "low-tmc/subarray-leaf-node-csp/01"
LOW_SDPSUBARRAY_LEAF_NODE = "low-tmc/subarray-leaf-node-sdp/01"
DISH_MASTER_DEVICE = "ska001/elt/master"
DISH_LEAF_NODE = "mid-tmc/leaf-node-dish/ska001"

DishLeafNodePrefix = "mid-tmc/leaf-node-dish/ska"
DISH_LEAF_NODE36 = "mid-tmc/leaf-node-dish/ska036"
DISH_LEAF_NODE100 = "mid-tmc/leaf-node-dish/ska100"
DISH_LEAF_NODE63 = "mid-tmc/leaf-node-dish/ska063"

DISH_LEAF_NODE2 = "mid-tmc/leaf-node-dish/ska002"

DISH_MASTER_DEVICE1 = "ska001/elt/master"
DISH_MASTER_DEVICE2 = "ska002/elt/master"
DISH_MASTER_DEVICE36 = "ska036/elt/master"
DISH_MASTER_DEVICE63 = "ska063/elt/master"
DISH_MASTER_DEVICE100 = "ska100/elt/master"

LOW_CSPSUBARRAY_NODE_NAME = "low-csp/subarray/01"
LOW_SDPSUBARRAY_NODE_NAME = "low-sdp/subarray/01"

MID_CSPSUBARRAY_NODE_NAME = "mid-csp/subarray/01"
MID_SDPSUBARRAY_NODE_NAME = "mid-sdp/subarray/01"

MCCSSUBARRAY_NODE_NAME = "low-mccs/subarray/01"
MCCS_SUBARRAY_LEAF_NODE = "low-tmc/subarray-leaf-node-mccs/01"

SDPQC_FQDN = "mid-sdp/queueconnector/01"

TMC_MID_DOMAIN = "mid-tmc"

TMC_LOW_DOMAIN = "low-tmc"
