"""settings module for Subarray  Node."""
import json
import logging
import time
from datetime import datetime
from typing import Any, List, Tuple, Union

import mock
import pytest
from ska_control_model import ObsState, ObsStateModel
from ska_tango_base.base.base_device import CommandTracker
from ska_tango_base.commands import ResultCode
from ska_tango_testing.mock.placeholders import Anything
from ska_tmc_common import (
    DevFactory,
    DishMode,
    FaultType,
    PointingState,
    TMCOpStateModel,
)
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DeviceProxy

from ska_tmc_subarraynode.manager.subarray_node_component_manager_low import (
    SubarrayNodeComponentManagerLow,
)
from ska_tmc_subarraynode.manager.subarray_node_component_manager_mid import (
    SubarrayNodeComponentManagerMid,
)
from ska_tmc_subarraynode.model.input import (
    InputParameterLow,
    InputParameterMid,
)
from src.ska_tmc_subarraynode.commands.subarray_node_command import LOGGER
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE63,
    DISH_LEAF_NODE100,
    DISH_MASTER_DEVICE,
    DISH_MASTER_DEVICE1,
    DISH_MASTER_DEVICE36,
    DISH_MASTER_DEVICE63,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_CSPSUBARRAY_NODE_NAME,
    LOW_SDPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_NODE_NAME,
    MCCS_SUBARRAY_LEAF_NODE,
    MCCSSUBARRAY_NODE_NAME,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_NODE_NAME,
)
from tests.test_helpers.event_recorder import EventRecorder

logger = logging.getLogger(__name__)

SLEEP_TIME = 0.5
TIMEOUT = 100
expected_json_dish_100 = {
    "interface": "https://schema.skao.int/ska-tmc-configure/4.1",
    "pointing": {
        "field": {
            "target_name": "Cen-A",
            "reference_frame": "icrs",
            "attrs": {"c1": 201.365, "c2": -43.0191667},
        }
    },
    "dish": {"receiver_band": "1"},
    "transaction_id": "txn-....-00001",
}

expected_json_dish_001_036 = {
    "field": {
        "target_name": "Cen-A",
        "reference_frame": "icrs",
        "attrs": {"c1": 201.365, "c2": -43.0191667},
    },
    "trajectory": {
        "name": "mosaic",
        "attrs": {
            "x_offsets": [-5.0, 0.0, 5.0, -5.0, 0.0, 5.0, -5.0, 0.0, 5.0],
            "y_offsets": [5.0, 5.0, 5.0, 0.0, 0.0, 0.0, -5.0, -5.0, -5.0],
        },
    },
    "projection": {"name": "SSN", "alignment": "ICRS"},
}

DEVICE_LIST_MID = [
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    DISH_LEAF_NODE,
    DISH_LEAF_NODE100,
    DISH_LEAF_NODE36,
    MID_CSPSUBARRAY_NODE_NAME,
    MID_SDPSUBARRAY_NODE_NAME,
    DISH_MASTER_DEVICE,
    DISH_MASTER_DEVICE36,
]
DISH_LEAF_NODE_LIST = [
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE63,
    DISH_LEAF_NODE100,
]

DEVICE_LIST_MID_MULTIPLE_DISH = DEVICE_LIST_MID + [
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE63,
    DISH_MASTER_DEVICE36,
    DISH_MASTER_DEVICE63,
]

DEVICE_LIST_LOW = [
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MCCS_SUBARRAY_LEAF_NODE,
    LOW_CSPSUBARRAY_NODE_NAME,
    LOW_SDPSUBARRAY_NODE_NAME,
    MCCSSUBARRAY_NODE_NAME,
]

SUBARRAY_LEAF_NODES_MID = [
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
]

SUBARRAY_LEAF_NODES_LOW = [
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MCCS_SUBARRAY_LEAF_NODE,
]

TIMEOUT_DEFECT = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.STUCK_IN_INTERMEDIATE_STATE,
        "error_message": "Device stuck in intermediate state",
        "result": ResultCode.FAILED,
        "intermediate_state": ObsState.RESOURCING,
    }
)

TIMEOUT_DEFECT_DISH = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.STUCK_IN_INTERMEDIATE_STATE,
        "error_message": "Device stuck in intermediate state",
        "result": ResultCode.FAILED,
        "intermediate_state": PointingState.READY,
    }
)

FAILED_RESULT_DEFECT = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.FAILED_RESULT,
        "error_message": "Device stuck cannot process command",
        "result": ResultCode.FAILED,
    }
)

ERROR_PROPAGATION_DEFECT = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.LONG_RUNNING_EXCEPTION,
        "error_message": "Exception occurred, command failed.",
        "result": ResultCode.FAILED,
    }
)

RESET_DEFECT = json.dumps(
    {
        "enabled": False,
        "fault_type": FaultType.FAILED_RESULT,
        "error_message": "Default exception.",
        "result": ResultCode.FAILED,
    }
)

EXCEPTION_MID_SDP = (
    "Exception occurred on the following devices: "
    + f"{MID_SDPSUBARRAY_LEAF_NODE}: Exception occurred, command failed.\n"
)
EXCEPTION_MID_CSP = (
    "Exception occurred on the following devices: "
    + f"{MID_CSPSUBARRAY_LEAF_NODE}: Exception occurred, command failed.\n"
)
EXCEPTION_DISH = (
    "Exception occurred on the following devices: "
    + f"{DISH_LEAF_NODE}: Exception occurred, command failed.\n"
)
EXCEPTION_LOW_SDP = (
    "Exception occurred on the following devices: "
    + f"{LOW_SDPSUBARRAY_LEAF_NODE}: Exception occurred, command failed.\n"
)
EXCEPTION_LOW_CSP = (
    "Exception occurred on the following devices: "
    + f"{LOW_CSPSUBARRAY_LEAF_NODE}: Exception occurred, command failed.\n"
)
EXCEPTION_MCCS = (
    "Exception occurred on the following devices: "
    + f"{MCCS_SUBARRAY_LEAF_NODE}: Exception occurred, command failed.\n"
)
EXCEPTION_MID_CSP_SDP = (
    "Exception occurred on the following devices: "
    + f"{MID_CSPSUBARRAY_LEAF_NODE}: "
    f"Exception occurred, command failed.\n{MID_SDPSUBARRAY_LEAF_NODE}: "
    "Exception occurred, command failed.\n"
)
EXCEPTION_ALL_DISH = (
    "Exception occurred on the following devices: " + f"{DISH_LEAF_NODE}: "
    f"Exception occurred, command failed.\n{DISH_LEAF_NODE36}: "
    f"Exception occurred, command failed.\n{DISH_LEAF_NODE100}: "
    "Exception occurred, command failed.\n"
)
EXCEPTION_LOW_CSP_SDP = (
    "Exception occurred on the following devices: "
    + f"{LOW_CSPSUBARRAY_LEAF_NODE}: "
    f"Exception occurred, command failed.\n{LOW_SDPSUBARRAY_LEAF_NODE}: "
    "Exception occurred, command failed.\n"
)
TIMEOUT_EXCEPTION = "Timeout has occurred, command failed"

FAILED_RESULT_EXCEPTION = "Device stuck cannot process command"

obs_state_model = ObsStateModel(logger=logger)


def count_faulty_devices(cm):
    """Method for counting faulty devices."""
    result = 0
    logger.debug("cm.checked_devices: %s", cm.checked_devices)
    for dev_info in cm.checked_devices:
        if dev_info.unresponsive:
            result += 1
    return result


def set_device_availability(cm, device_name, value):
    """Method to set device_availability value,
    depending on the input devices."""
    for dev_info in cm.checked_devices:
        logger.info("dev_info %s: {dev_info}")
        if dev_info.dev_name == device_name:
            dev_info.device_availability = value
            logger.info(dev_info.device_availability)


def create_cm_no_faulty_devices(
    tango_context,
    liveliness_probe,
    event_receiver,
    input_parameter=InputParameterMid(None),
):
    """Method for create component manager with no faulty devices"""
    logger.info("%s", tango_context)

    if isinstance(input_parameter, InputParameterMid):
        cm, start_time, obs_state_model = create_mid_cm(
            liveliness_probe, event_receiver, input_parameter
        )
    else:
        cm, start_time, obs_state_model = create_low_cm(
            liveliness_probe, event_receiver, input_parameter
        )

    num_faulty = count_faulty_devices(cm)

    assert num_faulty == 0
    elapsed_time = time.time() - start_time
    logger.info("checked %s devices in %s", num_faulty, elapsed_time)
    cm.__del__()
    return cm


def component_state_change_callback(*args):
    """Dummy callback method for testing"""


def set_devices_unresponsive(cm, device_names: list):
    """Sets devices unresponsive

    Args:
        cm: component manager instance
        device_names (list): devices names to be
        set as unresponsive
    """
    for device_name in device_names:
        dev_info = cm.get_device(device_name)
        dev_info.update_unresponsive(True, "Faulty")


def create_low_cm(
    liveliness_probe=True,
    event_receiver=True,
    input_parameter=InputParameterLow(None),
):
    """Method for creating component manager instance for low device"""
    op_state_model = TMCOpStateModel(logger)
    obs_state_model = ObsStateModel(logger=logger)
    _command_tracker = CommandTracker(
        queue_changed_callback=None,
        status_changed_callback=None,
        progress_changed_callback=None,
        result_callback=None,
    )
    cm = SubarrayNodeComponentManagerLow(
        op_state_model,
        obs_state_model,
        _command_tracker,
        logger=logger,
        _input_parameter=input_parameter,
        dev_family="leaf-node",
        _liveliness_probe=liveliness_probe,
        _event_receiver=event_receiver,
        component_state_changed_callback=component_state_change_callback,
        # command timeout occuring with 500ms, so increasing it to 1000ms
        proxy_timeout=1000,
    )

    DEVICE_LIST = DEVICE_LIST_LOW
    cm.input_parameter.tmc_mccs_sln_device_name = MCCS_SUBARRAY_LEAF_NODE
    cm.input_parameter.mccs_subarray_dev_name = MCCSSUBARRAY_NODE_NAME
    cm.input_parameter.tmc_csp_sln_device_name = LOW_CSPSUBARRAY_LEAF_NODE
    cm.input_parameter.tmc_sdp_sln_device_name = LOW_SDPSUBARRAY_LEAF_NODE
    cm.input_parameter.csp_subarray_dev_name = LOW_CSPSUBARRAY_NODE_NAME
    cm.input_parameter.sdp_subarray_dev_name = LOW_SDPSUBARRAY_NODE_NAME

    start_time = time.time()

    for dev in DEVICE_LIST:
        cm.add_device_to_lp(dev)
    if not liveliness_probe:
        time.sleep(0.5)
        return cm, start_time, obs_state_model
    num_devices = len(DEVICE_LIST)
    while num_devices != len(cm.checked_devices):
        time.sleep(SLEEP_TIME)
        elapsed_time = time.time() - start_time
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    time.sleep(0.5)
    cm.__del__()
    return cm, start_time, obs_state_model


def create_mid_cm(
    liveliness_probe=True,
    event_receiver=True,
    input_parameter=InputParameterMid(None),
    component_state_changed_callback=component_state_change_callback,
    multiple_dish=False,
):
    """Method for creating component manager instance for mid device"""
    op_state_model = TMCOpStateModel(logger)
    obs_state_model = ObsStateModel(logger=logger)
    _command_tracker = CommandTracker(
        queue_changed_callback=None,
        status_changed_callback=None,
        progress_changed_callback=None,
        result_callback=None,
    )
    cm = SubarrayNodeComponentManagerMid(
        op_state_model,
        obs_state_model,
        _command_tracker,
        dev_family="leaf-node",
        logger=logger,
        _input_parameter=input_parameter,
        _liveliness_probe=liveliness_probe,
        _event_receiver=event_receiver,
        component_state_changed_callback=component_state_changed_callback,
    )
    if multiple_dish:
        DEVICE_LIST = DEVICE_LIST_MID_MULTIPLE_DISH
        cm.input_parameter.dish_dev_names = [
            DISH_MASTER_DEVICE,
            DISH_MASTER_DEVICE36,
            DISH_MASTER_DEVICE63,
        ]
        cm.input_parameter.tmc_dish_ln_device_names = [
            DISH_LEAF_NODE,
            DISH_LEAF_NODE36,
            DISH_LEAF_NODE63,
        ]

    else:
        DEVICE_LIST = DEVICE_LIST_MID
        # In unit tests, it is considered that
        # Dish1 is assigned to SubarrayNode1 and
        # tested the SubarrayNode-Dish interface.
        cm.input_parameter.dish_dev_names = [DISH_MASTER_DEVICE]
        cm.input_parameter.tmc_dish_ln_device_names = [DISH_LEAF_NODE]
    cm.input_parameter.tmc_csp_sln_device_name = MID_CSPSUBARRAY_LEAF_NODE
    cm.input_parameter.tmc_sdp_sln_device_name = MID_SDPSUBARRAY_LEAF_NODE
    cm.input_parameter.csp_subarray_dev_name = MID_CSPSUBARRAY_NODE_NAME
    cm.input_parameter.sdp_subarray_dev_name = MID_SDPSUBARRAY_NODE_NAME

    start_time = time.time()
    num_devices = len(DEVICE_LIST)
    if not liveliness_probe:
        time.sleep(0.5)
        return cm, start_time, obs_state_model
    for dev in DEVICE_LIST:
        cm.add_device_to_lp(dev)

    while num_devices != len(cm.checked_devices):
        time.sleep(SLEEP_TIME)
        elapsed_time = time.time() - start_time
        if elapsed_time > TIMEOUT:
            checked_devices = [
                device.dev_name for device in cm.checked_devices
            ]
            logger.info(
                "Checked devices are %s, number expected: %s",
                checked_devices,
                num_devices,
            )
            pytest.fail("Timeout occurred while executing the test")
    time.sleep(0.5)
    cm.__del__()
    return cm, start_time, obs_state_model


def simulate_obs_state_events(
    cm: Union[
        SubarrayNodeComponentManagerLow, SubarrayNodeComponentManagerMid
    ],
    devices: List[str],
    obs_states: List[ObsState],
) -> None:
    """Simulate obsState events from given devices for given obsStates"""
    for obs_state in obs_states:
        for device in devices:
            cm.update_device_obs_state(device, obs_state, datetime.now())
        time.sleep(2)


def simulate_obs_state_and_result_code_events(
    cm: Union[
        SubarrayNodeComponentManagerLow, SubarrayNodeComponentManagerMid
    ],
    devices: List[str],
    obs_states: List[ObsState],
    command_name: str,
    result: ResultCode,
):
    """Simulate obsState and LRCR events from given devices for given
    obsStates and result.
    """
    for device in devices:
        for obs_state in obs_states:
            cm.update_device_obs_state(device, obs_state, datetime.now())
            time.sleep(0.5)
        try:
            unique_id = cm.event_data_manager.event_info.command_result_data[
                device
            ].unique_id[0]

        except KeyError:
            unique_id = f"{time.time()}-{command_name}"
        LOGGER.info("Updating for unique_id %s", unique_id)
        cm.update_long_running_command_result(
            device,
            (
                unique_id,
                json.dumps([str(int(result)), ""]),
            ),
            datetime.now(),
        )


def simulate_result_code_events(
    cm: Union[
        SubarrayNodeComponentManagerLow, SubarrayNodeComponentManagerMid
    ],
    devices: List[str],
    result: Tuple[str, str],
) -> None:
    """Simulate LRCR events for the given devices with given result."""
    for device in devices:
        cm.update_long_running_command_result(device, result, datetime.now())


def set_sdp_csp_leaf_node_availability_for_aggregation(
    cm,
    value: bool = True,
    input_parameter: Union[
        InputParameterLow, InputParameterMid
    ] = InputParameterMid(None),
):
    """
    Setting low Csp subarray leaf node and Sdp subarray leaf node availabilty
    attribute isSubsystemAvailable as True for aggregation
    """
    if isinstance(input_parameter, InputParameterMid):
        cm.update_subarray_availability_status(
            MID_CSPSUBARRAY_LEAF_NODE, value
        )
        cm.update_subarray_availability_status(
            MID_SDPSUBARRAY_LEAF_NODE, value
        )
    else:
        cm.update_subarray_availability_status(
            LOW_CSPSUBARRAY_LEAF_NODE, value
        )
        cm.update_subarray_availability_status(
            LOW_SDPSUBARRAY_LEAF_NODE, value
        )

    start_time = time.time()
    elapsed_time = 0
    while cm.component.subarray_availability != value:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")


def set_subsystem_availability(devices: list, availability: bool = True):
    """Sets subsystem availability to given value. Default is True."""
    for device in devices:
        device_proxy = DeviceProxy(device)
        device_proxy.SetSubsystemAvailable(availability)
        logger.info("%s device availability is set to True", device)


def set_leaf_nodes_obs_state(devices: list, obs_state: ObsState):
    """Set the leaf nodes obsState to given ObsState"""
    dev_factory = DevFactory()
    for device in devices:
        if "csp" in device:
            proxy = dev_factory.get_device(device)
            proxy.SetCspSubarrayLeafNodeObsState(obs_state)
        elif "sdp" in device:
            proxy = dev_factory.get_device(device)
            proxy.SetSdpSubarrayLeafNodeObsState(obs_state)
        else:
            proxy = dev_factory.get_device(device)
            proxy.SetDirectObsState(obs_state)


def simulate_configure_on_dish_device(component_manager, device_list):
    for device in device_list:
        component_manager.update_device_dish_mode(
            device, DishMode.OPERATE, datetime.now()
        )
        component_manager.update_device_pointing_state(
            device, PointingState.TRACK, datetime.now()
        )

        logger.info("Device: %s", device)
        logger.info(
            "command result data: %s",
            component_manager.event_data_manager.event_info.command_result_data,
        )
        try:
            unique_id = component_manager.event_data_manager.event_info.command_result_data[
                device
            ].unique_id[
                0
            ]
        except KeyError:
            unique_id = f"{time.time()}-Configure"
        component_manager.update_long_running_command_result(
            device,
            (
                unique_id,
                json.dumps([str(int(ResultCode.OK)), ""]),
            ),
            datetime.now(),
        )


def simulate_end_on_dish_device(component_manager, device_list):
    for device in device_list:
        component_manager.update_device_pointing_state(
            device, PointingState.READY, datetime.now()
        )

        logger.info("Device: %s", device)
        logger.info(
            "command result data: %s",
            component_manager.event_data_manager.event_info.command_result_data,
        )
        try:
            unique_id = component_manager.event_data_manager.event_info.command_result_data[
                device
            ].unique_id[
                0
            ]
        except KeyError:
            unique_id = f"{time.time()}-TrackStop"
        component_manager.update_long_running_command_result(
            device,
            (
                unique_id,
                json.dumps([str(int(ResultCode.OK)), ""]),
            ),
            datetime.now(),
        )


def simulate_configure_on_dish_master():
    """Simulate the configure command flow on Dish Master device."""
    dish_master = DevFactory().get_device(DISH_MASTER_DEVICE1)
    current_dish_mode = dish_master.dishMode

    # Setting the DishMode to CONFIG
    dish_master.SetDirectDishMode(DishMode.CONFIG)
    # Setting the DishMode back to its original value.
    dish_master.SetDirectDishMode(current_dish_mode)
    # If the DishMode is not OPERATE, setting it to OPERATE
    if current_dish_mode != DishMode.OPERATE:
        dish_master.SetDirectDishMode(DishMode.OPERATE)

    # Setting the Pointing State
    dish_master.SetDirectPointingState(PointingState.TRACK)


def check_lrcr_events(
    event_recorder: EventRecorder,
    device: DeviceProxy,
    command_name: str,
    result_code: ResultCode = ResultCode.OK,
    retries: int = 10,
):
    """Used to assert command name and result code in
       longRunningCommandResult event callbacks.

    Args:
        event_recorder (EventRecorder):fixture used to
        capture event callbacks
        device (str): device for which attribute needs to be checked
        command_name (str): command name to check
        result_code (ResultCode): result_code to check.
        Defaults to ResultCode.OK.
        retries (int):number of events to check. Defaults to 10.
    """
    COUNT = 0
    while COUNT <= retries:
        assertion_data = event_recorder.has_change_event_occurred(
            device, "longRunningCommandResult", Anything, lookahead=1
        )
        unique_id, result = assertion_data["attribute_value"]
        if unique_id.endswith(command_name):
            if result == str(result_code.value):
                logger.debug("TRACKLOADSTATICOFF_UID: %s", unique_id)
                break
        COUNT = COUNT + 1
        if COUNT >= retries:
            pytest.fail("Assertion Failed")


def wait_for_attribute_to_change_to(
    device: DeviceProxy, attribute_name: str, attribute_value: Any
) -> None:
    """Wait for the attribute to change to given value.

    :param attribute_name: Attribute name as a string
    :param attribute_value: Value of attribute to be asserted
    """
    start_time = time.time()
    elapsed_time = time.time() - start_time
    current_value = device.read_attribute(attribute_name).value
    while current_value != attribute_value:
        elapsed_time = time.time() - start_time
        if elapsed_time >= TIMEOUT:
            raise AssertionError(
                "Attribute value is not equal to given value. "
                + f"Current value: {current_value}, expected value: "
                + f"{attribute_value}"
            )
        current_value = device.read_attribute(attribute_name).value
        time.sleep(1)


def get_input_json_from_command_call_info(
    subarray_node, command_name: str
) -> dict:
    """Method to get json using CommandCallInfo attribute

    Args:
        subarray_node (fixture): fixture for SubarrayNode class
        command_name (str): Tango device command name

    Returns:
        dict: Returns commandcallInfo attribute values in json
    """
    start_time = time.time()
    elapsed_time = 0
    input_json = subarray_node.csp_subarray_leaf_node.commandCallInfo[-1][-1]
    command = subarray_node.csp_subarray_leaf_node.commandCallInfo[-1][-2]
    while not input_json or command != command_name:
        input_json = subarray_node.csp_subarray_leaf_node.commandCallInfo[-1][
            -1
        ]
        command = subarray_node.csp_subarray_leaf_node.commandCallInfo[-1][-2]
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    return input_json


def set_k_values(k_values: list):
    devices = [DISH_LEAF_NODE, DISH_LEAF_NODE36, DISH_LEAF_NODE63]

    for index, device_name in enumerate(devices):
        proxy = DeviceProxy(device_name)
        k_value = k_values[index] if index < len(k_values) else None
        if k_value is not None:
            try:
                proxy.SetKValue(k_value)
            except Exception as e:
                logger.exception(f"Unable to set the kValues :{e}")


def check_k_values_set(k_values: list) -> bool:
    devices = [DISH_LEAF_NODE, DISH_LEAF_NODE36, DISH_LEAF_NODE63]

    for index, device_name in enumerate(devices):
        proxy = DeviceProxy(device_name)
        k_value = k_values[index] if index < len(k_values) else None
        if k_value is not None:
            try:
                # Assuming GetKValue returns the current value of k
                current_k_value = proxy.kValue
                if current_k_value != k_value:
                    return False
            except Exception as e:
                logger.exception(
                    f"Unable to get the kValue for {device_name}: {e}"
                )
                return False

    return True


def create_dish_mocks(dish_list: list, adapter_factory: HelperAdapterFactory):
    """Create dish mock based on dish list provided
    :param dish_list: Number of dish
    :type dish_list: list
    :param adapter_factory: Helper adapter factory
    :type adapter_factory: HelperAdapterFactory
    """
    dish_mock_list = []
    for dish in dish_list:
        dish_leaf_node_mock = mock.Mock(
            spec=["dev_name", "Scan", "EndScan", "kValue", "AbortCommands"]
        )
        dish_leaf_node_mock.dev_name = dish
        dish_leaf_node_mock.unresponsive = False
        dish_leaf_node_mock.Scan = mock.Mock(
            return_value=([ResultCode.OK], [""])
        )
        dish_leaf_node_mock.EndScan = mock.Mock(
            return_value=([ResultCode.OK], [""])
        )
        dish_leaf_node_mock.AbortCommands = mock.Mock(
            return_value=([ResultCode.OK], [""])
        )
        dish_leaf_node_mock.kValue = 1
        dish_mock_list.append(dish_leaf_node_mock)

        adapter_factory.get_or_create_adapter(
            dish,
            AdapterType.DISH,
            proxy=dish_leaf_node_mock,
        )
    return dish_mock_list


def setup_mapping_scan_data_for_cm(
    component_manager: SubarrayNodeComponentManagerMid,
):
    """Set up mapping scan data on component manager
    :param component_manager: Component Manager
    """
    component_manager.component.scan_duration = 6
    component_manager.pattern_scan_data = {
        "receptors": ["SKA001", "SKA0036"],
        "attrs": {
            "x_offsets": [0.5, 5.0],
            "y_offsets": [-5.0, 2.0],
        },
    }
    component_manager.trajectory_dish_adapter_list = [
        DISH_LEAF_NODE,
        DISH_LEAF_NODE36,
    ]
