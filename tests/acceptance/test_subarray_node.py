""" This module has acceptance test cases for subarray node."""
import json

import pytest
import tango
from pytest_bdd import given, parsers, scenarios, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import HealthState, ObsState
from ska_tmc_common import DevFactory, DishMode, PointingState
from tango import Database, DeviceProxy

from tests.integration.common import wait_for_final_subarray_obsstate
from tests.settings import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MCCS_SUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    logger,
    set_subsystem_availability,
    wait_for_attribute_to_change_to,
)
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    TMC_LOW_DOMAIN,
    TMC_MID_DOMAIN,
)


def get_json_input_str(path):
    """This method returns json input string"""
    with open(path, "r", encoding="utf-8") as f:
        input_json_str = f.read()
        input_arg = json.loads(input_json_str)
    return json.dumps(input_arg)


@given(
    "a TANGO ecosystem with a set of devices deployed",
    target_fixture="device_list",
)
def device_list():
    """device list from database"""
    db = Database()
    return db.get_device_exported("*")


@given(
    parsers.parse("a SubarrayNode device"),
    target_fixture="subarray_node",
)
def subarray_node():
    """Device instance of subarray node"""
    database = Database()
    instance_list = database.get_device_exported_for_class("LowTmcSubarray")
    for instance in instance_list.value_string:
        return DeviceProxy(instance)
    instance_list = database.get_device_exported_for_class("MidTmcSubarray")
    for instance in instance_list.value_string:
        return DeviceProxy(instance)


@when("I get the attribute InternalModel of the SubarrayNode device")
def internal_model(subarray_node):
    """method for setting the internal model"""
    pytest.internal_model = subarray_node.internalModel


@when(parsers.parse("I call the command {command_name}"))
def call_command(subarray_node, command_name, json_factory):
    """test case that check the command calling"""
    dev_name = subarray_node.dev_name()
    try:
        input_str = ""
        if TMC_MID_DOMAIN in dev_name:
            set_subsystem_availability(
                [
                    MID_CSPSUBARRAY_LEAF_NODE,
                    MID_SDPSUBARRAY_LEAF_NODE,
                    DISH_LEAF_NODE,
                ]
            )
            match command_name:
                case "AssignResources":
                    input_str = json_factory("AssignResources_mid")
                case "Configure":
                    input_str = json_factory("Configure_mid")
                case "Scan":
                    input_str = json_factory("Scan_mid")
        else:
            set_subsystem_availability(
                [LOW_CSPSUBARRAY_LEAF_NODE, LOW_SDPSUBARRAY_LEAF_NODE]
            )
            match command_name:
                case "AssignResources":
                    input_str = json_factory("AssignResources_low")
                case "Configure":
                    input_str = json_factory("Configure_low")
                case "Scan":
                    input_str = json_factory("Scan_low")

        if input_str:
            if TMC_LOW_DOMAIN in dev_name:
                mccs_sa_ln = tango.DeviceProxy(MCCS_SUBARRAY_LEAF_NODE)
                if command_name == "AssignResources":
                    mccs_sa_ln.SetDirectObsState(ObsState.IDLE)
                if command_name == "Configure":
                    mccs_sa_ln.SetDirectObsState(ObsState.READY)
                if command_name == "Scan":
                    mccs_sa_ln.SetDirectObsState(ObsState.SCANNING)
        else:
            if TMC_LOW_DOMAIN in dev_name:
                mccs_sa_ln = tango.DeviceProxy(MCCS_SUBARRAY_LEAF_NODE)
                if command_name == "EndScan":
                    mccs_sa_ln.SetDirectObsState(ObsState.READY)
                if command_name == "End":
                    mccs_sa_ln.SetDirectObsState(ObsState.IDLE)
                if command_name == "ReleaseAllResources":
                    mccs_sa_ln.SetDirectObsState(ObsState.EMPTY)

        # Retry invocation in case of failure due to unavailability
        try:
            if input_str:
                pytest.command_result = subarray_node.command_inout(
                    command_name, input_str
                )
            else:
                pytest.command_result = subarray_node.command_inout(
                    command_name
                )
        except tango.DevFailed as dev_failed:
            logger.exception(
                "Exception occurred while executing command: %s", dev_failed
            )
            # If it is a 'TRANSIENT CORBA system exception', raise it
            if (
                "TRANSIENT CORBA system exception: TRANSIENT_CallTimedout"
                in str(dev_failed)
            ):
                raise
            if TMC_MID_DOMAIN in dev_name:
                set_subsystem_availability(
                    [
                        MID_CSPSUBARRAY_LEAF_NODE,
                        MID_SDPSUBARRAY_LEAF_NODE,
                        DISH_LEAF_NODE,
                    ]
                )
            else:
                set_subsystem_availability(
                    [LOW_CSPSUBARRAY_LEAF_NODE, LOW_SDPSUBARRAY_LEAF_NODE]
                )
            wait_for_attribute_to_change_to(
                subarray_node, "isSubarrayAvailable", True
            )
            if input_str:
                pytest.command_result = subarray_node.command_inout(
                    command_name, input_str
                )
            else:
                pytest.command_result = subarray_node.command_inout(
                    command_name
                )

    except Exception as ex:
        assert "CommandNotAllowed" in str(ex)
        pytest.command_result = str(ex)


@then("it correctly reports the failed and working devices")
def check_internal_model(device_list):
    """test case for checking internal model"""
    json_model = json.loads(pytest.internal_model)

    for dev in json_model["devices"]:
        running_dev = None
        for exported_dev in device_list.value_string:
            if exported_dev == dev["dev_name"]:
                running_dev = DeviceProxy(exported_dev)

        if running_dev is None:
            assert dev["unresponsive"] == "True"
            assert dev["exception"] != "None"
            continue

        assert "DevState." + str(running_dev.State()) == dev["state"]
        assert str(HealthState(running_dev.healthState)) == dev["healthState"]

        if ("subarray" in dev["dev_name"]) and (
            "leaf-node" not in dev["dev_name"]
        ):
            logger.info("dev['dev_name']: %s", dev["dev_name"])
            logger.info("running_dev.obsState: %s", running_dev.obsState)
            logger.info("dev['obsState']: %s", dev["obsState"])
            assert str(ObsState(running_dev.obsState)) == dev["obsState"]

        if "elt/master" in dev["dev_name"]:
            assert (
                str(PointingState(running_dev.pointingState))
                == dev["pointingState"]
            )


@then(
    parsers.parse(
        "the {command_name} command is executed "
        "successfully on lower level devices"
    )
)
def check_command(subarray_node, command_name, change_event_callbacks):
    """checking the command name acceptance test cases"""
    if "CommandNotAllowed" in pytest.command_result:
        raise Exception(pytest.command_result)

    assert pytest.command_result[0][0] == ResultCode.QUEUED
    unique_id = pytest.command_result[1][0]

    subarray_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )

    expected_obs_state = ObsState.EMPTY
    match command_name:
        case "AssignResources":
            expected_obs_state = ObsState.IDLE

        case "Configure":
            if TMC_MID_DOMAIN in subarray_node.dev_name():
                dish_master = DevFactory().get_device(DISH_LEAF_NODE)
                dish_master.SetDirectDishMode(DishMode.OPERATE)
                dish_master.SetDirectPointingState(PointingState.TRACK)
            expected_obs_state = ObsState.READY

        case "Scan":
            expected_obs_state = ObsState.SCANNING

        case "EndScan":
            expected_obs_state = ObsState.READY

        case "End":
            expected_obs_state = ObsState.IDLE

        case "ReleaseAllResources":
            expected_obs_state = ObsState.EMPTY

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id, json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=8,
    )
    wait_for_final_subarray_obsstate(subarray_node, expected_obs_state)
    assert expected_obs_state == subarray_node.read_attribute("obsState").value


scenarios("../features/subarraynode.feature")
