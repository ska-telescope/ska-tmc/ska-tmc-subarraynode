"""A test module to verify skb-477"""
import pytest
from pytest_bdd import given, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)


@pytest.mark.SKA_low
@scenario(
    "../features/XTP_60776_skb_477.feature",
    "Verify SKB-477 - Unnecessary subarray_id key in Scan schema",
)
def test_verify_skb_477():
    """Test to verify TMC SubarrayNode rejects
    unnecessary subarray_id key in Scan schema
    """


@given("Subarray Node in observation state READY")
def check_subarray_node_obsState_ready(
    subarray_node, event_recorder, json_factory
):
    ensure_checked_devices(subarray_node.subarray_node)
    assign_str = json_factory("AssignResources_low")
    configure_str = json_factory("Configure_low")
    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    subarray_node.assert_command_completion(configure_id, event_recorder)


@when("I invoke Scan command on Subarray Node with key subarray_id")
def invoke_scan_with_subarray_id(subarray_node, json_factory):
    scan_str = json_factory("Scan_low_with_subarray_id_key")
    pytest.result, _ = subarray_node.subarray_node.Scan(scan_str)


@then("TMC SubarrayNode raises exception with ResultCode.REJECTED")
def check_subarray_node_scan_resultcode():
    assert ResultCode.REJECTED == pytest.result


@then("TMC SubarrayNode remains in observation state READY")
def check_subarray_node_obs_state(subarray_node):
    subarray_node.subarray_node.obsState == ObsState.READY
