Feature: Default

	#Verify the output_host and output_port in configure json send to csp subarray
	@XTP-42680 @XTP-28347 @post_deployment @acceptance @SKA_mid
	Scenario: Verify the output_host and output_port in configure json send to csp subarray
		Given a SubarrayNode device in obsState IDLE
		When I invoke configure command on SubarrayNode
		Then SubarrayNode invokes configure on csp with json containing output_host and output_port
		And SubarrayNode obsState moves to READY