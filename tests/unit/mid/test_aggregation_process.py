"""This unit test is used for testing
aggregation using aggregation process
"""
import logging
import time
from datetime import datetime

import pytest
from ska_tango_base.control_model import ObsState

from ska_tmc_subarraynode.manager.event_data_manager import (
    CommandResultData,
    DishModeData,
    EventDataStorage,
    ObsStateData,
    PointingStateData,
)


def generate_data(
    obs_state_data_type, telescope="mid", use_command_in_progress=True
):
    event_data = EventDataStorage(
        command_in_progress="AssignResources"
        if use_command_in_progress
        else "",
    )
    if obs_state_data_type == "EMPTY":
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="EMPTY", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="EMPTY", event_timestamp=datetime.now()
            ),
        }
    if obs_state_data_type == "IDLE":
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="IDLE", event_timestamp=datetime.now()
            ),
            "sdp/1": ObsStateData(
                obs_state="IDLE", event_timestamp=datetime.now()
            ),
        }
        if use_command_in_progress:
            event_data.command_result_data = {
                "csp/1": CommandResultData(
                    result="OK",
                    unique_id="100",
                    event_timestamp=datetime.now(),
                ),
                "sdp/1": CommandResultData(
                    result="OK",
                    unique_id="100",
                    event_timestamp=datetime.now(),
                ),
            }
        if telescope == "low":
            event_data.obs_state_data["mccs/1"] = ObsStateData(
                obs_state="IDLE", event_timestamp=datetime.now()
            )
            if use_command_in_progress:
                event_data.command_result_data["mccs/1"] = CommandResultData(
                    result="OK",
                    unique_id="100",
                    event_timestamp=datetime.now(),
                )
        else:
            event_data.pointing_state_data = {
                "dish/1": PointingStateData(
                    pointing_state="READY", event_timestamp=datetime.now()
                ),
                "dish/2": PointingStateData(
                    pointing_state="READY", event_timestamp=datetime.now()
                ),
            }

    elif obs_state_data_type == "READY":
        event_data.command_in_progress = (
            "configure" if use_command_in_progress else ""
        )
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
        }
        if telescope == "mid":
            event_data.pointing_state_data = {
                "dish/1": PointingStateData(
                    pointing_state="TRACK", event_timestamp=datetime.now()
                ),
                "dish/2": PointingStateData(
                    pointing_state="TRACK", event_timestamp=datetime.now()
                ),
            }
            if use_command_in_progress:
                event_data.command_result_data = {
                    "csp/1": CommandResultData(
                        result="OK",
                        unique_id="100",
                        event_timestamp=datetime.now(),
                    ),
                    "sdp/1": CommandResultData(
                        result="OK",
                        unique_id="100",
                        event_timestamp=datetime.now(),
                    ),
                    "dish/1": CommandResultData(
                        result="OK",
                        unique_id="100",
                        event_timestamp=datetime.now(),
                    ),
                    "dish/2": CommandResultData(
                        result="OK",
                        unique_id="100",
                        event_timestamp=datetime.now(),
                    ),
                }
            event_data.dish_mode_data = {
                "dish/1": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
                "dish/2": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
            }
        else:
            event_data.obs_state_data["mccs/1"] = ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            )
            if use_command_in_progress:
                event_data.command_result_data = {
                    "csp/1": CommandResultData(
                        result="OK",
                        unique_id="100",
                        event_timestamp=datetime.now(),
                    ),
                    "sdp/1": CommandResultData(
                        result="OK",
                        unique_id="100",
                        event_timestamp=datetime.now(),
                    ),
                    "mccs/1": CommandResultData(
                        result="OK",
                        unique_id="100",
                        event_timestamp=datetime.now(),
                    ),
                }

    elif obs_state_data_type in ("RESOURCING", "RESOURCING_FAILED"):
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="RESOURCING"
                if obs_state_data_type == "RESOURCING"
                else "IDLE",
                event_timestamp=datetime.now(),
            ),
            "sdp/2": ObsStateData(
                obs_state="IDLE", event_timestamp=datetime.now()
            ),
        }
        if use_command_in_progress:
            event_data.command_result_data = {
                "csp/1": CommandResultData(
                    result=""
                    if obs_state_data_type == "RESOURCING"
                    else "FAILED",
                    unique_id="100",
                    event_timestamp=datetime.now(),
                ),
                "sdp/1": CommandResultData(
                    result="OK",
                    unique_id="100",
                    event_timestamp=datetime.now(),
                ),
            }
        if telescope == "mid":
            event_data.dish_mode_data = {
                "dish/1": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
                "dish/2": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
            }
        else:
            event_data.obs_state_data["mccs/1"] = ObsStateData(
                obs_state="IDLE", event_timestamp=datetime.now()
            )

    elif obs_state_data_type == "SUCCESSIVE_ASSIGN_RESOURCE":
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="IDLE", event_timestamp=datetime.now()
            ),
            "sdp/1": ObsStateData(
                obs_state="IDLE", event_timestamp=datetime.now()
            ),
        }
        if use_command_in_progress:
            event_data.command_result_data = {
                "csp/1": CommandResultData(
                    result="FAILED",
                    unique_id="100",
                    event_timestamp=datetime.now(),
                ),
                "sdp/1": CommandResultData(
                    result="OK",
                    unique_id="100",
                    event_timestamp=datetime.now(),
                ),
            }
        if telescope == "low":
            event_data.obs_state_data["mccs/1"] = ObsStateData(
                obs_state="IDLE", event_timestamp=datetime.now()
            )
            event_data.command_result_data["mccs/1"] = CommandResultData(
                result="OK", unique_id="100", event_timestamp=datetime.now()
            )

    elif obs_state_data_type == "CONFIGURING":
        event_data.command_in_progress = (
            "Configure" if use_command_in_progress else ""
        )
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="CONFIGURING", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
        }
        if telescope == "mid":
            event_data.dish_mode_data = {
                "dish/1": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
                "dish/2": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
            }
        if telescope == "low":
            event_data.obs_state_data["mccs/1"] = ObsStateData(
                obs_state="CONFIGURING", event_timestamp=datetime.now()
            )

    elif obs_state_data_type in ("PARTIAL_CONFIG", "FAILED_PARTIAL_CONFIG"):
        event_data.command_in_progress = "Configure"
        event_data.is_partial_configuration = True
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
        }
        event_data.dish_mode_data = {
            "dish/1": DishModeData(
                dish_mode="OPERATE", event_timestamp=datetime.now()
            ),
            "dish/2": DishModeData(
                dish_mode="OPERATE", event_timestamp=datetime.now()
            ),
        }
        event_data.pointing_state_data = {
            "dish/1": PointingStateData(
                pointing_state="TRACK", event_timestamp=datetime.now()
            ),
            "dish/2": PointingStateData(
                pointing_state="TRACK", event_timestamp=datetime.now()
            ),
        }
        event_data.command_result_data = {
            "dish/1": CommandResultData(
                result=""
                if obs_state_data_type == "FAILED_PARTIAL_CONFIG"
                else "OK",
                unique_id="100",
                event_timestamp=datetime.now(),
            ),
            "dish/2": CommandResultData(
                result="OK", unique_id="100", event_timestamp=datetime.now()
            ),
        }

    elif obs_state_data_type == "CONFIGURING_POINTING_READY":
        event_data.command_in_progress = (
            "Configure" if use_command_in_progress else ""
        )
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
        }
        if telescope == "mid":
            event_data.dish_mode_data = {
                "dish/1": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
                "dish/2": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
            }
            event_data.pointing_state_data = {
                "dish/1": PointingStateData(
                    pointing_state="READY", event_timestamp=datetime.now()
                ),
                "dish/2": PointingStateData(
                    pointing_state="TRACK", event_timestamp=datetime.now()
                ),
            }

    elif obs_state_data_type == "ABORTING":
        event_data.command_in_progress = (
            "Abort" if use_command_in_progress else ""
        )
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="ABORTING", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="ABORTING", event_timestamp=datetime.now()
            ),
        }
        if telescope == "mid":
            event_data.dish_mode_data = {
                "dish/1": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
                "dish/2": DishModeData(
                    dish_mode="OPERATE", event_timestamp=datetime.now()
                ),
            }
        else:
            event_data.obs_state_data["mccs/1"] = ObsStateData(
                obs_state="ABORTING", event_timestamp=datetime.now()
            )
    elif obs_state_data_type == "ABORTED":
        event_data.command_in_progress = (
            "Abort" if use_command_in_progress else ""
        )
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="ABORTED", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="ABORTED", event_timestamp=datetime.now()
            ),
        }
        if telescope == "mid":
            event_data.pointing_state_data = {
                "dish/1": PointingStateData(
                    pointing_state="READY", event_timestamp=datetime.now()
                ),
                "dish/2": PointingStateData(
                    pointing_state="READY", event_timestamp=datetime.now()
                ),
            }
        else:
            event_data.obs_state_data["mccs/1"] = ObsStateData(
                obs_state="ABORTED", event_timestamp=datetime.now()
            )
    elif obs_state_data_type == "RESTARTING":
        event_data.command_in_progress = (
            "restart" if use_command_in_progress else ""
        )
        event_data.obs_state_data = {
            "csp/1": ObsStateData(
                obs_state="RESTARTING", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="ABORTED", event_timestamp=datetime.now()
            ),
        }
        if telescope == "low":
            event_data.obs_state_data["mccs/1"] = ObsStateData(
                obs_state="EMPTY", event_timestamp=datetime.now()
            )
    return event_data


@pytest.mark.parametrize(
    "use_command_in_progress,telescope",
    [(True, "mid"), (False, "mid"), (True, "low"), (False, "low")],
)
def test_aggregation_process(
    aggregation_process_mid,
    aggregation_process_low,
    use_command_in_progress,
    telescope,
):
    """This test aggregation when data added to event data queue
    then it perform the aggregation as expected
    """
    if telescope == "mid":
        aggregation_process = aggregation_process_mid
    else:
        aggregation_process = aggregation_process_low

    aggregation_process.start_aggregation_process()

    event_data_to_test = [
        ("EMPTY", "EMPTY"),
        ("RESOURCING", "RESOURCING"),
        ("IDLE", "IDLE"),
        ("CONFIGURING", "CONFIGURING"),
        ("READY", "READY"),
        ("ABORTING", "ABORTING"),
        ("ABORTED", "ABORTED"),
        ("RESTARTING", "RESTARTING"),
    ]

    if telescope == "mid":
        event_data_to_test.append(
            ("CONFIGURING", "CONFIGURING_POINTING_READY")
        )
        if use_command_in_progress:
            event_data_to_test.append(
                ("READY", "PARTIAL_CONFIG"),
            )
            event_data_to_test.append(("CONFIGURING", "FAILED_PARTIAL_CONFIG"))

    if use_command_in_progress:
        event_data_to_test.append(("RESOURCING", "SUCCESSIVE_ASSIGN_RESOURCE"))
        event_data_to_test.append(("RESOURCING", "RESOURCING_FAILED"))

    for obs_state, obs_state_data_to_test in event_data_to_test:
        logging.info(f"obs state to test {obs_state_data_to_test}")
        event_data = generate_data(
            obs_state_data_to_test, telescope, use_command_in_progress
        )
        aggregation_process.event_data_queue.put(event_data)
        time.sleep(0.2)
        assert (
            aggregation_process.aggregated_obs_state[0] == ObsState[obs_state]
        )


@pytest.mark.parametrize(
    "telescope",
    ["mid", "low"],
)
def test_convert_event_data_to_dict(
    aggregation_process_mid, aggregation_process_low, telescope
):
    """This test convert event to dict method return dict as
    expected
    """
    if telescope == "mid":
        aggregation_process = aggregation_process_mid
    else:
        aggregation_process = aggregation_process_low

    event_data = EventDataStorage(
        command_in_progress="AssignResources",
        obs_state_data={
            "csp/1": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
            "sdp/2": ObsStateData(
                obs_state="READY", event_timestamp=datetime.now()
            ),
        },
        command_result_data={
            "csp/1": CommandResultData(
                result="OK", unique_id="100", event_timestamp=datetime.now()
            ),
            "sdp/1": CommandResultData(
                result="OK", unique_id="100", event_timestamp=datetime.now()
            ),
        },
    )
    event_data_dict = (
        aggregation_process._convert_event_data_to_dict_for_rule_engine(
            event_data
        )
    )
    expected_event_data_dict = {
        "all_unique_obs_states": ["READY"],
        "all_unique_command_results": ["OK"],
        "command_in_progress": "AssignResources",
        "command_timestamp": None,
    }
    if telescope == "mid":
        expected_event_data_dict["all_unique_pointing_states"] = []
        expected_event_data_dict["all_unique_dish_modes"] = []
        expected_event_data_dict["is_partial_configuration"] = False

    logging.info(
        f"expected event dict {expected_event_data_dict} and {event_data_dict}"
    )
    assert all(
        v == event_data_dict[k] for k, v in expected_event_data_dict.items()
    ) and len(expected_event_data_dict) == len(event_data_dict)
