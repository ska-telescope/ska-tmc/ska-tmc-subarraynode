import pytest
from ska_tango_base.executor import TaskStatus

from tests.settings import set_sdp_csp_leaf_node_availability_for_aggregation


def test_cm_off(tango_context, task_callback, component_manager_mid):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )
    res_code, message = component_manager_mid.off(task_callback)
    assert res_code == TaskStatus.QUEUED
    assert message == "Task queued"
