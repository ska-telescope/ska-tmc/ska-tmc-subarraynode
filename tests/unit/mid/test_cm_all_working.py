import time

import pytest

from tests.settings import count_faulty_devices, create_mid_cm, logger


@pytest.mark.skip(
    reason="Liveliness probe implementation removed for unit tests"
)
def test_all_working(tango_context, component_manager_mid):
    logger.info("%s", tango_context)
    cm, start_time, _ = create_mid_cm()
    num_faulty = count_faulty_devices(cm)
    assert num_faulty == 0

    elapsed_time = time.time() - start_time
    logger.info("checked %s devices in %s", num_faulty, elapsed_time)
    for dev_info in cm.devices:
        assert not dev_info.unresponsive
