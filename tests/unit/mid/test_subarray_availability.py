import time

import pytest

from tests.settings import (
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    TIMEOUT,
    count_faulty_devices,
    create_cm_no_faulty_devices,
    set_device_availability,
)


def test_check_subarray_availability_attribute_initial_events(
    tango_context, component_manager_mid
):
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    start_time = time.time()
    elapsed_time = 0
    while not component_manager_mid.component.subarray_availability:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert component_manager_mid.component.subarray_availability


def test_check_subarray_availability_with_no_csp_but_sdp_leaf_node_available(
    tango_context, component_manager_mid
):
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    set_device_availability(
        component_manager_mid, MID_CSPSUBARRAY_LEAF_NODE, False
    )
    set_device_availability(
        component_manager_mid, MID_SDPSUBARRAY_LEAF_NODE, True
    )
    start_time = time.time()
    elapsed_time = 0
    while component_manager_mid.component.subarray_availability is not False:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert component_manager_mid.component.subarray_availability is False


def test_check_subarray_availability_with_no_sdp_but_csp_leaf_node_available(
    tango_context, component_manager_mid
):
    num_faulty = count_faulty_devices(component_manager_mid)
    assert num_faulty == 0
    set_device_availability(
        component_manager_mid, MID_CSPSUBARRAY_LEAF_NODE, True
    )
    set_device_availability(
        component_manager_mid, MID_SDPSUBARRAY_LEAF_NODE, False
    )
    start_time = time.time()
    elapsed_time = 0
    while component_manager_mid.component.subarray_availability is not False:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail("Timeout occurred while executing the test")
    assert component_manager_mid.component.subarray_availability is False
