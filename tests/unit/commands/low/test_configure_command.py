import json
from os.path import dirname, join

import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    SUBARRAY_LEAF_NODES_LOW,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import LOW_CSPSUBARRAY_LEAF_NODE


def get_configure_input_str(
    configure_input_file="Configure_low.json",
):
    path = join(
        dirname(__file__), "..", "..", "..", "data", configure_input_file
    )
    with open(path, "r") as f:
        configure_input_str = f.read()
    return configure_input_str


def test_low_subarray_configure_command_completed(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    configure_str = get_configure_input_str()
    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.IDLE]
    )
    component_manager_low.set_subarray_id(1)
    component_manager_low.is_command_allowed("Configure")
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    component_manager_low.initial_obs_state = ObsState.IDLE
    component_manager_low.configure(configure_str, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_and_result_code_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.CONFIGURING, ObsState.READY],
        "Configure",
        ResultCode.OK,
    )

    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_low_subarray_configure_fail_csp_subarray_ln(
    tango_context, task_callback, component_manager_low
):
    component_manager_low.set_subarray_id(1)
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )

    helper_adapter_factory = HelperAdapterFactory()
    # include exception in Configure command
    failing_dev = LOW_CSPSUBARRAY_LEAF_NODE
    attrs = {"Configure.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.CSPSUBARRAY, proxy=CspsubarraylnMock
    )

    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.IDLE]
    )
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    component_manager_low.is_command_allowed("Configure")
    component_manager_low.initial_obs_state = ObsState.IDLE
    component_manager_low.adapter_factory = helper_adapter_factory
    configure_str = get_configure_input_str()
    component_manager_low.configure(configure_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking Configure in " in result["result"][1]


@pytest.mark.parametrize(
    "missing_key",
    ["sdp", "csp", "mccs"],
)
def test_low_subarray_configure_missing_keys(
    tango_context, task_callback, missing_key: str, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.is_command_allowed("Configure")
    component_manager_low.adapter_factory = HelperAdapterFactory()

    configure_str = get_configure_input_str()
    json_argument = json.loads(configure_str)
    del json_argument[missing_key]

    result_code, message = component_manager_low.configure(
        json.dumps(json_argument), task_callback=task_callback
    )
    assert result_code == TaskStatus.REJECTED
    assert f"Missing key: '{missing_key}'" in message


def test_low_subarray_configure_invalid_stn_beam_id(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    configure_str = get_configure_input_str()
    json_argument = json.loads(configure_str)
    json_argument["csp"]["lowcbf"]["vis"]["stn_beams"][0].update(
        {"stn_beam_id": 0}
    )
    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.IDLE]
    )
    component_manager_low.set_subarray_id(1)
    component_manager_low.is_command_allowed("Configure")
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    component_manager_low.initial_obs_state = ObsState.IDLE
    result, _ = component_manager_low.configure(
        json.dumps(json_argument), task_callback
    )

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    error_message = (
        "Error in receiving CSP Config schema from the TelModel library"
    )
    assert error_message in result["result"][1]


def test_low_subarray_configure_fail_is_allowed(
    tango_context, component_manager_low
):
    component_manager_low.op_state_model._op_state = DevState.FAULT
    assert component_manager_low.is_command_allowed("Configure") is False


def test_low_subarray_configure_empty_input_json(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.is_command_allowed("Configure")
    component_manager_low.adapter_factory = HelperAdapterFactory()
    res_code, _ = component_manager_low.configure(
        " ", task_callback=task_callback
    )
    assert res_code == TaskStatus.REJECTED


def test_low_subarray_configure_command_not_allowed(
    tango_context, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low,
        False,
        InputParameterLow(None),
    )
    with pytest.raises(CommandNotAllowed):
        callable = component_manager_low.command_allowed_callable("Configure")
        callable()


def test_configure_command_timeout(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.set_subarray_id(1)
    component_manager_low.is_command_allowed("Configure")
    component_manager_low.command_timeout = 5

    configure_str = get_configure_input_str()
    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.IDLE]
    )
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    component_manager_low.initial_obs_state = ObsState.IDLE
    component_manager_low.configure(configure_str, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Timeout has occurred, command failed" in result["result"][1]


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("EMPTY", "FAULT"),
)
def test_low_subarray_configure_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True
    )
    exception_message = "Configure command not permitted"
    component_manager_low.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_low.command_allowed_callable("Configure")
        callable()


def test_low_subarray_configure_incorrect_jones_keys(
    tango_context, task_callback, component_manager_low
):
    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.IDLE]
    )
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.set_subarray_id(1)
    component_manager_low.is_command_allowed("Configure")
    component_manager_low.initial_obs_state = ObsState.IDLE
    component_manager_low.adapter_factory = HelperAdapterFactory()

    configure_str = get_configure_input_str()
    json_argument = json.loads(configure_str)
    component_manager_low.jones_uri = 1
    component_manager_low.configure(
        json.dumps(json_argument), task_callback=task_callback
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)

    assert ResultCode.FAILED == result["result"][0]
    error_message = (
        "Error in receiving CSP Config schema from the TelModel library"
    )
    assert error_message in result["result"][1]
