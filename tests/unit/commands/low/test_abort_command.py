import mock
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.commands.abort_command import Abort
from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    SUBARRAY_LEAF_NODES_LOW,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_events,
)


def test_low_abort_command(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    helper_adapter_factory = HelperAdapterFactory()
    component_manager_low.is_command_allowed("Abort")
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    abort_command = Abort(
        component_manager_low,
        component_manager_low.obs_state_model,
        helper_adapter_factory,
        logger=logger,
    )
    abort_command.invoke_abort(logger, task_callback, component_manager_low)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.STARTED, "Command Started"),
    )


def test_low_subarray_abort_command_fail_on_defective_cspsubarray(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.is_command_allowed("Abort")
    helper_adapter_factory = HelperAdapterFactory()
    # include exception in Abort command
    attrs = {"Abort.side_effect": Exception}
    cspsubarraylnmock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        LOW_CSPSUBARRAY_LEAF_NODE,
        AdapterType.CSPSUBARRAY,
        proxy=cspsubarraylnmock,
    )
    component_manager_low.adapter_factory = helper_adapter_factory
    simulate_obs_state_events(
        component_manager_low, SUBARRAY_LEAF_NODES_LOW, [ObsState.IDLE]
    )
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    abort_command = Abort(
        component_manager_low,
        component_manager_low.obs_state_model,
        helper_adapter_factory,
        logger=logger,
    )
    abort_command.invoke_abort(logger, task_callback, component_manager_low)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert (
        "Error while invoking Abort command on CSP Subarray Leaf Node"
        in result["result"][1]
    )


def test_low_subarray_abort_invalid_state(
    tango_context, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.op_state_model._op_state = DevState.FAULT
    assert component_manager_low.is_command_allowed("Abort") is False


def test_check_scan_thread(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.obs_state_model._straight_to_state("IDLE")
    helper_adapter_factory = HelperAdapterFactory()
    abort_command = Abort(
        component_manager_low,
        component_manager_low.obs_state_model,
        helper_adapter_factory,
        logger=logger,
    )
    component_manager_low.is_scan_timer_running = False

    component_manager_low.adapter_factory = helper_adapter_factory
    abort_command.invoke_abort(logger, task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
