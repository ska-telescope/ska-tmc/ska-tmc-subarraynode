import time

import mock
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.commands.off_command import Off
from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
)
from tests.test_helpers.constants import LOW_SDPSUBARRAY_LEAF_NODE


def test_low_off(tango_context, component_manager_low, task_callback):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.is_command_allowed("Off")
    component_manager_low.off(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )


def test_low_off_fail_subarray(
    tango_context, component_manager_low, task_callback
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )

    helper_adapter_factory = HelperAdapterFactory()

    failing_dev = LOW_SDPSUBARRAY_LEAF_NODE
    attrs = {"Off.side_effect": Exception}
    sdpsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SDPSUBARRAY, proxy=sdpsubarraylnMock
    )

    component_manager_low.adapter_factory = helper_adapter_factory
    off_command = Off(
        component_manager_low, helper_adapter_factory, logger=logger
    )
    off_command.subarray_off(logger=logger, task_callback=task_callback)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            f"Error while invoking Off command on {failing_dev}",
        ),
        lookahead=2,
    )


def test_low_off_task_completed(
    tango_context, component_manager_low, task_callback
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )

    component_manager_low.is_command_allowed("Off")
    helper_adapter_factory = HelperAdapterFactory()

    off_command = Off(
        component_manager_low, helper_adapter_factory, logger=logger
    )
    off_command.subarray_off(logger, task_callback=task_callback)
    time.sleep(0.1)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
        lookahead=2,
    )


def test_low_off_fail_check_allowed_with_device_disable(
    tango_context, component_manager_low
):
    component_manager_low.op_state_model._op_state = DevState.DISABLE
    assert component_manager_low.is_command_allowed("Off") is False


def test_low_off_sdpln_unavailable_task_completed(
    tango_context, component_manager_low, task_callback
):
    unavailable_device = LOW_SDPSUBARRAY_LEAF_NODE
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, False, InputParameterLow(None)
    )
    component_manager_low.is_command_allowed("Off")
    helper_adapter_factory = HelperAdapterFactory()

    off_command = Off(
        component_manager_low, helper_adapter_factory, logger=logger
    )
    off_command.subarray_off(logger, task_callback=task_callback)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            f"{unavailable_device} is not available to receive command",
        ),
        lookahead=2,
    )
