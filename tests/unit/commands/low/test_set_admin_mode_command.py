import json

import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus

from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import set_sdp_csp_leaf_node_availability_for_aggregation


def test_low_subarray_admin_mode_command_completed(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    argin = {
        "devices": {
            "mid": {"adminMode": 2},
            "low": {
                "csp": {"adminMode": 0},
                "sdp": {"adminMode": 1},
                "mccs": {"adminMode": 0},
            },
        }
    }
    input_json = json.dumps(argin)
    # AdminMode can be invoked only when ObsState is EMPTY
    component_manager_low.is_admin_mode_enabled = True
    component_manager_low.initial_obs_state = ObsState.EMPTY
    component_manager_low.is_command_allowed("SetAdminMode")

    component_manager_low.set_admin_mode(
        input_json, task_callback=task_callback
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )


def test_low_subarray_admin_mode_not_allowed(
    tango_context, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.is_admin_mode_enabled = True
    exception_message = (
        "SetAdminMode command not permitted in observation state 4"
    )
    component_manager_low.obs_state_model._straight_to_state("READY")
    component_manager_low.is_command_allowed("SetAdminMode")
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_low.command_allowed_callable(
            "SetAdminMode"
        )
        callable()


def test_set_admin_mode_missing_devices(
    tango_context, component_manager_low, task_callback
):
    # Input JSON missing "devices" field
    argin = '{"invalid_key": {"sdp": {"adminMode": 1}}}'
    component_manager_low.is_admin_mode_enabled = True
    # Assertions
    input_json = json.dumps(argin)
    # AdminMode can be invoked only when ObsState is EMPTY
    component_manager_low.initial_obs_state = ObsState.EMPTY
    component_manager_low.is_command_allowed("SetAdminMode")
    component_manager_low.set_admin_mode(
        input_json, task_callback=task_callback
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)

    assert ResultCode.FAILED == result["result"][0]
    error_message = "Missing devices field in input."
    assert error_message in result["result"][1]


def test_set_admin_mode_partial_devices(
    tango_context, component_manager_low, task_callback
):
    argin = {"devices": {"low": {"sdp": {"adminMode": 3}}}}
    component_manager_low.is_admin_mode_enabled = True
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    input_json = json.dumps(argin)
    # AdminMode can be invoked only when ObsState is EMPTY
    component_manager_low.initial_obs_state = ObsState.EMPTY
    component_manager_low.is_command_allowed("SetAdminMode")
    component_manager_low.set_admin_mode(
        input_json, task_callback=task_callback
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )
