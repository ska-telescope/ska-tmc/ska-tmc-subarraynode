import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterType, DevFactory
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterLow
from tests.settings import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    SUBARRAY_LEAF_NODES_LOW,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)


@pytest.mark.parametrize(
    "device_name, adapter_type, devices_in_ready",
    [
        pytest.param(
            LOW_CSPSUBARRAY_LEAF_NODE,
            AdapterType.CSPSUBARRAY,
            [LOW_SDPSUBARRAY_LEAF_NODE],
        ),
        pytest.param(
            LOW_SDPSUBARRAY_LEAF_NODE,
            AdapterType.SDPSUBARRAY,
            [LOW_CSPSUBARRAY_LEAF_NODE],
        ),
    ],
)
def test_low_subarray_end_scan_fail_on_subarray_ln(
    tango_context,
    task_callback,
    device_name,
    adapter_type,
    devices_in_ready,
    component_manager_low,
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )

    helper_adapter_factory = HelperAdapterFactory()
    subarrayleafnode_mock = mock.Mock(
        spec=["dev_name", "EndScan"],
    )
    subarrayleafnode_mock.EndScan = mock.Mock(
        return_value=([ResultCode.FAILED], ["Induced Failure"])
    )
    subarrayleafnode_mock.dev_name = device_name
    helper_adapter_factory.get_or_create_adapter(
        device_name, adapter_type, proxy=subarrayleafnode_mock
    )
    simulate_obs_state_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.SCANNING],
    )
    component_manager_low.obs_state_model._straight_to_state("SCANNING")
    component_manager_low.is_command_allowed("EndScan")
    component_manager_low.adapter_factory = helper_adapter_factory
    component_manager_low.end_scan(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking EndScan on " in result["result"][1]

    # Verifying that the other devices completed the task successfully
    dev_factory = DevFactory()
    for device in devices_in_ready:
        device_proxy = dev_factory.get_device(device)
        assert device_proxy.obsState == ObsState.READY


def test_low_subarray_end_scan_fail_is_allowed(
    tango_context, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low,
        False,
        InputParameterLow(None),
    )

    with pytest.raises(CommandNotAllowed):
        callable = component_manager_low.command_allowed_callable("EndScan")
        callable()


def test_low_subarray_end_scan_task_completed(
    tango_context, task_callback, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True, InputParameterLow(None)
    )
    component_manager_low.obs_state_model._straight_to_state("SCANNING")
    simulate_obs_state_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.SCANNING],
    )
    component_manager_low.is_command_allowed("EndScan")
    component_manager_low.end_scan(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    assert component_manager_low.command_in_progress == "EndScan"

    simulate_obs_state_and_result_code_events(
        component_manager_low,
        SUBARRAY_LEAF_NODES_LOW,
        [ObsState.READY],
        "EndScan",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("EMPTY", "FAULT", "READY"),
)
def test_low_subarray_end_scan_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_low
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_low, True
    )
    exception_message = "EndScan command not permitted"
    component_manager_low.obs_state_model._straight_to_state(
        not_allowed_obs_state
    )
    with pytest.raises(StateModelError, match=exception_message):
        callable = component_manager_low.command_allowed_callable("EndScan")
        callable()
