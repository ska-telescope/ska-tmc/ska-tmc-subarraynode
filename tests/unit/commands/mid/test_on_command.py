import time

import mock
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.commands.on_command import On
from tests.mock_callable import MockCallable
from tests.settings import (
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
)
from tests.test_helpers.constants import MID_SDPSUBARRAY_LEAF_NODE


def test_on(tango_context, component_manager_mid, task_callback):
    logger.info("%s", tango_context)
    # import debugpy; debugpy.debug_this_thread()
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )

    component_manager_mid.is_command_allowed("On")
    component_manager_mid.on(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )


def test_on_fail_sdp_subarray(tango_context, component_manager_mid):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )
    component_manager_mid.is_command_allowed("On")
    my_adapter_factory = HelperAdapterFactory()
    # include exception in On command
    failing_dev = MID_SDPSUBARRAY_LEAF_NODE
    attrs = {"On.side_effect": Exception}
    sdpsubarraylnMock = mock.Mock(**attrs)
    my_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SDPSUBARRAY, proxy=sdpsubarraylnMock
    )
    unique_id = f"{time.time()}_On"
    task_callback = MockCallable(unique_id)

    component_manager_mid.adapter_factory = my_adapter_factory
    on_command = On(component_manager_mid, my_adapter_factory, logger=logger)
    on_command.on_leaf_nodes(logger=logger, task_callback=task_callback)
    assert task_callback.status == TaskStatus.COMPLETED
    assert task_callback.result == (
        ResultCode.FAILED,
        "Error while invoking On command on " + f"{failing_dev}",
    )


def test_on_task_completed(tango_context, component_manager_mid):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )

    component_manager_mid.is_command_allowed("On")
    my_adapter_factory = HelperAdapterFactory()

    unique_id = f"{time.time()}_On"
    task_callback = MockCallable(unique_id)

    on_command = On(component_manager_mid, my_adapter_factory, logger=logger)
    on_command.on_leaf_nodes(logger, task_callback=task_callback)
    time.sleep(0.1)
    assert task_callback.status == TaskStatus.COMPLETED
    assert task_callback.result == (
        ResultCode.OK,
        "Command Completed",
    )


def test_on_fail_check_allowed_with_device_fault(
    tango_context, component_manager_mid
):
    component_manager_mid.op_state_model._op_state = DevState.FAULT
    assert component_manager_mid.is_command_allowed("On") is False


def test_on_sdpln_unavailable_task_completed(
    tango_context, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, False
    )
    # include exception in On command
    unavailable_device = MID_SDPSUBARRAY_LEAF_NODE
    component_manager_mid.is_command_allowed("On")
    my_adapter_factory = HelperAdapterFactory()

    unique_id = f"{time.time()}_On"
    task_callback = MockCallable(unique_id)

    on_command = On(component_manager_mid, my_adapter_factory, logger=logger)
    on_command.on_leaf_nodes(logger, task_callback=task_callback)
    assert task_callback.status == TaskStatus.COMPLETED
    assert task_callback.result == (
        ResultCode.FAILED,
        f"{unavailable_device}" + " is not available to receive command",
    )
