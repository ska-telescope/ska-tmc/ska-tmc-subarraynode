import json
import time
from datetime import datetime
from os.path import dirname, join

import mock
import pytest
from ska_control_model import ObsState
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    AdapterType,
    DevFactory,
    DishMode,
    HelperAdapterFactory,
    InvalidJSONError,
    PointingState,
)

from ska_tmc_subarraynode.commands.build_up_data import ElementDeviceDataMid
from ska_tmc_subarraynode.commands.configure_command import Configure
from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.input_validator import ConfigureValidator
from ska_tmc_subarraynode.model.input import InputParameterMid
from tests.settings import (
    DISH_LEAF_NODE_LIST,
    SUBARRAY_LEAF_NODES_MID,
    expected_json_dish_001_036,
    expected_json_dish_100,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_configure_on_dish_device,
    simulate_configure_on_dish_master,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE63,
    DISH_LEAF_NODE100,
    DISH_MASTER_DEVICE1,
    DISH_MASTER_DEVICE36,
    DISH_MASTER_DEVICE63,
    MID_CSPSUBARRAY_LEAF_NODE,
)


def get_configure_input_str(configure_input_file="Configure_mid.json"):
    path = join(
        dirname(__file__), "..", "..", "..", "data", configure_input_file
    )
    with open(path, "r") as f:
        configure_input_str = f.read()
    return configure_input_str


@pytest.fixture
def setup_component_manager(component_manager_mid):
    """Common setup for component_manager_mid."""
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.set_subarray_id(1)
    component_manager_mid.is_command_allowed("Configure")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    return component_manager_mid


def execute_configure(
    component_manager_mid, configure_input_file, task_callback
):
    """Load, modify, and execute the configure command."""
    configure_str = get_configure_input_str(
        configure_input_file=configure_input_file
    )
    config_json = json.loads(configure_str)
    configure_str = json.dumps(config_json)
    return component_manager_mid.configure(configure_str, task_callback)


def test_subarray_configure_partial_json(
    tango_context, task_callback, component_manager_mid
):
    """Test the Configure command with a partial json input."""
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.set_subarray_id(1)
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s",
        len(component_manager_mid.checked_devices),
        elapsed_time,
    )
    logger.info("%s", tango_context)
    component_manager_mid.obs_state_model._straight_to_state("READY")
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )
    component_manager_mid.is_command_allowed("Configure")
    component_manager_mid.initial_obs_state = ObsState.IDLE

    # Partial Configure
    configure_str = get_configure_input_str(
        configure_input_file="Configure_partial_json.json"
    )
    component_manager_mid.configure(configure_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    component_manager_mid.command_in_progress_id = f"{time.time()}-Configure"
    assert component_manager_mid.command_in_progress == "Configure"
    # Wait for tracker thread to start.
    time.sleep(0.5)
    # Simulate dishMode and pointingState events.
    component_manager_mid.update_device_dish_mode(
        DISH_LEAF_NODE, DishMode.OPERATE, datetime.now()
    )
    component_manager_mid.update_device_pointing_state(
        DISH_LEAF_NODE, PointingState.TRACK, datetime.now()
    )
    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.observable.notify_observers(
        attribute_value_change=True
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_invalid_partial_json(
    tango_context, task_callback, component_manager_mid
):
    """Test the Subarray Node rejecting command in case of incorrect partial
    config json."""
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )
    component_manager_mid.set_subarray_id(1)
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.is_command_allowed("Configure")

    configure_str = get_configure_input_str("Configure_partial_json.json")
    config_json = json.loads(configure_str)
    del config_json["pointing"]["target"]

    task_status, message = component_manager_mid.configure(
        json.dumps(config_json), task_callback=task_callback
    )
    assert task_status == TaskStatus.REJECTED
    assert "Malformed input string. Please check the JSON format." in message
    assert (
        "Validation 'Mid TMC configure 2.2' Key 'pointing' error:\n"
        "'Pointing Mid TMC configure 2.2' Missing key: 'target'" in message
    )


def test_subarray_configure_fail_csp_subarray_ln(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.set_subarray_id(1)
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s",
        len(component_manager_mid.checked_devices),
        elapsed_time,
    )
    my_adapter_factory = HelperAdapterFactory()
    failing_dev = MID_CSPSUBARRAY_LEAF_NODE
    attrs = {"Configure.side_effect": Exception}
    skuid = mock.Mock(**attrs)
    my_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SUBARRAY, proxy=skuid
    )
    component_manager_mid.is_command_allowed("Configure")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    logger.info("is allowed successful")
    component_manager_mid.adapter_factory = my_adapter_factory

    configure_str = get_configure_input_str()
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.configure(
        argin=configure_str, task_callback=task_callback
    )

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking Configure in " in result["result"][1]


def test_subarray_configure_fail_dish_subarray_ln(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.set_subarray_id(1)
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s",
        len(component_manager_mid.checked_devices),
        elapsed_time,
    )
    my_adapter_factory = HelperAdapterFactory()
    failing_dev = DISH_LEAF_NODE
    attrs = {"Configure.side_effect": Exception}
    skuid = mock.Mock(**attrs)
    my_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.DISH, proxy=skuid
    )
    component_manager_mid.is_command_allowed("Configure")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    logger.info("is allowed successful")
    component_manager_mid.adapter_factory = my_adapter_factory
    configure_str = get_configure_input_str()
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.configure(
        argin=configure_str, task_callback=task_callback
    )

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert (
        "Error while invoking Configure on Dish Leaf Node "
        in result["result"][1]
    )


@pytest.mark.parametrize(
    "missing_key",
    [
        "pointing",
        "dish",
        "csp",
        "sdp",
        "tmc",
    ],
)
def test_subarray_configure_missing_keys(
    tango_context, task_callback, missing_key: str, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )
    configure_str = get_configure_input_str()
    json_argument = json.loads(configure_str)
    del json_argument[missing_key]
    component_manager_mid.is_command_allowed("Configure")
    task_status, message = component_manager_mid.configure(
        argin=json.dumps(json_argument), task_callback=task_callback
    )
    assert task_status == TaskStatus.REJECTED
    assert f"{missing_key}" in message


def test_subarray_configure_empty_input_json(
    tango_context, task_callback, component_manager_mid
):
    start_time = time.time()
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.set_subarray_id(1)
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s",
        len(component_manager_mid.checked_devices),
        elapsed_time,
    )

    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.is_command_allowed("Configure")
    component_manager_mid.adapter_factory = HelperAdapterFactory()
    res_code, _ = component_manager_mid.configure(
        " ", task_callback=task_callback
    )
    assert res_code == TaskStatus.REJECTED


def test_holography_for_non_mosaic_pattern(
    tango_context, task_callback, component_manager_mid
):
    """Verify that when holography pattern other than mosiac
    provided then command is rejected
    """
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.set_subarray_id(1)
    component_manager_mid.is_command_allowed("Configure")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)
    config_json["pointing"]["groups"][0]["trajectory"]["name"] = "raster"
    configure_str = json.dumps(config_json)
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )
    assert task_status == TaskStatus.REJECTED
    assert "TMC support only mosaic pattern" in message


def test_configure_no_groups(
    tango_context,
    task_callback,
    component_manager_mid,
    setup_component_manager,
):
    """Verify that the command is rejected if there are no groups
    in the configuration."""
    component_manager_mid = setup_component_manager
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)

    # Set the groups list to be empty to simulate no groups
    config_json["pointing"]["groups"] = []
    configure_str = json.dumps(config_json)

    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )

    assert task_status == TaskStatus.REJECTED
    assert "Number of groups cannot be zero" in message


def test_configure_missing_receptors_for_multiple_groups(
    tango_context,
    task_callback,
    component_manager_mid,
    setup_component_manager,
):
    """Verify that the command is rejected if receptors are missing when
    there are multiple groups."""
    component_manager_mid = setup_component_manager
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)

    # Ensure no receptors are provided for the second group
    if len(config_json["pointing"]["groups"]) > 1:
        config_json["pointing"]["groups"][1]["receptors"] = []
    else:
        # If there's only one group, add a second group with empty receptors
        config_json["pointing"]["groups"].append({"receptors": []})

    configure_str = json.dumps(config_json)

    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )

    assert task_status == TaskStatus.REJECTED
    assert "Receptor key is mandatory if there are multiple groups" in message


def test_configure_receptor_assigned_to_multiple_groups(
    tango_context,
    task_callback,
    component_manager_mid,
    setup_component_manager,
):
    """Verify that the command is rejected if a receptor is assigned
    to multiple groups."""
    component_manager_mid = setup_component_manager
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)

    # Assign the same receptor to multiple groups
    config_json["pointing"]["groups"][0]["receptors"] = ["SKA100"]
    config_json["pointing"]["groups"].append(
        {"receptors": ["SKA100"]}
    )  # Duplicate receptor

    configure_str = json.dumps(config_json)

    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )

    assert task_status == TaskStatus.REJECTED
    assert "Receptor SKA100 is already assigned to another group" in message


def test_configure_receptor_not_allocated(
    tango_context,
    task_callback,
    component_manager_mid,
    setup_component_manager,
):
    """Verify that the command is rejected if a receptor is not
    allocated to the subarray."""
    component_manager_mid = setup_component_manager
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)

    # Modify receptors to include an unallocated receptor
    config_json["pointing"]["groups"][0]["receptors"] = ["SK100"]

    # Mock the get_assigned_resources to return an empty list
    component_manager_mid.get_assigned_resources = lambda: []

    configure_str = json.dumps(config_json)

    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )

    assert task_status == TaskStatus.REJECTED
    assert (
        "Request includes receptors not allocated to the subarray" in message
    )


def test_configure_single_group_with_correct_receptors(
    tango_context,
    task_callback,
    component_manager_mid,
    setup_component_manager,
):
    """Verify that the command is accepted if there is a single group with
    correctly assigned receptors."""
    component_manager_mid = setup_component_manager
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)

    # Ensure there's only one group with assigned receptors
    config_json["pointing"]["groups"] = [
        {
            "receptors": ["SKA001", "SKA036"],
            "field": {
                "target_name": "Cen-A",
                "reference_frame": "ICRS",
                "attrs": {"c1": 201.365, "c2": -43.0191667},
            },
        }
    ]

    # Mock the get_assigned_resources to return the receptors
    component_manager_mid.set_assigned_resources(["SKA001", "SKA036"])

    configure_str = json.dumps(config_json)

    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )

    assert task_status == TaskStatus.QUEUED


def test_configure_single_group_without_receptors(
    tango_context,
    task_callback,
    component_manager_mid,
    setup_component_manager,
):
    """Verify that the command is accepted if there is a single group without
    receptors, using assigned resources."""
    component_manager_mid = setup_component_manager
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)

    # Ensure there's only one group without receptors
    config_json["pointing"]["groups"] = [
        {
            "field": {
                "target_name": "Cen-A",
                "reference_frame": "ICRS",
                "attrs": {"c1": 201.365, "c2": -43.0191667},
            },
        }
    ]

    # Mock the get_assigned_resources to return the receptors
    component_manager_mid.get_assigned_resources = lambda: ["SKA001", "SKA100"]

    configure_str = json.dumps(config_json)

    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )

    assert task_status == TaskStatus.QUEUED


def test_configure_multiple_groups_receptors_not_matching(
    tango_context,
    task_callback,
    component_manager_mid,
    setup_component_manager,
):
    """Verify that the command is rejected if multiple groups have receptors
    that don't match assigned resources."""
    component_manager_mid = setup_component_manager
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)

    # Define multiple groups with receptors not in assigned resources
    config_json["pointing"]["groups"] = [
        {"receptors": ["SKA003"], "trajectory": {"name": "mosaic"}},
        {"receptors": ["SKA004"], "trajectory": {"name": "mosaic"}},
    ]

    # Mock the get_assigned_resources to return different receptors
    component_manager_mid.get_assigned_resources = lambda: ["SKA001", "SKA002"]

    configure_str = json.dumps(config_json)

    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )

    assert task_status == TaskStatus.REJECTED
    assert (
        "Request includes receptors not allocated to the subarray" in message
    )


def test_configure_multiple_groups_receptors(
    tango_context,
    task_callback,
    component_manager_mid,
    setup_component_manager,
):
    """Verify that the command is rejected if multiple groups have receptors
    that don't match assigned resources."""
    component_manager_mid = setup_component_manager
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    config_json = json.loads(configure_str)

    # Define multiple groups with receptors not in assigned resources
    config_json["pointing"]["groups"] = [
        {
            "receptors": ["SKA001"],
            "field": {
                "target_name": "Cen-A",
                "reference_frame": "ICRS",
                "attrs": {"c1": 201.365, "c2": -43.0191667},
            },
        },
        {
            "receptors": ["SKA036"],
            "field": {
                "target_name": "Cen-A",
                "reference_frame": "ICRS",
                "attrs": {"c1": 201.365, "c2": -43.0191667},
            },
        },
    ]

    # Mock the get_assigned_resources to return different receptors
    component_manager_mid.get_assigned_resources = lambda: ["SKA001", "SKA036"]

    configure_str = json.dumps(config_json)

    task_status, message = component_manager_mid.configure(
        configure_str, task_callback
    )

    assert task_status == TaskStatus.QUEUED


def test_invalid_pointing_correction_key(
    tango_context, task_callback, component_manager_mid
):
    """
    Test that the TMC MID pointing.correction field only allows the
    values MAINTAIN, UPDATE, and RESET.
    """
    configure_json = get_configure_input_str("Configure_partial_json.json")
    json_argument = json.loads(configure_json)
    # Setting the correction key to an invalid value.
    json_argument["pointing"]["correction"] = "FAILED"
    task_status, message = component_manager_mid.configure(
        json.dumps(json_argument), task_callback
    )
    assert task_status == TaskStatus.REJECTED
    assert (
        "Input should be 'MAINTAIN', 'UPDATE' or 'RESET' [type=enum, "
        + "input_value='FAILED', input_type=str]"
        in message
    )


def test_non_sidereal_tracking_json(
    tango_context, task_callback, component_manager_mid
):
    """Tests the acceptance of Non-sidereal tracking json."""
    cm = component_manager_mid
    configure_validator = ConfigureValidator(cm, logger)
    configure_str = get_configure_input_str("Configure_mid_nonsidereal.json")
    configure_validator.loads(configure_str)

    configure_json = json.loads(configure_str)
    configure_json["pointing"]["target"]["target_name"] = "Abc"
    with pytest.raises(InvalidJSONError) as json_error:
        configure_validator.loads(json.dumps(configure_json))

    assert "Malformed input string. Please check the JSON format." in str(
        json_error.value
    )

    del configure_json["pointing"]["target"]["target_name"]
    with pytest.raises(InvalidJSONError) as json_error:
        configure_validator.loads(json.dumps(configure_json))

    assert "Malformed input string. Please check the JSON format." in str(
        json_error.value
    )


def test_configure_csp_resources_fail_is_allowed(
    tango_context, component_manager_mid
):
    logger.info("%s", tango_context)
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s",
        len(component_manager_mid.checked_devices),
        elapsed_time,
    )
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, False
    )
    component_manager_mid.set_subarray_id(1)
    with pytest.raises(CommandNotAllowed):
        callable = component_manager_mid.command_allowed_callable("Configure")
        callable()


def test_mid_configure_task_completed_holography(
    tango_context,
    task_callback,
    component_manager_mid,
):
    component_manager_mid.command_timeout = 30

    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )

    dish_list = [
        DISH_LEAF_NODE,
        DISH_LEAF_NODE36,
        DISH_LEAF_NODE100,
    ]
    helper_adapter_factory = HelperAdapterFactory()
    dish_mock = []

    for dish in dish_list:
        dish_leaf_node_mock = mock.Mock(
            spec=["dev_name", "Configure", "kValue"]
        )
        dish_leaf_node_mock.dev_name = dish
        dish_leaf_node_mock.unresponsive = False
        dish_leaf_node_mock.Configure = mock.Mock(
            return_value=([ResultCode.QUEUED], [""])
        )
        dish_leaf_node_mock.kValue = 1
        dish_mock.append(dish_leaf_node_mock)

        helper_adapter_factory.get_or_create_adapter(
            dish,
            AdapterType.DISH,
            proxy=dish_leaf_node_mock,
        )
    component_manager_mid.set_assigned_resources(
        ["SKA001", "SKA036", "SKA100"]
    )
    component_manager_mid.set_subarray_id(1)
    component_manager_mid.is_command_allowed("Configure")
    component_manager_mid.adapter_factory = helper_adapter_factory
    component_manager_mid.initial_obs_state = ObsState.IDLE
    configure_str = get_configure_input_str(
        configure_input_file="Configure_holography.json"
    )
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.configure(configure_str, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    component_manager_mid.command_in_progress_id = f"{time.time()}-Configure"
    assert component_manager_mid.command_in_progress == "Configure"

    simulate_configure_on_dish_device(
        component_manager_mid, DISH_LEAF_NODE_LIST
    )
    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
        "Configure",
        ResultCode.OK,
    )

    # Simulate dishMode and pointingState events.
    component_manager_mid.observable.notify_observers(
        attribute_value_change=True
    )

    # Capture and assert the JSON structure in each mock call
    actual_json_dish_001 = json.loads(dish_mock[0].Configure.call_args[0][0])
    actual_json_dish_036 = json.loads(dish_mock[1].Configure.call_args[0][0])
    actual_json_dish_100 = json.loads(dish_mock[2].Configure.call_args[0][0])

    # Assertions for expected JSON structures
    assert actual_json_dish_100 == expected_json_dish_100
    assert actual_json_dish_001["pointing"] == expected_json_dish_001_036
    assert actual_json_dish_036["pointing"] == expected_json_dish_001_036

    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.parametrize(
    "json_input",
    (
        "Configure_mid.json",
        "Configure_holography.json",
    ),
)
def test_mid_configure_task_completed(
    tango_context, task_callback, component_manager_mid, json_input
):
    component_manager_mid.command_timeout = 30
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    if json_input == "Configure_holography.json":
        component_manager_mid.set_assigned_resources(
            ["SKA001", "SKA036", "SKA100"]
        )
    component_manager_mid.set_subarray_id(1)
    component_manager_mid.is_command_allowed("Configure")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    configure_str = get_configure_input_str(configure_input_file=json_input)
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.configure(configure_str, task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    component_manager_mid.command_in_progress_id = f"{time.time()}-Configure"
    assert component_manager_mid.command_in_progress == "Configure"
    simulate_configure_on_dish_master()
    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
        "Configure",
        ResultCode.OK,
    )
    # Simulate dishMode and pointingState events.
    component_manager_mid.set_assigned_resources(["SKA100"])
    component_manager_mid.update_device_dish_mode(
        DISH_LEAF_NODE, DishMode.OPERATE, datetime.now()
    )
    component_manager_mid.update_device_pointing_state(
        DISH_LEAF_NODE, PointingState.TRACK, datetime.now()
    )
    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.observable.notify_observers(
        attribute_value_change=True
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_mid_configure_kvalue_validation_error(
    tango_context, task_callback, component_manager_mid
):
    devFactory = DevFactory()
    proxy_d001 = devFactory.get_device(DISH_LEAF_NODE)
    proxy_d036 = devFactory.get_device(DISH_LEAF_NODE36)
    proxy_d063 = devFactory.get_device(DISH_LEAF_NODE63)
    proxy_d001.kValue = 3
    proxy_d036.kValue = 1
    proxy_d063.kValue = 1

    component_manager_mid.add_device_to_lp(proxy_d036.dev_name())
    component_manager_mid.add_device_to_lp(proxy_d063.dev_name())

    component_manager_mid.input_parameter.dish_dev_names = [
        DISH_MASTER_DEVICE1,
        DISH_MASTER_DEVICE36,
        DISH_MASTER_DEVICE63,
    ]
    component_manager_mid.input_parameter.tmc_dish_ln_device_names = [
        DISH_LEAF_NODE,
        DISH_LEAF_NODE36,
        DISH_LEAF_NODE63,
    ]

    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True
    )
    component_manager_mid.set_subarray_id(1)
    component_manager_mid.initial_obs_state = ObsState.IDLE
    component_manager_mid.is_command_allowed("Configure")
    configure_str = get_configure_input_str()
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    component_manager_mid.configure(configure_str, task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    # component_manager_mid.command_in_progress_id = f"{time.time()}-Configure"
    # assert component_manager_mid.command_in_progress == "Configure"
    simulate_configure_on_dish_master()
    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
        "Configure",
        ResultCode.OK,
    )
    # Simulate dishMode and pointingState events.
    component_manager_mid.update_device_dish_mode(
        DISH_LEAF_NODE, DishMode.OPERATE, datetime.now()
    )
    component_manager_mid.update_device_pointing_state(
        DISH_LEAF_NODE, PointingState.TRACK, datetime.now()
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert (
        "K-values must be either all same or all different."
        in result["result"][1]
    )


def test_mid_configure_kvalue_validation_successful(
    tango_context, task_callback, component_manager_mid
):
    cm = component_manager_mid
    cm.command_timeout = 30
    devFactory = DevFactory()
    proxy_d001 = devFactory.get_device(DISH_LEAF_NODE)
    proxy_d036 = devFactory.get_device(DISH_LEAF_NODE36)
    proxy_d063 = devFactory.get_device(DISH_LEAF_NODE63)
    proxy_d001.kValue = 1
    proxy_d036.kValue = 1
    proxy_d063.kValue = 1

    set_sdp_csp_leaf_node_availability_for_aggregation(cm, True)
    cm.set_subarray_id(1)
    cm.is_command_allowed("Configure")
    cm.initial_obs_state = ObsState.IDLE
    configure_str = get_configure_input_str()
    simulate_obs_state_events(cm, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE])
    cm.obs_state_model._straight_to_state("IDLE")
    cm.configure(configure_str, task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    cm.command_in_progress_id = f"{time.time()}-Configure"
    assert cm.command_in_progress == "Configure"
    simulate_configure_on_dish_master()
    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
        "Configure",
        ResultCode.OK,
    )
    # Simulate dishMode and pointingState events.
    cm.update_device_dish_mode(
        DISH_LEAF_NODE, DishMode.OPERATE, datetime.now()
    )
    cm.update_device_pointing_state(
        DISH_LEAF_NODE, PointingState.TRACK, datetime.now()
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


def test_configure_command_timeout_mid(
    tango_context, task_callback, component_manager_mid
):
    cm = component_manager_mid
    cm.command_timeout = 2
    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )
    simulate_obs_state_events(cm, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE])
    cm.set_subarray_id(1)
    cm.is_command_allowed("Configure")

    configure_str = get_configure_input_str()
    cm.initial_obs_state = ObsState.IDLE
    cm.obs_state_model._straight_to_state("IDLE")
    cm.configure(configure_str, task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Timeout has occurred, command failed" in result["result"][1]


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("EMPTY", "FAULT"),
)
def test_mid_subarray_configure_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_mid
):
    cm = component_manager_mid
    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )
    cm.set_subarray_id(1)
    exception_message = "Configure command not permitted"
    cm.obs_state_model._straight_to_state(not_allowed_obs_state)
    with pytest.raises(StateModelError, match=exception_message):
        callable = cm.command_allowed_callable("Configure")
        callable()


def test_csp_interface_version_skb_509(component_manager_mid):
    configure_str = get_configure_input_str(
        configure_input_file="configure_mid_skb_509.json"
    )
    csp_config_str = get_configure_input_str(
        configure_input_file="csp_configure_mid.json"
    )
    cm = component_manager_mid
    csp_data = ElementDeviceDataMid(json.loads(configure_str), cm)
    path = join(
        dirname(__file__),
        "..",
        "..",
        "..",
        "data",
        "ReceiveAddresses_mid.json",
    )
    with open(path, "r") as f:
        receive_addresses = f.read()
    _, csp_schema = csp_data.build_up_csp_cmd_data(
        json.loads(receive_addresses)
    )
    csp_config_schema = json.loads(csp_schema)
    assert (
        csp_config_schema["interface"]
        == "https://schema.skao.int/ska-csp-configurescan/3.0"
    )
    assert csp_config_schema == json.loads(csp_config_str)
