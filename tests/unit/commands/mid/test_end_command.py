import json
import time

import mock
import pytest
from ska_control_model import ObsState
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import DevFactory, FaultType
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.commands.end_command import End
from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterMid
from src.ska_tmc_subarraynode.model.component import LOGGER
from tests.mock_callable import MockCallable
from tests.settings import (
    DISH_LEAF_NODE_LIST,
    SUBARRAY_LEAF_NODES_MID,
    create_mid_cm,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    simulate_end_on_dish_device,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
)


def get_end_command_obj():
    cm, start_time, obs_state_model = create_mid_cm()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    my_adapter_factory = HelperAdapterFactory()
    end_command = End(cm, obs_state_model, my_adapter_factory, logger=logger)
    return end_command, my_adapter_factory, cm, obs_state_model


def test_mid_subarray_end_fail_csp_subarray_ln(
    tango_context, task_callback, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()
    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    helper_adapter_factory = HelperAdapterFactory()
    # include exception in End command
    failing_dev = MID_CSPSUBARRAY_LEAF_NODE
    attrs = {"End.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SUBARRAY, proxy=CspsubarraylnMock
    )
    simulate_obs_state_events(
        cm,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
    )
    cm.is_command_allowed("End")
    cm.initial_obs_state = ObsState.READY
    cm.obs_state_model._straight_to_state("READY")
    callable = cm.command_allowed_callable("End")
    callable()
    cm.adapter_factory = helper_adapter_factory
    cm.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert (
        "Error while invoking End on CSP Subarray Leaf Node: "
        in result["result"][1]
    )


def test_mid_subarray_end_fail_sdp_subarray_ln(
    tango_context, task_callback, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()
    set_sdp_csp_leaf_node_availability_for_aggregation(cm, True)
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    helper_adapter_factory = HelperAdapterFactory()
    # include exception in End command
    failing_dev = MID_SDPSUBARRAY_LEAF_NODE
    attrs = {"End.side_effect": Exception}
    SdpsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.SUBARRAY, proxy=SdpsubarraylnMock
    )
    simulate_obs_state_events(
        cm,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
    )
    cm.is_command_allowed("End")
    cm.initial_obs_state = ObsState.READY
    cm.obs_state_model._straight_to_state("READY")
    callable = cm.command_allowed_callable("End")
    callable()
    cm.adapter_factory = helper_adapter_factory
    cm.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)

    assert ResultCode.FAILED == result["result"][0]
    assert (
        "Error while invoking End on SDP Subarray Leaf Node "
        in result["result"][1]
    )


def test_mid_subarray_end_fail_dish_ln(
    tango_context, task_callback, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()
    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    helper_adapter_factory = HelperAdapterFactory()
    # include exception in End command
    failing_dev = DISH_LEAF_NODE
    attrs = {"TrackStop.side_effect": Exception}
    dishMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.DISH, proxy=dishMock
    )
    simulate_obs_state_events(
        cm,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
    )
    cm.is_command_allowed("End")
    cm.initial_obs_state = ObsState.READY
    cm.obs_state_model._straight_to_state("READY")
    callable = cm.command_allowed_callable("End")
    callable()
    cm.adapter_factory = helper_adapter_factory
    cm.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)

    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking TrackStop on " in result["result"][1]


def test_mid_subarray_end_fail_is_allowed_device_unresponsive(
    tango_context, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    set_sdp_csp_leaf_node_availability_for_aggregation(cm, False)

    with pytest.raises(CommandNotAllowed):
        callable = cm.command_allowed_callable("End")
        callable()


def test_mid_subarray_end_task_completed(
    tango_context, task_callback, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()

    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )

    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    simulate_obs_state_events(
        cm,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
    )
    cm.is_command_allowed("End")
    cm.initial_obs_state = ObsState.READY
    cm.obs_state_model._straight_to_state("READY")
    callable = cm.command_allowed_callable("End")
    callable()

    cm.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    assert cm.command_in_progress == "End"

    simulate_end_on_dish_device(component_manager_mid, DISH_LEAF_NODE_LIST)

    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.IDLE],
        "END",
        ResultCode.OK,
    )

    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )


def test_mid_subarray_end_fail_not_allowed(
    tango_context, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    cm.op_state_model._op_state = DevState.UNKNOWN
    assert cm.is_command_allowed("End") is False


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("EMPTY", "ABORTED"),
)
def test_mid_subarray_end_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_mid
):
    cm = component_manager_mid
    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )
    exception_message = "End command not permitted"
    cm.obs_state_model._straight_to_state(not_allowed_obs_state)
    with pytest.raises(StateModelError, match=exception_message):
        callable = cm.command_allowed_callable("End")
        callable()


def test_mid_subarray_end_timeout(
    tango_context, task_callback, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()

    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )

    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    simulate_obs_state_events(
        cm,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
    )
    cm.is_command_allowed("End")
    cm.initial_obs_state = ObsState.READY
    cm.obs_state_model._straight_to_state("READY")
    callable = cm.command_allowed_callable("End")
    callable()
    LOGGER.info("timeout %s", cm.command_timeout)

    defect = {
        "enabled": True,
        "fault_type": FaultType.STUCK_IN_INTERMEDIATE_STATE,
        "error_message": "Command stuck in processing",
        "result": ResultCode.FAILED,
        "intermediate_state": ObsState.CONFIGURING,
    }
    csp_sln = DevFactory().get_device(MID_CSPSUBARRAY_LEAF_NODE)
    csp_sln.SetDefective(json.dumps(defect))

    cm.end(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    assert cm.command_in_progress == "End"

    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Timeout has occurred, command failed" == result["result"][1]
    csp_sln.SetDefective(json.dumps({"enabled": False}))
