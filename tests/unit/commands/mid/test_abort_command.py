import json
import time
from datetime import datetime

import mock
import pytest
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import PointingState
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_subarraynode.commands.abort_command import Abort
from ska_tmc_subarraynode.model.input import InputParameterMid
from tests.conftest import get_scan_input_str
from tests.settings import (
    MID_CSPSUBARRAY_LEAF_NODE,
    SUBARRAY_LEAF_NODES_MID,
    create_dish_mocks,
    create_mid_cm,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    setup_mapping_scan_data_for_cm,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE100,
)


def test_mid_abort_command(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    helper_adapter_factory = HelperAdapterFactory()
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.is_command_allowed("Abort")
    component_manager_mid.initial_obs_state = ObsState.IDLE
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    abort_command = Abort(
        component_manager_mid,
        component_manager_mid.obs_state_model,
        helper_adapter_factory,
        logger=logger,
    )
    abort_command.invoke_abort(logger, task_callback, component_manager_mid)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.STARTED, "Command Started"),
    )


def test_mid_subarray_abort_command_fail_on_defective_cspsubarray(
    tango_context, task_callback, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.is_command_allowed("Abort")
    helper_adapter_factory = HelperAdapterFactory()
    # include exception in Abort command
    attrs = {"Abort.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)
    helper_adapter_factory.get_or_create_adapter(
        MID_CSPSUBARRAY_LEAF_NODE,
        AdapterType.CSPSUBARRAY,
        proxy=CspsubarraylnMock,
    )
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.IDLE]
    )
    component_manager_mid.obs_state_model._straight_to_state("IDLE")
    abort_command = Abort(
        component_manager_mid,
        component_manager_mid.obs_state_model,
        helper_adapter_factory,
        logger=logger,
    )
    abort_command.invoke_abort(logger, task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert (
        "Error while invoking Abort command on CSP Subarray Leaf Node"
        in result["result"][1]
    )


def test_mid_subarray_abort_command_invalid_obsstate(
    tango_context, component_manager_mid
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    component_manager_mid.op_state_model._op_state = DevState.FAULT
    assert component_manager_mid.is_command_allowed("Abort") is False


@pytest.mark.parametrize("pointingstate", (0, 1, 2))
def test_mid_abort_with_pointing_states(
    tango_context, task_callback, component_manager_mid, pointingstate
):
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    helper_adapter_factory = HelperAdapterFactory()
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )
    component_manager_mid.update_device_pointing_state(
        DISH_LEAF_NODE, pointingstate, datetime.now()
    )
    component_manager_mid.is_command_allowed("Abort")
    component_manager_mid.initial_obs_state = ObsState.READY
    component_manager_mid.obs_state_model._straight_to_state("READY")
    abort_command = Abort(
        component_manager_mid,
        component_manager_mid.obs_state_model,
        helper_adapter_factory,
        logger=logger,
    )
    abort_command.invoke_abort(logger, task_callback, component_manager_mid)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.STARTED, "Command Started"),
    )


def test_abort_with_mapping_scan(
    tango_context, task_callback, component_manager_mid
):
    """Test Mapping scan is stopped when abort is invoked"""
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )
    dish_list = [
        DISH_LEAF_NODE,
        DISH_LEAF_NODE36,
        DISH_LEAF_NODE100,
    ]
    helper_adapter_factory = HelperAdapterFactory()
    create_dish_mocks(dish_list, helper_adapter_factory)
    component_manager_mid.adapter_factory = helper_adapter_factory
    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.initial_obs_state = ObsState.READY
    setup_mapping_scan_data_for_cm(component_manager_mid)
    component_manager_mid.is_command_allowed("Scan")
    scan_input = get_scan_input_str()
    scan_json = json.loads(scan_input)
    scan_json["scan_ids"] = [1, 2]
    component_manager_mid.scan(json.dumps(scan_json), task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.SCANNING],
        "Scan",
        ResultCode.OK,
    )

    assert component_manager_mid.is_mapping_scan

    # Invoke Abort
    abort_command = Abort(
        component_manager_mid,
        component_manager_mid.obs_state_model,
        helper_adapter_factory,
        logger=logger,
    )
    abort_command.invoke_abort(logger, task_callback, component_manager_mid)

    assert not component_manager_mid.is_mapping_scan
