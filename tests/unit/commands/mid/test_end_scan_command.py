import json
import time
from datetime import datetime

import mock
import pytest
from ska_control_model import ObsState
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterType, DevFactory, DishMode, PointingState
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_subarraynode.exceptions import CommandNotAllowed
from ska_tmc_subarraynode.model.input import InputParameterMid
from tests.conftest import get_scan_input_str
from tests.settings import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE_LIST,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    SUBARRAY_LEAF_NODES_MID,
    create_dish_mocks,
    logger,
    set_sdp_csp_leaf_node_availability_for_aggregation,
    setup_mapping_scan_data_for_cm,
    simulate_configure_on_dish_device,
    simulate_configure_on_dish_master,
    simulate_obs_state_and_result_code_events,
    simulate_obs_state_events,
)


@pytest.mark.parametrize(
    "device_name, adapter_type, devices_in_ready",
    [
        pytest.param(
            MID_CSPSUBARRAY_LEAF_NODE,
            AdapterType.CSPSUBARRAY,
            [MID_SDPSUBARRAY_LEAF_NODE],
        ),
        pytest.param(
            MID_SDPSUBARRAY_LEAF_NODE,
            AdapterType.SDPSUBARRAY,
            [MID_CSPSUBARRAY_LEAF_NODE],
        ),
        pytest.param(
            DISH_LEAF_NODE, AdapterType.DISH, SUBARRAY_LEAF_NODES_MID
        ),
    ],
)
def test_low_subarray_end_scan_fail_on_leaf_nodes(
    tango_context,
    task_callback,
    device_name,
    adapter_type,
    devices_in_ready,
    component_manager_mid,
):
    cm = component_manager_mid
    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )

    helper_adapter_factory = HelperAdapterFactory()
    subarrayleafnode_mock = mock.Mock(
        spec=["dev_name", "EndScan"],
    )
    subarrayleafnode_mock.EndScan = mock.Mock(
        return_value=([ResultCode.FAILED], ["Induced Failure"])
    )
    subarrayleafnode_mock.dev_name = device_name

    helper_adapter_factory.get_or_create_adapter(
        device_name, adapter_type, proxy=subarrayleafnode_mock
    )
    simulate_obs_state_events(
        cm,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.SCANNING],
    )
    cm.is_command_allowed("EndScan")
    cm.initial_obs_state = ObsState.SCANNING
    cm.obs_state_model._straight_to_state("SCANNING")
    cm.adapter_factory = helper_adapter_factory
    cm.end_scan(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    result = task_callback.assert_against_call(status=TaskStatus.COMPLETED)
    assert ResultCode.FAILED == result["result"][0]
    assert "Error while invoking EndScan" in result["result"][1]

    # Verifying that the other devices completed the task successfully
    dev_factory = DevFactory()
    for device in devices_in_ready:
        device_proxy = dev_factory.get_device(device)
        assert device_proxy.obsState == ObsState.READY


def test_mid_subarray_end_scan_fail_is_allowed(
    tango_context, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    set_sdp_csp_leaf_node_availability_for_aggregation(cm, False)

    with pytest.raises(CommandNotAllowed):
        cm.command_allowed_callable("EndScan")()


def test_mid_subarray_end_scan_task_completed(
    tango_context, task_callback, component_manager_mid
):
    cm = component_manager_mid
    start_time = time.time()
    set_sdp_csp_leaf_node_availability_for_aggregation(cm, True)
    elapsed_time = time.time() - start_time
    logger.info(
        "checked %s devices in %s", len(cm.checked_devices), elapsed_time
    )
    simulate_obs_state_events(
        cm,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.SCANNING],
    )
    cm.is_command_allowed("EndScan")
    cm.obs_state_model._straight_to_state("SCANNING")
    cm.end_scan(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    assert cm.command_in_progress == "EndScan"

    simulate_configure_on_dish_device(
        component_manager_mid, DISH_LEAF_NODE_LIST
    )
    simulate_obs_state_and_result_code_events(
        cm,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
        "EndScan",
        ResultCode.OK,
    )

    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )


@pytest.mark.parametrize(
    "not_allowed_obs_state",
    ("EMPTY", "FAULT", "READY"),
)
def test_mid_subarray_end_scan_raises_state_model_exception(
    tango_context, not_allowed_obs_state, component_manager_mid
):
    cm = component_manager_mid
    set_sdp_csp_leaf_node_availability_for_aggregation(
        cm, True, InputParameterMid(None)
    )
    exception_message = "EndScan command not permitted"
    cm.obs_state_model._straight_to_state(not_allowed_obs_state)
    with pytest.raises(StateModelError, match=exception_message):
        cm.command_allowed_callable("EndScan")()


def test_endscan_with_mapping_scan(
    tango_context, task_callback, component_manager_mid
):
    """Test Mapping scan is stopped when end scan is invoked"""
    set_sdp_csp_leaf_node_availability_for_aggregation(
        component_manager_mid, True, InputParameterMid(None)
    )
    simulate_obs_state_events(
        component_manager_mid, SUBARRAY_LEAF_NODES_MID, [ObsState.READY]
    )

    helper_adapter_factory = HelperAdapterFactory()

    component_manager_mid.adapter_factory = helper_adapter_factory
    component_manager_mid.obs_state_model._straight_to_state("READY")
    component_manager_mid.initial_obs_state = ObsState.READY
    setup_mapping_scan_data_for_cm(component_manager_mid)

    component_manager_mid.is_command_allowed("Scan")
    scan_input = get_scan_input_str()
    scan_json = json.loads(scan_input)
    scan_json["scan_ids"] = [1, 2]
    component_manager_mid.scan(json.dumps(scan_json), task_callback)

    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.SCANNING],
        "Scan",
        ResultCode.OK,
    )

    assert component_manager_mid.is_mapping_scan

    component_manager_mid.end_scan(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )

    simulate_configure_on_dish_device(
        component_manager_mid, DISH_LEAF_NODE_LIST
    )
    simulate_obs_state_and_result_code_events(
        component_manager_mid,
        SUBARRAY_LEAF_NODES_MID,
        [ObsState.READY],
        "EndScan",
        ResultCode.OK,
    )
    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )

    assert not component_manager_mid.is_mapping_scan
