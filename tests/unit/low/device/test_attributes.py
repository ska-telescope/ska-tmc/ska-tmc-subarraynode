import pytest
import tango
from ska_tango_base.control_model import (
    ControlMode,
    HealthState,
    SimulationMode,
    TestMode,
)
from tango import DevState
from tango.test_utils import DeviceTestContext

from ska_tmc_subarraynode import release
from ska_tmc_subarraynode.subarray_node_low import LowTmcSubarray


@pytest.fixture
def subarray_node_device(request):
    """Create DeviceProxy for tests"""
    true_context = request.config.getoption("--true-context")
    if not true_context:
        with DeviceTestContext(LowTmcSubarray, process=True) as proxy:
            yield proxy
    else:
        database = tango.Database()
        instance_list = database.get_device_exported_for_class(
            "LowTmcSubarray"
        )
        for instance in instance_list.value_string:
            yield tango.DeviceProxy(instance)
            break


def test_attributes(subarray_node_device):
    assert subarray_node_device.HealthState == HealthState.UNKNOWN
    assert subarray_node_device.State() in [
        DevState.ON,
        DevState.FAULT,
        DevState.UNKNOWN,
    ]
    subarray_node_device.loggingTargets = ["console::cout"]
    assert "console::cout" in subarray_node_device.loggingTargets
    subarray_node_device.testMode = TestMode.NONE
    assert subarray_node_device.testMode == TestMode.NONE
    subarray_node_device.simulationMode = SimulationMode.FALSE
    assert subarray_node_device.testMode == SimulationMode.FALSE
    subarray_node_device.controlMode = ControlMode.REMOTE
    assert subarray_node_device.controlMode == ControlMode.REMOTE
    json_model = subarray_node_device.internalmodel
    assert "subarray_health_state" in json_model
    assert "devices" in json_model
    json_model = subarray_node_device.transformedinternalmodel
    assert "subarray_health_state" in json_model
    assert "devices" not in json_model
    assert subarray_node_device.versionId == release.version
    assert subarray_node_device.buildState == (
        "{},{},{}".format(release.name, release.version, release.description)
    )
