import pytest
from ska_tango_base.control_model import ObsState
from ska_tmc_common.dev_factory import DevFactory
from ska_tmc_common.test_helpers.helper_subarray_device import (
    HelperSubArrayDevice,
)
from ska_tmc_common.test_helpers.helper_subarray_leaf_device import (
    HelperSubarrayLeafDevice,
)

from tests.test_helpers.constants import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    LOW_SUBARRAY_NODE_NAME,
    MCCS_SUBARRAY_LEAF_NODE,
)


@pytest.fixture()
def devices_to_load():
    return (
        {
            "class": HelperSubArrayDevice,
            "devices": [
                {"name": LOW_SUBARRAY_NODE_NAME},
            ],
        },
        {
            "class": HelperSubarrayLeafDevice,
            "devices": [
                {"name": MCCS_SUBARRAY_LEAF_NODE},
                {"name": LOW_CSPSUBARRAY_LEAF_NODE},
                {"name": LOW_SDPSUBARRAY_LEAF_NODE},
            ],
        },
    )


def test_set_obs_state_ok(tango_context):
    # _ = create_cm_no_faulty_devices(
    #     tango_context, True, True, InputParameterLow(None)
    # )
    devFactory = DevFactory()
    proxy = devFactory.get_device(LOW_SUBARRAY_NODE_NAME)
    assert proxy.obsState == ObsState.EMPTY


# def set_obsstate_idle(devFactory, cm, expected_elapsed_time):
#     # Set MCCS Subarray obsState = IDLE
#     proxy_mccs_sa = devFactory.get_device("low-mccs/subarray/01")
#     proxy_mccs_sa.SetDirectObsState(ObsState.IDLE)
#     assert proxy_mccs_sa.obsState == ObsState.IDLE

#     # Set command in progress to AssignResources
#     cm.command_in_progress = "AssignResources"

#     proxy_subarray_node = devFactory.get_device
#       ("ska_low/tm_subarray_node/1")

#     start_time = time.time()
#     elapsed_time = 0
#     while proxy_subarray_node.obsState != ObsState.IDLE:
#         elapsed_time = time.time() - start_time
#         time.sleep(0.1)
#         if elapsed_time > TIMEOUT:
#             pytest.fail("Timeout occurred while executing the test")
#     assert elapsed_time < expected_elapsed_time


# def test_set_obs_state_idle(tango_context):
#     devFactory = DevFactory()
#     cm = create_cm_no_faulty_devices(
#         tango_context, True, True, InputParameterLow(None)
#     )
#     set_obsstate_idle(devFactory, cm, 5)

#     proxy_subarray_node = devFactory.get_device
#      ("ska_low/tm_subarray_node/1")
#     assert proxy_subarray_node.obsState == ObsState.IDLE
