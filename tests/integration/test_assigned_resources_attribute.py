import json
import logging

import pytest
from ska_control_model import ObsState
from tango import DeviceProxy

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)
from tests.settings import MCCSSUBARRAY_NODE_NAME
from tests.test_helpers.common_utils import SubarrayNode

LOGGER = logging.getLogger(__name__)


@pytest.mark.SKA_mid
@pytest.mark.post_deployment
def test_assigned_resources_mid(
    tango_context, subarray_node: SubarrayNode, json_factory, event_recorder
):
    """Test assignedResources attribute."""
    ensure_checked_devices(subarray_node.subarray_node)
    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)
    input_str = json_factory("AssignResources_mid")
    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)
    input = json.loads(input_str)
    receptors = input["dish"]["receptor_ids"]
    assigned_resources = subarray_node.subarray_node.assignedResources
    for receptor in receptors:
        assert receptor in assigned_resources
    release_id = subarray_node.invoke_command("ReleaseAllResources")
    subarray_node.assert_command_completion(release_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)
    assigned_resources = subarray_node.subarray_node.assignedResources
    assert assigned_resources == () or assigned_resources == []


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_assigned_resources_low(
    tango_context, subarray_node: SubarrayNode, json_factory, event_recorder
):
    """Test assignedResources attribute."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)
    input_str = json_factory("AssignResources_low")
    set_mccs_subarray_ln_obsstate(2)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    proxy_mccs_sa = DeviceProxy(MCCSSUBARRAY_NODE_NAME)
    assigned_resources_json = json_factory("AssignedResources_low")
    proxy_mccs_sa.SetDirectassignedResources(assigned_resources_json)
    assert subarray_node.wait_for_subarray_assign_resources(
        assigned_resources_json
    )

    set_mccs_subarray_ln_obsstate(0)
    release_id = subarray_node.invoke_command("ReleaseAllResources")
    subarray_node.assert_command_completion(release_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)

    assigned_resources_json = json_factory("AssignedResources_low_empty")
    proxy_mccs_sa.SetDirectassignedResources(assigned_resources_json)
    assert subarray_node.wait_for_subarray_assign_resources(
        assigned_resources_json
    )
