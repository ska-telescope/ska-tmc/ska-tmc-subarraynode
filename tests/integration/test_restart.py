import pytest
from ska_control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)
from tests.test_helpers.common_utils import SubarrayNode


def restart_command(
    tango_context, subarray_node: SubarrayNode, inputs: dict, event_recorder
):
    """Run the Restart sequence."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    subarray_node.invoke_abort_command(event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.ABORTED)

    restart_id = subarray_node.invoke_command("Restart")
    subarray_node.assert_command_completion(restart_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)


@pytest.mark.parametrize(
    "assign_str",
    [
        pytest.param("AssignResources_mid", marks=[pytest.mark.SKA_mid]),
        pytest.param(
            "AssignResources_low",
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_restart_command(
    tango_context, subarray_node, assign_str, json_factory, event_recorder
):
    """Test Restart command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory(assign_str)
    restart_command(
        tango_context,
        subarray_node,
        inputs,
        event_recorder,
    )
