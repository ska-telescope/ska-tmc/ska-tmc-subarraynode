import json

import pytest
from ska_tango_base.control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
    simulate_configure_on_dish_master,
)
from tests.settings import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE100,
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_ALL_DISH,
    EXCEPTION_DISH,
    TIMEOUT_DEFECT_DISH,
    TIMEOUT_EXCEPTION,
)
from tests.test_helpers.common_utils import SubarrayNode


def configure_command_partial_configuration(
    tango_context,
    subarray_node: SubarrayNode,
    assign_str: str,
    configure_str: str,
    partial_configure_str: str,
    scan_str: str,
    event_recorder,
):
    """Run the Configure Command sequence with multiple partial
    configurations."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    # Full configuration and Scan
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    subarray_node.wait_for_subarray_obsstate(ObsState.CONFIGURING)
    simulate_configure_on_dish_master()
    subarray_node.assert_command_completion(configure_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)

    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.assert_command_completion(scan_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)

    # Partial configuration and Scan
    partial_config_json = json.loads(partial_configure_str)
    OFFSET_1 = partial_config_json["pointing"]["target"]["ca_offset_arcsec"]
    OFFSET_2 = partial_config_json["pointing"]["target"]["ie_offset_arcsec"]

    configure_id = subarray_node.invoke_command(
        "Configure",
        partial_configure_str,
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.CONFIGURING)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.assert_command_completion(scan_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)

    partial_config_json["pointing"]["target"]["ca_offset_arcsec"] = OFFSET_2
    partial_config_json["pointing"]["target"]["ie_offset_arcsec"] = OFFSET_1

    # Second partial configuration and Scan
    configure_str = json.dumps(partial_config_json)
    configure_id = subarray_node.invoke_command(
        "Configure",
        partial_configure_str,
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.CONFIGURING)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.assert_command_completion(scan_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_configure_command_with_partial_configurations(
    tango_context,
    subarray_node,
    json_factory,
    event_recorder,
):
    """Test Configure command with multiple partial configurations."""
    configure_command_partial_configuration(
        tango_context,
        subarray_node,
        json_factory("AssignResources_mid"),
        json_factory("Configure_mid"),
        json_factory("Configure_partial_json"),
        json_factory("Scan_mid"),
        event_recorder,
    )


def configure_command_partial_configuration_dish_error(
    tango_context,
    subarray_node: SubarrayNode,
    assign_str: str,
    configure_str: str,
    partial_configure_str: str,
    scan_str: str,
    event_recorder,
    defective_device_names,
    exception_message,
    defective_params,
):
    """Run the Configure Command sequence with multiple partial
    configurations."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    # Full configuration and Scan
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    subarray_node.wait_for_subarray_obsstate(ObsState.CONFIGURING)
    simulate_configure_on_dish_master()
    subarray_node.assert_command_completion(configure_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)

    scan_id = subarray_node.invoke_command("Scan", scan_str)
    subarray_node.assert_command_completion(scan_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)

    subarray_node.set_devices_defective(
        defective_device_names, defective_params
    )

    configure_id = subarray_node.invoke_command(
        "Configure",
        partial_configure_str,
    )
    subarray_node.assert_command_failure(
        configure_id, exception_message, event_recorder
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.CONFIGURING)
    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "defective_device_names, exception_message, " + "defective_params",
    [
        pytest.param(
            [DISH_LEAF_NODE],
            EXCEPTION_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            [DISH_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT_DISH,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            [DISH_LEAF_NODE, DISH_LEAF_NODE36, DISH_LEAF_NODE100],
            EXCEPTION_ALL_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
    ],
)
@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_configure_command_with_partial_configurations_dish_error(
    tango_context,
    subarray_node,
    json_factory,
    event_recorder,
    defective_device_names,
    exception_message,
    defective_params,
):
    """Test Configure command with multiple partial configurations."""
    configure_command_partial_configuration_dish_error(
        tango_context,
        subarray_node,
        json_factory("AssignResources_mid"),
        json_factory("Configure_mid"),
        json_factory("Configure_partial_json"),
        json_factory("Scan_mid"),
        event_recorder,
        defective_device_names,
        exception_message,
        defective_params,
    )
