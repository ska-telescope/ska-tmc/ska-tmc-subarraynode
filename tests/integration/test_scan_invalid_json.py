import json

import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common import DevFactory, DishMode, PointingState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
    tear_down,
    wait_for_final_subarray_obsstate,
)
from tests.settings import wait_for_attribute_to_change_to
from tests.test_helpers.constants import (
    DISH_MASTER_DEVICE,
    LOW_SUBARRAY_NODE_NAME,
    MID_SUBARRAY_NODE_NAME,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_scan_low_invalid_json(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_low_sdp_csp_leaf_node_availability_for_aggregation,
):
    dev_factory = DevFactory()
    subarray_node = dev_factory.get_device(LOW_SUBARRAY_NODE_NAME)
    ensure_checked_devices(subarray_node)
    (result, unique_id) = subarray_node.On()

    subarray_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=4,
    )

    assign_res_string = json_factory("AssignResources_low")
    configure_string = json_factory("Configure_low")

    set_mccs_subarray_ln_obsstate(2)
    (result, unique_id) = subarray_node.AssignResources(assign_res_string)

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    wait_for_final_subarray_obsstate(subarray_node, ObsState.IDLE)

    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=4,
    )

    set_mccs_subarray_ln_obsstate(4)
    result, unique_id = subarray_node.Configure(configure_string)
    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED

    wait_for_final_subarray_obsstate(subarray_node, ObsState.READY)

    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=6,
    )

    # Test Scan with empty string as scan input
    scan_with_empty_string = ""
    result, message = subarray_node.Scan(scan_with_empty_string)
    assert result[0] == ResultCode.REJECTED
    assert "Malformed input json" in message[0]
    wait_for_attribute_to_change_to(subarray_node, "obsState", ObsState.READY)

    # Test Scan with empty dict as scan input
    scan_with_empty_dict = {}
    with pytest.raises(Exception):
        result, message = subarray_node.Scan(scan_with_empty_dict)
    assert result[0] == ResultCode.REJECTED
    assert "Malformed input json" in message[0]

    wait_for_attribute_to_change_to(subarray_node, "obsState", ObsState.READY)

    scan_input_json = json_factory("Scan_low")
    scan_input_dict = json.loads(scan_input_json)
    del scan_input_dict["scan_id"]

    result, message = subarray_node.Scan(json.dumps(scan_input_dict))
    assert result[0] == ResultCode.REJECTED
    assert "Missing key: 'scan_id'" in message[0]

    wait_for_attribute_to_change_to(subarray_node, "obsState", ObsState.READY)

    tear_down(dev_factory, LOW_SUBARRAY_NODE_NAME)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_scan_mid_invalid_json(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_mid_sdp_csp_leaf_node_availability_for_aggregation,
):
    dev_factory = DevFactory()
    subarray_node = dev_factory.get_device(MID_SUBARRAY_NODE_NAME)
    dish_master = dev_factory.get_device(DISH_MASTER_DEVICE)
    ensure_checked_devices(subarray_node)
    (result, unique_id) = subarray_node.On()

    subarray_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=4,
    )

    assign_res_string = json_factory("AssignResources_mid")
    configure_string = json_factory("Configure_mid")

    (result, unique_id) = subarray_node.AssignResources(assign_res_string)

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    wait_for_final_subarray_obsstate(subarray_node, ObsState.IDLE)

    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=4,
    )

    (result, unique_id) = subarray_node.Configure(configure_string)

    dish_master.SetDirectDishMode(DishMode.OPERATE)
    dish_master.SetDirectPointingState(PointingState.TRACK)

    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED

    wait_for_final_subarray_obsstate(subarray_node, ObsState.READY)

    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=6,
    )

    # Test Scan with empty string as scan input
    scan_with_empty_string = ""
    result, message = subarray_node.Scan(scan_with_empty_string)
    assert result[0] == ResultCode.REJECTED
    assert "Malformed input json" in message[0]

    wait_for_attribute_to_change_to(subarray_node, "obsState", ObsState.READY)

    # Test Scan with empty dict as scan input
    scan_with_empty_dict = {}
    with pytest.raises(Exception):
        result, message = subarray_node.Scan(scan_with_empty_dict)
    assert result[0] == ResultCode.REJECTED
    assert "Malformed input json" in message[0]

    wait_for_attribute_to_change_to(subarray_node, "obsState", ObsState.READY)

    scan_string = json_factory("Scan_mid")
    scan_input_dict = json.loads(scan_string)
    del scan_input_dict["scan_id"]
    result, message = subarray_node.Scan(json.dumps(scan_input_dict))
    assert result[0] == ResultCode.REJECTED
    assert "Malformed input string" in message[0]

    wait_for_attribute_to_change_to(subarray_node, "obsState", ObsState.READY)

    tear_down(dev_factory, MID_SUBARRAY_NODE_NAME)
