import json

import numpy as np
import pytest
from ska_tmc_common.dev_factory import DevFactory

from tests.integration.common import ensure_checked_devices
from tests.settings import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE63,
    DISH_LEAF_NODE100,
    MID_SDPSUBARRAY_NODE_NAME,
)

POINTING_OFFSETS = np.array(
    [
        [
            "SKA001",
            -4.115211938625473,
            69.9725295732531,
            -7.090356031104502,
            104.10028693155607,
            70.1182176899719,
            78.8829949012184,
            95.49061976199042,
            729.5782881970024,
            119.27311545171803,
            1065.4074085647912,
            0.9948872678443994,
            0.8441090109163307,
        ],
        [
            "SKA036",
            -4.115211938625473,
            69.10028693155607,
            -7.5782881970024,
            104.10028693155607,
            70.1182176899719,
            78.8829949012184,
            95.49061976199042,
            729.5782881970024,
            119.27311545171803,
            1065.4074085647912,
            0.9948872678443994,
            0.8441090109163307,
        ],
        [
            "SKA063",
            -4.4074085647912,
            69.9725295732531,
            -7.27311545171803,
            104.10028693155607,
            70.1182176899719,
            78.8829949012184,
            95.49061976199042,
            729.5782881970024,
            119.27311545171803,
            1065.4074085647912,
            0.9948872678443994,
            0.8441090109163307,
        ],
        [
            "SKA100",
            -4.8441090109163307,
            69.9725295732531,
            -7.9948872678443994,
            104.10028693155607,
            70.1182176899719,
            78.8829949012184,
            95.49061976199042,
            729.5782881970024,
            119.27311545171803,
            1065.4074085647912,
            0.9948872678443994,
            0.8441090109163307,
        ],
    ]
)

POINTING_OFFSETS1 = np.array(
    [
        [
            "SKA001",
            -4.115211938625473,
            69.9725295732531,
            -7.090356031104502,
            104.10028693155607,
            70.1182176899719,
            78.8829949012184,
            95.49061976199042,
            729.5782881970024,
            119.27311545171803,
            1065.4074085647912,
            0.9948872678443994,
            0.8441090109163307,
        ],
        [
            "SKA100",
            -4.8441090109163307,
            69.9725295732531,
            -7.9948872678443994,
            104.10028693155607,
            70.1182176899719,
            78.8829949012184,
            95.49061976199042,
            729.5782881970024,
            119.27311545171803,
            1065.4074085647912,
            0.9948872678443994,
            0.8441090109163307,
        ],
    ]
)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_assign_pointing_calibrations(
    subarray_node,
    json_factory,
    event_recorder,
):
    """Run the AssignResources sequence."""
    devfactory = DevFactory()

    ensure_checked_devices(subarray_node.subarray_node)
    subarray_node.devices["DISHLEAFNODE"].extend(
        [DISH_LEAF_NODE36, DISH_LEAF_NODE63, DISH_LEAF_NODE100]
    )
    subarray_node.set_devices_availability(
        [DISH_LEAF_NODE36, DISH_LEAF_NODE63, DISH_LEAF_NODE100]
    )
    input_str = json_factory("AssignResources_mid_sdpqc")
    receive_addr = json_factory("ReceiveAddresses_mid")
    sdp_subarray_proxy = devfactory.get_device(MID_SDPSUBARRAY_NODE_NAME)

    dishln01_proxy = devfactory.get_device(DISH_LEAF_NODE)
    dishln36_proxy = devfactory.get_device(DISH_LEAF_NODE36)
    dishln63_proxy = devfactory.get_device(DISH_LEAF_NODE63)
    dishln100_proxy = devfactory.get_device(DISH_LEAF_NODE100)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    sdp_subarray_proxy.SetDirectreceiveAddresses(receive_addr)

    receive_addr_json = json.loads(receive_addr)
    pointing_cal = receive_addr_json["science_A"]["vis0"]["pointing_cal"]

    subarray_node.wait_for_attribute_to_change_to(
        dishln01_proxy, "sdpQueueConnectorFqdn", pointing_cal
    )
    subarray_node.wait_for_attribute_to_change_to(
        dishln36_proxy, "sdpQueueConnectorFqdn", pointing_cal
    )
    subarray_node.wait_for_attribute_to_change_to(
        dishln63_proxy, "sdpQueueConnectorFqdn", pointing_cal
    )
    subarray_node.wait_for_attribute_to_change_to(
        dishln100_proxy, "sdpQueueConnectorFqdn", pointing_cal
    )

    assert dishln01_proxy.sdpQueueConnectorFqdn == pointing_cal
    assert dishln36_proxy.sdpQueueConnectorFqdn == pointing_cal
    assert dishln63_proxy.sdpQueueConnectorFqdn == pointing_cal
    assert dishln100_proxy.sdpQueueConnectorFqdn == pointing_cal

    # Another check with updated receiveAddresses and pointing_offsets.
    receive_addr = json_factory("ReceiveAddresses_mid")
    receive_addr = json.loads(receive_addr)
    receive_addr["target:a"]["pss2"]["search_beam_id"] = 2
    sdp_subarray_proxy.SetDirectreceiveAddresses(json.dumps(receive_addr))
    pointing_cal = receive_addr["target:a"]["vis0"]["pointing_cal"]

    subarray_node.wait_for_attribute_to_change_to(
        dishln01_proxy, "sdpQueueConnectorFqdn", pointing_cal
    )
    subarray_node.wait_for_attribute_to_change_to(
        dishln36_proxy, "sdpQueueConnectorFqdn", pointing_cal
    )
    subarray_node.wait_for_attribute_to_change_to(
        dishln63_proxy, "sdpQueueConnectorFqdn", pointing_cal
    )
    subarray_node.wait_for_attribute_to_change_to(
        dishln100_proxy, "sdpQueueConnectorFqdn", pointing_cal
    )

    assert dishln01_proxy.sdpQueueConnectorFqdn == pointing_cal
    assert dishln36_proxy.sdpQueueConnectorFqdn == pointing_cal
    assert dishln63_proxy.sdpQueueConnectorFqdn == pointing_cal
    assert dishln100_proxy.sdpQueueConnectorFqdn == pointing_cal
