import pytest

from tests.integration.common import ensure_checked_devices
from tests.test_helpers.common_utils import SubarrayNode


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_set_admin_mode_command(
    tango_context, subarray_node: SubarrayNode, event_recorder, json_factory
):
    """Run the AssignResources sequence."""
    subarray_node.subarray_node.isAdminModeEnabled = True
    ensure_checked_devices(subarray_node.subarray_node)

    input_str = json_factory("AdminMode_devices")

    set_admin_mode_id = subarray_node.invoke_command("SetAdminMode", input_str)

    subarray_node.assert_command_completion(set_admin_mode_id, event_recorder)
    subarray_node.subarray_node.isAdminModeEnabled = False
