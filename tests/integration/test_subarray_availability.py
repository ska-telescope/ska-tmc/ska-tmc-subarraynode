import time

import pytest
from ska_tango_base.commands import ResultCode

from tests.integration.common import ensure_checked_devices
from tests.settings import TIMEOUT, logger
from tests.test_helpers.common_utils import SubarrayNode
from tests.test_helpers.constants import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE2,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
)


def wait_for_availability_transition(subarray_node, expected_status):
    """
    This method will wait till subarray node transitions to expected_status
    till timeout has been reached .
    """
    start_time = time.time()
    elapsed_time = 0

    while subarray_node.isSubarrayAvailable != expected_status:
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
        if elapsed_time > TIMEOUT:
            pytest.fail(
                "Timeout occurred checking the SubarrayNode availability."
            )

    logger.info(
        "Current subarray_node.isSubarrayAvailable"
        + f"is set to :: {subarray_node.isSubarrayAvailable}"
    )
    assert subarray_node.isSubarrayAvailable is expected_status


@pytest.mark.parametrize(
    "",
    [
        pytest.param(marks=[pytest.mark.SKA_mid]),
        pytest.param(
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_subarraynode_availability_status(subarray_node):
    """Test availability aggregation on Subarray Node"""
    ensure_checked_devices(subarray_node.subarray_node)

    if subarray_node.deployment == "LOW":
        device_list = [LOW_CSPSUBARRAY_LEAF_NODE, LOW_SDPSUBARRAY_LEAF_NODE]
    else:
        device_list = [
            MID_CSPSUBARRAY_LEAF_NODE,
            MID_SDPSUBARRAY_LEAF_NODE,
        ]

    subarray_node.wait_for_attribute_to_change_to(
        subarray_node.subarray_node, "isSubarrayAvailable", True
    )
    logger.info("Verified device availability to be True")

    subarray_node.set_devices_availability(device_list, False)
    subarray_node.wait_for_attribute_to_change_to(
        subarray_node.subarray_node, "isSubarrayAvailable", False
    )
    logger.info(
        "Verified device availability to be False when all devices report "
        + "False availability"
    )

    subarray_node.set_devices_availability(device_list, True)
    subarray_node.wait_for_attribute_to_change_to(
        subarray_node.subarray_node, "isSubarrayAvailable", True
    )

    for device in device_list:
        subarray_node.set_devices_availability([device], False)
        subarray_node.wait_for_attribute_to_change_to(
            subarray_node.subarray_node, "isSubarrayAvailable", False
        )
        logger.info(
            "Verified device availability to be False when %s reports "
            + "False availability",
            device,
        )

        subarray_node.set_devices_availability([device], True)
        subarray_node.wait_for_attribute_to_change_to(
            subarray_node.subarray_node, "isSubarrayAvailable", True
        )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_subarray_dish_availability(
    tango_context, subarray_node: SubarrayNode, json_factory, event_recorder
):
    """This test case will perform testing around inclusion
    of dishes in availability calculation.It will follow below rule.
    Dish availability is only considered after the Assign command is completed
    hence even though  dish devices are unavailable before assign ,
    it will not impact subarray node availability .
    But once assign command is completed , if dish device
    is not available , it will impact subarry node
    availability which will be set to false
    """

    ensure_checked_devices(subarray_node.subarray_node)
    subarray_node_name = subarray_node.subarray_node

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    subarray_node.set_devices_availablity({DISH_LEAF_NODE: False})
    wait_for_availability_transition(subarray_node_name, True)

    assign_json = json_factory("AssignResources_mid")
    assign_id = subarray_node.invoke_command("AssignResources", assign_json)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    # Dish availability is only considered after the Assign command
    # Hence due to abscence of dish master ,
    # subarray node availablity will be false
    wait_for_availability_transition(subarray_node_name, False)
    exception_str = (
        "Exception from 'is_cmd_allowed' method: Command not allowed due to "
        + "unavailabity of following devices ['ska001/elt/master']"
    )
    release_id = subarray_node.invoke_command("ReleaseAllResources")
    subarray_node.assert_command_failure(
        release_id,
        exception_str,
        event_recorder,
        result_code=ResultCode.REJECTED,
    )
    subarray_node.set_devices_availablity({DISH_LEAF_NODE: True})
    wait_for_availability_transition(subarray_node_name, True)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_sn_availability_false_with_multiple_dishes(
    tango_context, subarray_node: SubarrayNode, json_factory, event_recorder
):
    """This test case will test availability aggregation with 2 dish devices.
    One of the dish availability will be True and another will be false.
    It is expected that subarray will show availability as False in this case
    """
    ensure_checked_devices(subarray_node.subarray_node)
    subarray_node_name = subarray_node.subarray_node

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    set_availability = {DISH_LEAF_NODE2: True, DISH_LEAF_NODE: False}
    subarray_node.set_devices_availablity(set_availability)

    wait_for_availability_transition(subarray_node_name, True)

    assign_json = json_factory("AssignResources_mid_multiple_dish")
    assign_id = subarray_node.invoke_command("AssignResources", assign_json)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    wait_for_availability_transition(subarray_node_name, False)

    exception_str = (
        "Exception from 'is_cmd_allowed' method: Command not allowed due to "
        + "unavailabity of following devices ['ska001/elt/master']"
    )
    release_id = subarray_node.invoke_command("ReleaseAllResources")
    subarray_node.assert_command_failure(
        release_id,
        exception_str,
        event_recorder,
        result_code=ResultCode.REJECTED,
    )

    assert subarray_node_name.isSubarrayAvailable is False

    set_availability = {DISH_LEAF_NODE2: True, DISH_LEAF_NODE: True}
    subarray_node.set_devices_availablity(set_availability)
    wait_for_availability_transition(subarray_node_name, True)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_sn_availability_true_with_multiple_dishes(
    tango_context, subarray_node: SubarrayNode, json_factory, event_recorder
):
    """This test case will test availability aggregation with 2 dish devices.
    Both of the dish availability will be True .
    It is expected that subarray will show availability as True in this case
    """
    ensure_checked_devices(subarray_node.subarray_node)
    subarray_node_name = subarray_node.subarray_node

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    set_availability = {DISH_LEAF_NODE2: True, DISH_LEAF_NODE: True}
    subarray_node.set_devices_availablity(set_availability)
    wait_for_availability_transition(subarray_node_name, True)

    assign_json = json_factory("AssignResources_mid_multiple_dish")
    assign_id = subarray_node.invoke_command("AssignResources", assign_json)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    wait_for_availability_transition(subarray_node_name, True)
