import pytest
from ska_tango_base.control_model import ObsState

from tests.integration.common import ensure_checked_devices
from tests.test_helpers.common_utils import SubarrayNode


def configure(
    tango_context, subarray_node: SubarrayNode, inputs: dict, event_recorder
):
    """Run the Configure sequence."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)
    exception_message = "Processing Region config 4.0' Key 'fsp_ids' error"
    subarray_node.invoke_and_assert_command_rejected(
        "Configure", exception_message, input_str=inputs["CONFIGURE"]
    )
    subarray_node.assert_obs_state(ObsState.IDLE)


@pytest.mark.parametrize(
    "assign_str, configure_str",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid_invalid_fsp_id",
            marks=[pytest.mark.SKA_mid],
        ),
    ],
)
@pytest.mark.post_deployment
def test_configure_command_mid_invalid_fsp_id(
    tango_context,
    subarray_node,
    assign_str,
    configure_str,
    json_factory,
    event_recorder,
):
    """Test Configure command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory(assign_str)
    inputs["CONFIGURE"] = json_factory(configure_str)
    configure(
        tango_context,
        subarray_node,
        inputs,
        event_recorder,
    )
