import pytest
from ska_control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
    simulate_configure_on_dish_master,
)
from tests.settings import (
    DISH_LEAF_NODE,
    DISH_LEAF_NODE36,
    DISH_LEAF_NODE100,
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_ALL_DISH,
    EXCEPTION_DISH,
    EXCEPTION_LOW_CSP,
    EXCEPTION_LOW_CSP_SDP,
    EXCEPTION_LOW_SDP,
    EXCEPTION_MCCS,
    EXCEPTION_MID_CSP,
    EXCEPTION_MID_SDP,
    FAILED_RESULT_DEFECT,
    FAILED_RESULT_EXCEPTION,
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MCCS_SUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    TIMEOUT_DEFECT,
    TIMEOUT_DEFECT_DISH,
    TIMEOUT_EXCEPTION,
)
from tests.test_helpers.common_utils import SubarrayNode


def end_command(
    tango_context, subarray_node: SubarrayNode, inputs: dict, event_recorder
):
    """Run the End sequence."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(2)
    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    configure_id = subarray_node.invoke_command(
        "Configure", inputs["CONFIGURE"]
    )
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    end_id = subarray_node.invoke_command("End")
    subarray_node.assert_command_completion(end_id, event_recorder)

    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)


@pytest.mark.parametrize(
    "assign_str, configure_str",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_end_command(
    tango_context,
    subarray_node,
    assign_str,
    configure_str,
    json_factory,
    event_recorder,
):
    """Test End command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory(assign_str)
    inputs["CONFIGURE"] = json_factory(configure_str)
    end_command(
        tango_context,
        subarray_node,
        inputs,
        event_recorder,
    )


def end_command_failed_on_subsystem_before_queueing(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    assign_str: str,
    configure_str: str,
    event_recorder,
):
    """Run Configure sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    subarray_node.set_devices_defective(
        defective_device_names, FAILED_RESULT_DEFECT
    )
    end_id = subarray_node.invoke_command("End")
    subarray_node.assert_command_failure(
        end_id, FAILED_RESULT_EXCEPTION, event_recorder
    )
    subarray_node.reset_defects_for_devices(defective_device_names)


def end_command_exception_handling(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    assign_str: str,
    configure_str: str,
    event_recorder,
    defect_param: str,
    exception_message: str,
):
    """Run End sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command("Configure", configure_str)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.wait_for_subarray_obsstate(ObsState.READY)
    subarray_node.assert_command_completion(configure_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, defect_param)
    end_id = subarray_node.invoke_command("End")
    subarray_node.assert_command_failure(
        end_id, exception_message, event_recorder
    )
    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "assign_str, configure_str, defective_device_names,",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_end_command_failed_on_subsystem_before_queueing(
    tango_context,
    subarray_node,
    defective_device_names,
    assign_str,
    configure_str,
    json_factory,
    event_recorder,
):
    """Test Configure command with defective devices
    for Timeout and Error Propagation."""
    end_command_failed_on_subsystem_before_queueing(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(assign_str),
        json_factory(configure_str),
        event_recorder,
    )


@pytest.mark.parametrize(
    "assign_str, configure_str, defective_device_names,exception_message, defective_params",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [DISH_LEAF_NODE, DISH_LEAF_NODE36, DISH_LEAF_NODE100],
            EXCEPTION_ALL_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [DISH_LEAF_NODE],
            EXCEPTION_DISH,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [DISH_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT_DISH,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            EXCEPTION_MCCS,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [MCCS_SUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            [LOW_SDPSUBARRAY_LEAF_NODE, LOW_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_CSP_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_end_command_exception_handling(
    tango_context,
    subarray_node,
    defective_device_names,
    assign_str,
    configure_str,
    json_factory,
    event_recorder,
    defective_params,
    exception_message,
):
    """Test End command with defective devices
    for Timeout and Error Propagation."""
    end_command_exception_handling(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(assign_str),
        json_factory(configure_str),
        event_recorder,
        defective_params,
        exception_message,
    )
