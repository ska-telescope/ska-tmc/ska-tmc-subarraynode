import pytest
from ska_tango_base.control_model import AdminMode, HealthState
from tango import DeviceProxy

from tests.integration.common import ensure_checked_devices
from tests.settings import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_CSPSUBARRAY_NODE_NAME,
    LOW_SDPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_NODE_NAME,
    MCCS_SUBARRAY_LEAF_NODE,
    MCCSSUBARRAY_NODE_NAME,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
    logger,
)
from tests.test_helpers.common_utils import SubarrayNode


@pytest.mark.parametrize(
    "csp_sa_healthstate, sdp_sa_healthstate, mccs_sa_healthstate, \
    tmc_sa_healthstate",
    [
        pytest.param(
            HealthState.OK,
            HealthState.OK,
            HealthState.OK,
            HealthState.OK,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            HealthState.UNKNOWN,
            HealthState.OK,
            HealthState.OK,
            HealthState.UNKNOWN,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            HealthState.OK,
            HealthState.FAILED,
            HealthState.OK,
            HealthState.FAILED,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            HealthState.OK,
            HealthState.FAILED,
            HealthState.DEGRADED,
            HealthState.FAILED,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            HealthState.OK,
            HealthState.OK,
            HealthState.DEGRADED,
            HealthState.DEGRADED,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_low_subarray_health_state_aggregation(
    tango_context,
    subarray_node: SubarrayNode,
    csp_sa_healthstate,
    sdp_sa_healthstate,
    mccs_sa_healthstate,
    tmc_sa_healthstate,
):
    ensure_checked_devices(subarray_node.subarray_node)

    low_cspsubarray_proxy = DeviceProxy(LOW_CSPSUBARRAY_NODE_NAME)
    low_sdpsubarray_proxy = DeviceProxy(LOW_SDPSUBARRAY_NODE_NAME)
    mccssubarray_proxy = DeviceProxy(MCCSSUBARRAY_NODE_NAME)

    low_cspsubarray_proxy.SetDirectHealthState(csp_sa_healthstate)
    low_sdpsubarray_proxy.SetDirectHealthState(sdp_sa_healthstate)
    mccssubarray_proxy.SetDirectHealthState(mccs_sa_healthstate)

    subarray_node.wait_for_subarray_healthstate(tmc_sa_healthstate)
    subarray_node.assert_health_state(tmc_sa_healthstate)


@pytest.mark.SKA_low
@pytest.mark.post_deployment
def test_low_subarray_admin_mode_health_state_aggregation(
    tango_context,
    subarray_node: SubarrayNode,
):
    ensure_checked_devices(subarray_node.subarray_node)

    subarray_node.subarray_node.isAdminModeEnabled = True

    low_cspsubarray_proxy = DeviceProxy(LOW_CSPSUBARRAY_LEAF_NODE)
    low_sdpsubarray_proxy = DeviceProxy(LOW_SDPSUBARRAY_LEAF_NODE)
    low_mccssubarray_proxy = DeviceProxy(MCCS_SUBARRAY_LEAF_NODE)
    # Set all devices offline
    low_cspsubarray_proxy.SetCspSubarrayLeafNodeAdminMode(AdminMode.OFFLINE)
    low_sdpsubarray_proxy.SetSdpSubarrayLeafNodeAdminMode(AdminMode.OFFLINE)
    low_mccssubarray_proxy.SetMccsSubarrayLeafNodeAdminMode(AdminMode.OFFLINE)

    low_mccssubarray_proxy.SetMccsSubarrayLeafNodeAdminMode(AdminMode.ONLINE)
    # Check health state degraded when one of the device Offline
    subarray_node.wait_for_subarray_healthstate(HealthState.DEGRADED)
    low_cspsubarray_proxy.SetCspSubarrayLeafNodeAdminMode(AdminMode.ONLINE)
    low_sdpsubarray_proxy.SetSdpSubarrayLeafNodeAdminMode(AdminMode.ONLINE)

    subarray_node.wait_for_subarray_healthstate(HealthState.OK)
    subarray_node.assert_health_state(HealthState.OK)


@pytest.mark.SKA_mid
@pytest.mark.post_deployment
def test_mid_subarray_admin_mode_health_state_aggregation(
    tango_context,
    subarray_node: SubarrayNode,
):
    ensure_checked_devices(subarray_node.subarray_node)

    subarray_node.subarray_node.isAdminModeEnabled = True

    mid_cspsubarray_proxy = DeviceProxy(MID_CSPSUBARRAY_LEAF_NODE)
    mid_sdpsubarray_proxy = DeviceProxy(MID_SDPSUBARRAY_LEAF_NODE)
    # Set all devices offline
    mid_cspsubarray_proxy.SetCspSubarrayLeafNodeAdminMode(AdminMode.OFFLINE)
    mid_sdpsubarray_proxy.SetSdpSubarrayLeafNodeAdminMode(AdminMode.OFFLINE)

    mid_cspsubarray_proxy.SetCspSubarrayLeafNodeAdminMode(AdminMode.ONLINE)
    # Check health state degraded when one of the device Offline
    subarray_node.wait_for_subarray_healthstate(HealthState.DEGRADED)
    mid_sdpsubarray_proxy.SetSdpSubarrayLeafNodeAdminMode(AdminMode.ONLINE)

    # when both all admin mode is ONLINE then health state is OK
    subarray_node.wait_for_subarray_healthstate(HealthState.OK)
    subarray_node.assert_health_state(HealthState.OK)
