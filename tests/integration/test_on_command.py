import json

import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory

from tests.integration.common import (  # noqa F401
    devices_to_load,
    ensure_checked_devices,
)
from tests.test_helpers.constants import (
    LOW_SUBARRAY_NODE_NAME,
    MID_SUBARRAY_NODE_NAME,
)


def on(
    tango_context,
    change_event_callbacks,
    subarray_node_name,
    set_mid_sdp_csp_leaf_node_availability_for_aggregation,
):
    dev_factory = DevFactory()
    subarray_node = dev_factory.get_device(subarray_node_name)
    ensure_checked_devices(subarray_node)

    result, unique_id = subarray_node.On()

    assert unique_id[0].endswith("On")
    assert result[0] == ResultCode.QUEUED

    subarray_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks.assert_change_event(
        "longRunningCommandResult",
        (unique_id[0], json.dumps((int(ResultCode.OK), "Command Completed"))),
        lookahead=2,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
@pytest.mark.parametrize(
    "subarray_node_name",
    [(MID_SUBARRAY_NODE_NAME)],
)
def test_on_command_mid(
    tango_context,
    change_event_callbacks,
    subarray_node_name,
    set_mid_sdp_csp_leaf_node_availability_for_aggregation,
):
    return on(
        tango_context,
        change_event_callbacks,
        subarray_node_name,
        set_mid_sdp_csp_leaf_node_availability_for_aggregation,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
@pytest.mark.parametrize(
    "subarray_node_name",
    [(LOW_SUBARRAY_NODE_NAME)],
)
def test_on_command_low(
    tango_context,
    change_event_callbacks,
    subarray_node_name,
    set_low_sdp_csp_leaf_node_availability_for_aggregation,
):
    return on(
        tango_context,
        change_event_callbacks,
        subarray_node_name,
        set_low_sdp_csp_leaf_node_availability_for_aggregation,
    )
