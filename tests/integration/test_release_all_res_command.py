import pytest
from ska_control_model import ObsState

from tests.conftest import (
    LOW_CSPSUBARRAY_LEAF_NODE,
    LOW_SDPSUBARRAY_LEAF_NODE,
    MID_CSPSUBARRAY_LEAF_NODE,
    MID_SDPSUBARRAY_LEAF_NODE,
)
from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
)
from tests.settings import (
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_LOW_CSP,
    EXCEPTION_LOW_CSP_SDP,
    EXCEPTION_LOW_SDP,
    EXCEPTION_MID_CSP,
    EXCEPTION_MID_CSP_SDP,
    EXCEPTION_MID_SDP,
    FAILED_RESULT_DEFECT,
    FAILED_RESULT_EXCEPTION,
    TIMEOUT_DEFECT,
    TIMEOUT_EXCEPTION,
)
from tests.test_helpers.common_utils import SubarrayNode


def release_all_resources_command(
    tango_context, subarray_node: SubarrayNode, input_str: str, event_recorder
):
    """Run the ReleaseResources sequence."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(2)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(0)
    release_id = subarray_node.invoke_command("ReleaseAllResources")
    subarray_node.assert_command_completion(release_id, event_recorder)
    subarray_node.wait_for_subarray_obsstate(ObsState.EMPTY)


@pytest.mark.parametrize(
    "json_used",
    [
        pytest.param("AssignResources_mid", marks=[pytest.mark.SKA_mid]),
        pytest.param(
            "AssignResources_low",
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_release_resources(
    tango_context, subarray_node, json_used, json_factory, event_recorder
):
    """Test ReleaseResources command."""
    release_all_resources_command(
        tango_context,
        subarray_node,
        json_factory(json_used),
        event_recorder,
    )


def release_all_resources_with_exception(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    defect_param: str,
    input_str: str,
    exception_message: str,
    event_recorder,
):
    """Run the ReleaseResources sequence with defective devices."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(2)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    subarray_node.set_devices_defective(defective_device_names, defect_param)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(0)
    release_id = subarray_node.invoke_command("ReleaseAllResources")
    subarray_node.assert_command_failure(
        release_id, exception_message, event_recorder
    )
    subarray_node.assert_obs_state(ObsState.RESOURCING)

    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "json_used, defective_device_names, exception_message, defective_params",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE, MID_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_MID_CSP_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_CSP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            TIMEOUT_EXCEPTION,
            TIMEOUT_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_SDPSUBARRAY_LEAF_NODE, LOW_CSPSUBARRAY_LEAF_NODE],
            EXCEPTION_LOW_CSP_SDP,
            ERROR_PROPAGATION_DEFECT,
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_release_all_resources_exception(
    tango_context,
    subarray_node,
    defective_device_names,
    defective_params,
    json_used,
    json_factory,
    exception_message,
    event_recorder,
):
    """Test ReleaseResources command with defective devices
    for Timeout and Error Propagation."""
    release_all_resources_with_exception(
        tango_context,
        subarray_node,
        defective_device_names,
        defective_params,
        json_factory(json_used),
        exception_message,
        event_recorder,
    )


def releaseallresources_command_failed_on_subsystem(
    tango_context,
    subarray_node: SubarrayNode,
    defective_device_names: list,
    input_str: str,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)
    assign_id = subarray_node.invoke_command("AssignResources", input_str)
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(ObsState.IDLE)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    subarray_node.set_devices_defective(
        defective_device_names, FAILED_RESULT_DEFECT
    )
    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(0)
    release_id = subarray_node.invoke_command("ReleaseAllResources")
    subarray_node.assert_command_failure(
        release_id, FAILED_RESULT_EXCEPTION, event_recorder
    )

    subarray_node.reset_defects_for_devices(defective_device_names)


@pytest.mark.parametrize(
    "json_used, defective_device_names",
    [
        pytest.param(
            "AssignResources_mid",
            [MID_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_CSPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            "AssignResources_mid",
            [MID_SDPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            [LOW_SDPSUBARRAY_LEAF_NODE],
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_releaseallresources_command_failed_on_subsystem(
    tango_context,
    subarray_node,
    defective_device_names,
    json_used,
    json_factory,
    event_recorder,
):
    releaseallresources_command_failed_on_subsystem(
        tango_context,
        subarray_node,
        defective_device_names,
        json_factory(json_used),
        event_recorder,
    )
