import time

import pytest
from ska_tango_base.control_model import HealthState, ObsState

from tests.integration.common import (
    ensure_checked_devices,
    simulate_configure_on_multiple_dish_master,
)
from tests.settings import check_k_values_set, set_k_values
from tests.test_helpers.common_utils import SubarrayNode


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_configure_kValue_error_scenario(
    tango_context,
    subarray_node: SubarrayNode,
    json_factory,
    event_recorder,
):
    ensure_checked_devices(subarray_node.subarray_node)
    set_k_values([1, 1, 3])
    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command(
        "AssignResources", json_factory("AssignResources_mid_kValue")
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)
    configure_id = subarray_node.invoke_command(
        "Configure", json_factory("Configure_mid")
    )
    if subarray_node.deployment == "MID":
        simulate_configure_on_multiple_dish_master()
    exception_message = (
        "Configure command failed: "
        + "Error while invoking Configure command: "
        + "K-values must be either all same or all different."
    )
    subarray_node.assert_command_failure(
        configure_id, exception_message, event_recorder
    )
    subarray_node.wait_for_subarray_healthstate(HealthState.DEGRADED)
    subarray_node.assert_health_state(HealthState.DEGRADED)
    subarray_node.wait_for_subarray_obsstate(ObsState.IDLE)
    # reset the k-values
    set_k_values([0, 0, 0])
    assert check_k_values_set([0, 0, 0])


def configure_command(
    tango_context,
    subarray_node: SubarrayNode,
    assign_str: str,
    configure_str: str,
    event_recorder,
    kValues,
):
    """Run the Configure Command sequence."""
    ensure_checked_devices(subarray_node.subarray_node)
    set_k_values(kValues)
    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    assign_id = subarray_node.invoke_command("AssignResources", assign_str)
    subarray_node.assert_command_completion(assign_id, event_recorder)

    configure_id = subarray_node.invoke_command("Configure", configure_str)
    if subarray_node.deployment == "MID":
        simulate_configure_on_multiple_dish_master()

    subarray_node.assert_command_completion(configure_id, event_recorder)
    subarray_node.wait_for_subarray_healthstate(HealthState.OK)
    subarray_node.assert_health_state(HealthState.OK)
    set_k_values([0, 0, 0])
    assert check_k_values_set([0, 0, 0])


@pytest.mark.parametrize(
    "assign_input, configure_input, kValues",
    [
        pytest.param(
            "AssignResources_mid_kValue",
            "Configure_mid",
            [1, 2, 3],
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_mid_kValue",
            "Configure_mid",
            [1, 1, 1],
            marks=[pytest.mark.SKA_mid],
        ),
    ],
)
@pytest.mark.post_deployment
def test_configure_command(
    tango_context,
    subarray_node,
    assign_input,
    configure_input,
    json_factory,
    event_recorder,
    kValues,
):
    """Test Configure command."""
    configure_command(
        tango_context,
        subarray_node,
        json_factory(assign_input),
        json_factory(configure_input),
        event_recorder,
        kValues,
    )
