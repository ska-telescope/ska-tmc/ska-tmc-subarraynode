import json

import pytest
from ska_tango_base.control_model import ObsState

from tests.integration.common import (
    ensure_checked_devices,
    set_mccs_subarray_ln_obsstate,
    simulate_configure_on_dish_master,
)
from tests.test_helpers.common_utils import SubarrayNode


def multi_configure(
    tango_context, subarray_node: SubarrayNode, inputs: dict, event_recorder
):
    """Run the multi-Configure sequence."""
    ensure_checked_devices(subarray_node.subarray_node)

    on_id = subarray_node.invoke_command("On")
    subarray_node.assert_command_completion(on_id, event_recorder)

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(2)
    assign_id = subarray_node.invoke_command(
        "AssignResources", inputs["ASSIGN"]
    )
    subarray_node.assert_command_completion(assign_id, event_recorder)

    configure1_string = inputs["CONFIGURE"]

    if subarray_node.deployment == "LOW":
        set_mccs_subarray_ln_obsstate(4)
    configure_id = subarray_node.invoke_command("Configure", configure1_string)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.assert_command_completion(configure_id, event_recorder)

    if subarray_node.deployment == "MID":
        json_argument = json.loads(configure1_string)
        json_argument["csp"]["common"][
            "config_id"
        ] = "sbi-test-20220916-00000-calibration:b"
        json_argument["sdp"]["scan_type"] = "calibration:b"
        configure2_string = json.dumps(json_argument)
    else:
        configure2_string = inputs["CONFIGURE"]

    configure_id = subarray_node.invoke_command("Configure", configure2_string)
    if subarray_node.deployment == "MID":
        simulate_configure_on_dish_master()
    subarray_node.assert_command_completion(configure_id, event_recorder)

    subarray_node.wait_for_subarray_obsstate(ObsState.READY)


@pytest.mark.parametrize(
    "assign_str, configure_str",
    [
        pytest.param(
            "AssignResources_mid",
            "Configure_mid",
            marks=[pytest.mark.SKA_mid],
        ),
        pytest.param(
            "AssignResources_low",
            "Configure_low",
            marks=[pytest.mark.SKA_low],
        ),
    ],
)
@pytest.mark.post_deployment
def test_multi_configure_command(
    tango_context,
    subarray_node,
    assign_str,
    configure_str,
    json_factory,
    event_recorder,
):
    """Test mult-configure (configure after configure) command."""
    inputs = {}
    inputs["ASSIGN"] = json_factory(assign_str)
    inputs["CONFIGURE"] = json_factory(configure_str)
    multi_configure(
        tango_context,
        subarray_node,
        inputs,
        event_recorder,
    )
