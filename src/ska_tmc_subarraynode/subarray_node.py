# -*- coding: utf-8 -*-
#
# This file is part of the SubarrayNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

""" Subarray Node
Provides the monitoring and control interface required by users as well as
other TM Components (such as OET, Central Node) for a Subarray.
"""
# pylint: disable=attribute-defined-outside-init
import functools
import json
from typing import List, Optional, Tuple, cast

from ska_tango_base import SKASubarray
from ska_tango_base.commands import ResultCode, SubmittedSlowCommand
from ska_tango_base.control_model import ObsState
from ska_tmc_common.op_state_model import TMCOpStateModel
from ska_tmc_common.v1.tmc_base_device import TMCBaseDevice
from tango import AttrWriteType, DebugIt
from tango.server import attribute, command, device_property

from ska_tmc_subarraynode import release
from ska_tmc_subarraynode.manager.subarray_node_component_manager import (
    SubarrayNodeComponentManager,
)


class BaseSubarrayNode(SKASubarray, TMCBaseDevice):
    """
    Provides the monitoring and control interface required by users as well as
    other TM Components (such as OET, Central Node) for a Subarray.

    BaseSubarrayNode class is inherited from SKASubarray class
    and TMCBaseDevice class.
    TMCBaseDevice class is further inherited from SKABaseDevice class.

    Common attributes and device_properties within TMC nodes are getting
    inherited from TMCBaseDevice.

    :Device Properties:

        SdpSubarrayLNFQDN:
            This property contains the FQDN of the SDP Subarray Leaf
            Node associated with the Subarray Node.

        CspSubarrayLNFQDN:
            This property contains the FQDN of the CSP Subarray
            Leaf Node associated with the Subarray Node.

        CspSubarrayFQDN:
            FQDN of the CSP Subarray Tango Device Server.

        SdpSubarrayFQDN:
            FQDN of the SDP Subarray Tango Device Server.

    :Device Attributes:

        scanID:
            ID of ongoing SCAN

        sbID:
            ID of ongoing Scheduling Block

        isSubarrayAvailable:
            Shows availability of subarray device


    """

    # -----------------
    # Device Properties
    # -----------------

    ProxyTimeout = device_property(dtype="DevUShort", default_value=500)

    CommandTimeOut = device_property(dtype="DevUShort", default_value=30)
    DevFamily = device_property(dtype="DevString", default_value="leaf-node")
    AbortCommandTimeOut = device_property(dtype="DevUShort", default_value=40)
    isAdminModeEnabled = attribute(dtype=bool, access=AttrWriteType.READ_WRITE)
    CspScanInterfaceURL = device_property(
        dtype="str",
        doc="This property contains the interface URL of the"
        "CSP sub-system for Scan command",
    )

    SdpScanInterfaceURL = device_property(
        dtype="str",
        doc="This property contains the interface URL of the"
        "SDP sub-system for Scan command",
    )

    CspAssignResourcesInterfaceURL = device_property(
        dtype="str",
        doc="This property contains the interface URL of the"
        "CSP sub-system for AssignResources command",
    )

    JonesURI = device_property(
        dtype="str",
        doc="This property contains URI for Jones",
    )

    CspSubarrayLNFQDN = device_property(
        dtype="str",
        doc="This property contains the FQDN of the CSP Subarray Leaf Node"
        "associated with the Subarray Node.",
    )

    SdpSubarrayLNFQDN = device_property(
        dtype="str",
        doc="This property contains the FQDN of the SDP Subarray Leaf Node"
        "associated with the Subarray Node.",
    )

    CspSubarrayFQDN = device_property(
        dtype="str",
        doc="This property contains the FQDN of the CSP Subarray"
        "associated with the Subarray Node.",
    )

    SdpSubarrayFQDN = device_property(
        dtype="str",
        doc="This property contains the FQDN of the SDP Subarray"
        "associated with the Subarray Node.",
    )

    # ----------
    # Attributes
    # ----------
    scanID = attribute(
        dtype="str",
        doc="ID of ongoing SCAN",
    )

    sbID = attribute(
        dtype="str",
        doc="ID of ongoing Scheduling Block",
    )

    isSubarrayAvailable = attribute(
        dtype="DevBoolean",
        doc="Shows aggregated value for availability on subarray device",
        access=AttrWriteType.READ,
    )

    assignedResources = attribute(
        dtype=("str",),
        max_dim_x=1024,
        access=AttrWriteType.READ,
    )

    isAdminModeEnabled = attribute(dtype=bool, access=AttrWriteType.READ_WRITE)

    def update_device_callback(self, dev_info):
        """Updates the last device information changed
        and triggers a push change event.
        """
        self.last_device_info_changed = dev_info.to_json()
        self.push_change_archive_events(
            "lastDeviceInfoChanged", self.last_device_info_changed
        )

    def update_subarray_health_state_callback(self, subarray_health_state):
        """This method updates the subarray health state
        and triggers a push change event.
        """
        self._health_state = subarray_health_state
        self.push_change_archive_events("healthState", subarray_health_state)

    def update_assigned_resources_callback(self, assigned_resources):
        """Triggers a push change event for the assigned resources."""
        self.push_change_archive_events(
            "assignedResources", assigned_resources
        )

    def update_subarray_availability_status_callback(
        self, subarray_availability
    ):
        """Triggers a push change event for the isSubarrayAvailable."""
        self.push_change_archive_events(
            "isSubarrayAvailable", subarray_availability
        )

    # ---------------
    # General methods
    # ---------------
    class InitCommand(SKASubarray.InitCommand):
        """
        A class for the TMC SubarrayNode's init_device() method.
        """

        def do(self):
            """
            Initializes the attributes and properties of the Subarray Node.

            return:
                A tuple containing a return code and a string
                message indicating status.
                The message is for information purpose only.

            rtype:
                (ReturnCode, str)

            raises:
                DevFailed if the error while subscribing the tango attribute
            """
            super().do()
            self._device._build_state = (
                f"{release.name},{release.version},{release.description}"
            )
            self._device._version_id = release.version
            self._device.op_state_model.perform_action("component_on")
            self._device.initial_obs_state = ObsState.EMPTY
            self._device._is_admin_mode_enabled = True
            for attribute_name in [
                "healthState",
                "assignedResources",
                "isSubarrayAvailable",
                "isAdminModeEnabled",
            ]:
                self._device.set_change_event(attribute_name, True, False)
                self._device.set_archive_event(attribute_name, True)
            return (ResultCode.OK, "")

    def always_executed_hook(self):
        """Internal construct of TANGO."""

    def delete_device(self):
        # if the init is called more than once
        # I need to stop all threads
        if hasattr(self, "component_manager"):
            self.component_manager.stop()
            self.component_manager.stop_all_process()

    # ------------------
    # Attributes methods
    # ------------------
    def internalModel_read(self):
        # The obsState is attribute of SKA Subarray base class. All the
        # obsState transitions are handled in base classes. obsState is never
        # set on SubarrayNode. There obsState attribute can not be mapped  with
        # the variable in SubarrayComponent.
        internal_model = json.loads(super().internalModel_read())
        internal_model["obsState"] = str(ObsState(self._obs_state))
        return json.dumps(internal_model)

    def transformedInternalModel_read(self):
        # The obsState is attribute of SKA Subarray base class. All the
        # obsState transitions are handled in base classes. obsState is never
        # set on SubarrayNode. There obsState attribute can not be mapped  with
        # the variable in SubarrayComponent.
        result = json.loads(super().transformedInternalModel_read())
        result[
            "subarray_health_state"
        ] = self.component_manager.get_subarray_healthstate()
        result["obsState"] = str(ObsState(self._obs_state))
        result[
            "isSubarrayAvailable"
        ] = self.component_manager.get_subarray_availability()
        return json.dumps(result)

    def read_scanID(self):
        """Internal construct of TANGO. Returns the Scan ID.

        EXAMPLE: 123
            Where 123 is a Scan ID from configuration json string.
        """
        return self.component_manager.component.scan_id

    def read_sbID(self):
        """Internal construct of TANGO. Returns the scheduling block ID."""
        return self.component_manager.component.sb_id

    def read_isSubarrayAvailable(self):
        """Return the TMC Subarray availability status. True/False"""
        return self.component_manager.component.subarray_availability

    def read_assignedResources(self):
        """Returns assignedResources attribute value."""
        return self.component_manager.component.assigned_resources

    def read_isAdminModeEnabled(self) -> bool:
        """Return the isAdminModeEnabled attribute value"""
        return self.component_manager.is_admin_mode_enabled

    def write_isAdminModeEnabled(self, value: bool):
        """Set the value of isAdminModeEnabled attribute"""
        self.component_manager.is_admin_mode_enabled = value

    # --------
    # Commands
    # --------

    def is_AssignResources_allowed(self):
        """
        Return whether the `AssignResource` command may be called
        in the current state.

        :return: whether the command may be called in the current device
            state
        :rtype: bool
        """
        # Get the obsState of SubarrayNode before executing
        # AssignResources command
        self.initial_obs_state = self._obs_state
        return self.component_manager.is_command_allowed("AssignResources")

    @command(
        dtype_in="str",
        doc_in="JSON-encoded string with the resources to add to subarray",
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def AssignResources(
        self, argin: str
    ) -> Tuple[List[ResultCode], List[str]]:
        """
        Assign resources to this subarray.

        :param argin: the resources to be assigned
        :type argin: str

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("AssignResources")
        (result_code, unique_id) = commands(argin)
        return ([result_code], [str(unique_id)])

    def is_On_allowed(self):
        """
        Checks whether the command is allowed to be run in the current state

        :return: True if this command is allowed to be run in
            current device state, False otherwise.

        :rtype: boolean

        :raises: DevFailed if this command is not allowed to be run
            in current device state
        """
        return self.component_manager.is_command_allowed("On")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    def On(self):
        """Invokes On command on SubarrayNode"""
        handler = self.get_command_object("On")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    def is_Off_allowed(self):
        """
        Checks whether the command is allowed to be run in the current state

        :return: True if this command is allowed to be run in
            current device state, False otherwise,

        :rtype: boolean

        :raises: DevFailed if this command is not allowed to be run
            in current device state
        """
        return self.component_manager.is_command_allowed("Off")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    def Off(self):
        """Invokes Off command on SubarrayNode"""
        handler = self.get_command_object("Off")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    def is_Standby_allowed(self):
        """
        Checks whether the command is allowed to be run in the current state

        :return: True if this command is allowed to be run in
            current device state, False otherwise.

        :rtype: boolean

        :raises: DevFailed if this command is not allowed to be run
            in current device state
        """
        return self.component_manager.is_command_allowed("Standby")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    def Standby(self):
        """Invokes Standby command on SubarrayNode"""
        handler = self.get_command_object("Off")
        result_code, unique_id = handler()
        return ([result_code], [unique_id])

    def is_ReleaseAllResources_allowed(self):
        """
        Check if command `ReleaseAllResources` is allowed in the
        current device state.

        :return: ``True`` if the command is allowed
        :rtype: boolean
        """
        # Get the obsState of SubarrayNode before executing
        # ReleaseAllResources command
        self.initial_obs_state = self._obs_state
        return self.component_manager.is_command_allowed("ReleaseAllResources")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def ReleaseAllResources(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Remove all resources to tear down to an empty subarray.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("ReleaseAllResources")
        (result_code, unique_id) = commands()
        return ([result_code], [str(unique_id)])

    def is_Configure_allowed(self):
        """
        Check if command `Configure` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        :rtype: boolean
        """

        # Get the obsState of SubarrayNode before executing End command
        self.initial_obs_state = self._obs_state
        return self.component_manager.is_command_allowed("Configure")

    @command(
        dtype_in="str",
        doc_in="JSON-encoded string with the scan configuration",
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Configure(self, argin: str) -> Tuple[List[ResultCode], List[str]]:
        """
        Configure the capabilities of this subarray.

        :param argin: configuration specification
        :type argin: string

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("Configure")
        (result_code, unique_id) = commands(argin)
        self.logger.debug(
            "ResultCode, unique_id/message is %s, %s", result_code, unique_id
        )
        return ([result_code], [str(unique_id)])

    def is_End_allowed(self):
        """
        Check if command `End` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        :rtype: boolean
        """
        # Get the obsState of SubarrayNode before executing End command
        self.initial_obs_state = self._obs_state
        return self.component_manager.is_command_allowed("End")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def End(self) -> Tuple[List[ResultCode], List[str]]:
        """
        End the scan block.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("End")
        (result_code, unique_id) = commands()
        return ([result_code], [str(unique_id)])

    def is_Scan_allowed(self):
        """
        Check if command `Scan` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        :rtype: boolean
        """
        # Get the obsState of SubarrayNode before executing Scan command
        self.initial_obs_state = self._obs_state
        return self.component_manager.is_command_allowed("Scan")

    @command(
        dtype_in="str",
        doc_in="JSON-encoded string with the per-scan configuration",
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Scan(self, argin) -> Tuple[List[ResultCode], List[str]]:
        """
        Start scanning.

        To modify behaviour for this command, modify the do() method of
        the command class.

        :param argin: Information about the scan
        :type argin: Array of str

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("Scan")
        (result_code, unique_id) = commands(argin)
        return ([result_code], [str(unique_id)])

    def is_EndScan_allowed(self) -> bool:
        """
        Check if command `EndScan` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        :rtype: boolean
        """
        # Get the obsState of SubarrayNode before executing End command
        self.initial_obs_state = self._obs_state
        return self.component_manager.is_command_allowed("EndScan")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def EndScan(self) -> Tuple[List[ResultCode], List[str]]:
        """
        End the scan.

        To modify behavior for this command, modify the do() method of
        the command class.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("EndScan")
        (result_code, unique_id) = commands()
        return ([result_code], [str(unique_id)])

    def is_Abort_allowed(self) -> bool:
        """
        Check if command `Abort` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        :rtype: boolean
        """
        super().is_Abort_allowed()
        # Get the obsState of SubarrayNode before executing Abort command
        self.initial_obs_state = self._obs_state
        return self.component_manager.is_command_allowed("Abort")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Abort(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Abort any long-running command such as ``Configure()`` or ``Scan()``.

        To modify behavior for this command, modify the do() method of
        the command class.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("Abort")
        (result_code, unique_id) = commands()
        return ([result_code], [str(unique_id)])

    def is_Restart_allowed(self) -> bool:
        """
        Check if command `Restart` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        :rtype: boolean
        """
        self.initial_obs_state = self._obs_state
        return self.component_manager.is_command_allowed("Restart")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'informational message')",
    )
    @DebugIt()
    def Restart(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Restart the subarray. That is, deconfigure and release all resources.

        To modify behavior for this command, modify the do() method of
        the command class.

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("Restart")
        (result_code, unique_id) = commands()
        return ([result_code], [str(unique_id)])

    def is_SetAdminMode_allowed(self) -> bool:
        """
        Check if command `Restart` is allowed in the current device state.

        :return: ``True`` if the command is allowed
        :rtype: boolean
        """
        return self.component_manager.is_command_allowed("SetAdminMode")

    @command(
        dtype_in="str",
        doc_in="The adminMode in json format",
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def SetAdminMode(self, argin) -> Tuple[List[ResultCode], List[str]]:
        """
        Set the AdminMode of subarrays by invoking the SetAdminMode
        command on SubarrayLeafNodes

        :return: A tuple containing a return code and a string
            message indicating status. The message is for
            information purpose only.
        :rtype: (ResultCode, str)
        """
        commands = self.get_command_object("SetAdminMode")
        (result_code, unique_id) = commands(argin)
        return ([result_code], [str(unique_id)])

    # def is_Reset_allowed(self):
    #     """
    #     Whether the ``Reset()`` command is allowed to be run
    #     in the current state.

    #     :returns: whether the ``Reset()`` command is allowed to be run
    #     in the current state
    #     :rtype: boolean
    #     """
    #     command = self.get_command_object("Reset")
    #     return command.is_allowed(raise_if_disallowed=True)

    # @command(
    #     dtype_out="DevVarLongStringArray",
    #     doc_out="(ReturnType, 'informational message')",
    # )
    # @DebugIt()
    # def Reset(self):
    #     """
    #     Reset the device from the FAULT state.

    #     To modify behavior for this command, modify the do() method of
    #     the command class.

    #     :return: A tuple containing a return code and a string
    #         message indicating status. The message is for
    #         information purpose only.
    #     :rtype: (ResultCode, str)
    #     """
    #     return self.enqueue_command("Reset")

    # ----------
    # Callbacks
    # ----------
    def _component_state_changed_callback(
        self,
        state_change,
        fqdn: Optional[str] = None,
    ) -> None:
        """
        Handle change in this device's state.

        This is a callback hook, called whenever the state changes. It
        is responsible for updating the tango side of things i.e. making
        sure the attribute is up to date, and events are pushed.

        :param state_change: A dictionary containing the name of the state
            that changed and its new value.
        :param fqdn: The fqdn of the device.
        """
        self.logger.debug(
            "Initial observation state: %s", self.initial_obs_state
        )
        if "resourced" in state_change.keys():
            if state_change.get("resourced", True):
                # If Initial obsState is EMPTY, then only perform action
                # "component_resourced" is needed
                # to transition SubarrayNode obsState from RESOURCING_EMPTY
                # to RESOURCING_IDLE.
                if self.initial_obs_state == ObsState.EMPTY:
                    self.obs_state_model.perform_action("component_resourced")
            else:
                # If Initial obsState is IDLE, then only perform action
                # "component_unresourced" is needed
                # to transition SubarrayNode obsState from RESOURCING_IDLE
                # to RESOURCING_EMPTY.
                if self.initial_obs_state in (
                    ObsState.IDLE,
                    ObsState.EMPTY,
                ):
                    self.obs_state_model.perform_action(
                        "component_unresourced"
                    )

        if "configured" in state_change.keys():
            if state_change.get("configured", True):
                # If Initial obsState is IDLE, then only perform action
                # "component_configured" is needed
                # to transition SubarrayNode obsState from RESOURCING_IDLE
                # to RESOURCING_READY.
                if self.initial_obs_state == ObsState.IDLE:
                    self.obs_state_model.perform_action("component_configured")
            else:
                # If Initial obsState is READY, then only perform action
                # "component_unconfigured" is needed.
                if self.initial_obs_state in (
                    ObsState.READY,
                    ObsState.IDLE,
                ):
                    self.obs_state_model.perform_action(
                        "component_unconfigured"
                    )
        if "scanning" in state_change.keys():
            if state_change.get("scanning", True):
                # If Initial obsState is READY, then only perform action
                # "component_configured" is needed
                if self.initial_obs_state == ObsState.READY:
                    self.obs_state_model.perform_action("component_scanning")
            else:
                self.obs_state_model.perform_action("component_not_scanning")

        if "assign_completed" in state_change.keys():
            self.obs_state_model.perform_action("assign_completed")
        if "release_completed" in state_change.keys():
            self.obs_state_model.perform_action("release_completed")
        if "configure_completed" in state_change.keys():
            self.obs_state_model.perform_action("configure_completed")
        if "abort_completed" in state_change.keys():
            self.obs_state_model.perform_action("abort_completed")
        if "restart_completed" in state_change.keys():
            self.obs_state_model.perform_action("restart_completed")
        if "component_obsfault" in state_change.keys():
            self.obs_state_model.perform_action("component_obsfault")
        self.logger.debug("Observation state %s", self._obs_state)

    def create_component_manager(self):
        """Create component manager object for command invocation."""
        self.op_state_model = TMCOpStateModel(
            logger=self.logger, callback=super()._update_state
        )

    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this
        device.
        """
        super().init_command_objects()

        def _callback(hook, running):
            self.logger.info("No action to be performed.")
            self.logger.info("Observation state is: %s", self._obs_state)

        # Note: The Standby command executes the Off command flow instead
        # of the separate Standby flow.
        # The reason is that it needs to propagate the relevant command
        # on SDP Subarray Leaf Node.
        # However, SDP does not support Standby command.
        # Due to the design evolution, especially after ADR-53.
        # the On/Off/Standby commands on Subarray Node will be
        # deprecated in near future.
        for command_name, method_name in [
            ("Standby", "off"),
            ("Off", "off"),
            ("On", "on"),
        ]:
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    logger=None,
                ),
            )

        for command_name, method_name, state_model_hook in [
            ("AssignResources", "assign", "assign"),
            ("Configure", "configure", None),
            ("ReleaseAllResources", "release_all", "release"),
            ("End", "end", "end"),
            ("EndScan", "end_scan", None),
            ("Scan", "scan", None),
            ("Restart", "restart", "restart"),
            ("SetAdminMode", "set_admin_mode", "set_admin_mode"),
        ]:
            callback = (
                None
                if state_model_hook is None
                else functools.partial(_callback, state_model_hook)
            )
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    callback=callback,
                    logger=None,
                ),
            )
        self.register_command_object(
            "Abort",
            self.AbortCommand(
                self._command_tracker,
                cast(SubarrayNodeComponentManager, self.component_manager),
                callback=functools.partial(_callback, "abort"),
                logger=self.logger,
            ),
        )

        self.logger.info("Commands registered.")

    def _update_obs_state(self, obs_state: ObsState) -> None:
        """
        Perform Tango operations in response to a change in obsState.

        This helper method is passed to the observation state model as a
        callback, so that the model can trigger actions in the Tango
        device.

        :param obs_state: the new obs_state value
        """
        super()._update_obs_state(obs_state)
        if obs_state == ObsState.ABORTED:
            self.logger.info("Setting aborted flag to True")
            self.component_manager.is_operation_aborted = True
