"""
This module is for identifing and changing the transaction ids
"""
import functools
import json
import logging
from typing import Any, Dict, Union

from ska_ser_log_transactions import transaction

# ENABLE_TRANSACTION_IDS = os.getenv('ENABLE_TRANSACTION_IDS')


def identify_with_id(name: str, arg_name: str):
    """This method decorator that identifies a transaction with
    a unique ID and adds it to the wrapped function's object.
    """

    def wrapper(func):
        @functools.wraps(func)
        def wrap(obj, *args, **kwargs):
            # if ENABLE_TRANSACTION_IDS:
            if args:
                argin = args[0]
            elif kwargs:
                argin = kwargs[arg_name]
            else:
                raise ValueError(
                    "no arguments provided for wrapping with transaction ids"
                )
            try:
                parameters = json.loads(argin)
            except Exception:
                logging.warning(
                    "unable to use transaction id as not able to parse n\
                    input arguments into a dictionary"
                )
                return func(obj, argin)
            with transaction(
                name, parameters, logger=obj.logger
            ) as transaction_id:
                obj.transaction_id = transaction_id
                return func(obj, argin)

        #  else: return func(obj,*args,**kwargs)
        return wrap

    return wrapper


def inject_id(obj, data: Dict) -> Dict:
    """This method injecting a transaction id"""
    get_id = getattr(obj, "transaction_id", None)
    if get_id:
        data["transaction_id"] = get_id
    return data


def update_with_id(obj, parameters: Any) -> Union[Dict, str]:
    """
    Updates the given dictionary-like `obj`
    with an ID field and the values in `parameters`.
    """
    if isinstance(parameters, str):
        parameters = json.loads(parameters)
        inject_id(obj, parameters)
        return json.dumps(parameters)
    if isinstance(parameters, dict):
        inject_id(obj, parameters)
        return parameters

    raise Exception(f"arg {parameters} is of not type dict or string")


def inject_with_id(arg_position: int, arg_name: str):
    """
    For this method  A decorator that injects an ID field
    into a dictionary-like argument of a function.
    """

    def wrapper(func):
        @functools.wraps(func)
        def wrap(obj, *args, **kwargs):
            if args:
                updated_args = list(args)
                updated_args[arg_position] = update_with_id(
                    obj, args[arg_position]
                )
                args = tuple(updated_args)
            elif kwargs:
                kwargs[arg_name] = update_with_id(obj, kwargs[arg_name])
            else:
                raise Exception("arguments not matching wrap function")
            return func(obj, *args, **kwargs)

        return wrap

    return wrapper
