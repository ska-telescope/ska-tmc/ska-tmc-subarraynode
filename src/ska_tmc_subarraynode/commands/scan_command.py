"""
 A class for TMC SubarrayNode's Scan() command
"""
import json
import threading
import time
from collections import defaultdict
from typing import Callable, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode, SubmittedSlowCommand
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeoutCallback, TimeoutState
from ska_tmc_common.adapters import AdapterFactory
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_subarraynode.commands.end_scan_command import EndScan
from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.utils.constants import COMMAND_COMPLETION_MESSAGE


class Scan(SubarrayNodeCommand):
    """
    A class for SubarrayNode's Scan() command.
    The command accepts Scan id as an input and executes
    a scan on the subarray.
    Scan command is invoked on respective CSP and SDP
    subarray node for the provided
    interval of time. It checks whether the scan is already in progress.
    If yes it throws error showing duplication of command.
    """

    def __init__(
        self,
        component_manager,
        obs_state_model,
        adapter_factory=None,
        logger=None,
    ):
        super().__init__(
            component_manager=component_manager,
            adapter_factory=adapter_factory,
            logger=logger,
        )

        self.component_manager = component_manager
        self.obs_state_model = obs_state_model
        self._adapter_factory = adapter_factory or AdapterFactory()
        self.only_dish_config_flag = False
        self.scan_id = ""

        # All data required to store for mapping scan
        self.current_scan_id_index = None
        self.scan_json = None
        self.mapping_scan_timeout_timer = None
        self.is_scan_on_all: bool = True
        self.is_endscan_on_all: bool = True
        self.timeout_id = f"{time.time()}_{__class__.__name__}"

        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: TaskCallbackType = self.task_callback_default

    def set_up_initial_mapping_data(self, json_argument: str):
        """Set up initial data required for mapping scan
        :param json_argument: scan command input
        :type json_argument: str
        This method called to setup data required for
        mapping scan
        """
        self.current_scan_id_index = 0
        self.scan_json = json_argument

    def clear_data(self):
        """Clear all data"""
        self.logger.debug("Clearing data for scan")
        self.component_manager.clear_mapping_scan_data()
        self.component_manager.scan_obj = None
        self.current_scan_id_index = None
        self.scan_json = None
        self.is_scan_on_all: bool = True
        self.is_endscan_on_all: bool = True
        self.stop_scan_timeout_timer()

    @timeout_tracker
    @error_propagation_tracker("is_scan_completed", [True])
    def invoke_scan(
        self,
        argin: str,
    ) -> None:
        """This is a long running method for Scan command,
        it executes do hook,invokes Scan command on
        CspSubarrayleafnode and SdpSubarrayleafnode.

        :param argin: JSON string consisting of the resources to be Scan
        :type argin: Json string, defaults to None
        :type task_callback: TaskCallbackType, optional
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """

        self.obs_state_model.perform_action("scan_invoked")
        return self.do(argin)

    def do_mid(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Scan command.

        :param argin: DevString. JSON string containing id.

        JSON string example as follows:
        .. code-block::

            {interface": "https://schema.skao.intg/ska-tmc-scan/2.0",
            "transaction_id": "txn-....-00001",
            "scan_id": 1}

        Note: Above JSON string can be used as an input
            argument while invoking this command from WEBJIVE.

        return: A tuple containing a return code and a string message
            indicating status.The message is for information purpose only.

        rtype: Tuple(ReturnCode, str)
        """

        self.update_command_in_progress_id("Scan")
        try:
            self.logger.info(argin)
            json_argument = json.loads(argin)
        except Exception as exception:
            log_msg = (
                "Execution of Scan command is failed."
                + f"Reason: JSON parsing failed with exception: {exception}"
            )
            self.logger.exception(log_msg)

            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in loading the JSON string in Scan command: "
                + f"{exception}",
            )
        self.only_dish_config_flag = False

        if "transaction_id" in json_argument:
            del json_argument["transaction_id"]

        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        if self.component_manager.component_state_changed_callback is not None:
            self.component_manager.component_state_changed_callback(
                {"scanning": True}
            )

        if self.is_scan_pattern() and "scan_ids" in json_argument:
            if self.component_manager.timer_object:
                self.logger.debug("Stopping Scan Timer now")
                self.component_manager.stop_timer()
            self.component_manager.mapping_scan_completed = False
            self.is_endscan_on_all = False
            scan_duration = self.get_scan_duration_for_mapping_scan(
                len(json_argument["scan_ids"])
            )
            result_code, message = self.send_scan_on_leaf_nodes(
                json_argument, scan_duration=scan_duration
            )
            self.component_manager.is_mapping_scan = True
            self.component_manager.scan_obj = self
            self.set_up_initial_mapping_data(json_argument)
            # Update aggregation queue with flag for multiple scan
            # is multiple scan flag used in aggregation queue
            event_manager = self.component_manager.event_data_manager
            event_manager.update_aggragation_queue()
            self.mapping_scan_timeout_timer = threading.Timer(
                self.component_manager.get_scan_duration(),
                self.set_failure_on_task_callback,
            )
            self.mapping_scan_timeout_timer.start()
        else:
            result_code, message = self.send_scan_on_leaf_nodes(json_argument)
        return result_code, message

    def is_scan_pattern(self):
        """Validate if multiple scan need to execute based
        on receptor and trajectory data
        """
        pattern_scan_data = self.component_manager.pattern_scan_data
        return pattern_scan_data and len(pattern_scan_data["receptors"]) > 0

    def get_scan_duration_for_mapping_scan(self, scan_ids_length: int):
        """Get scan duration for mapping scan"""
        total_scan_duration = self.component_manager.get_scan_duration()
        return total_scan_duration / scan_ids_length

    def set_failure_on_task_callback(self, scan_id):
        """Set Failure on task callback"""
        message = (
            "Timed out while waiting for obs state to ready "
            f"for scan id {scan_id}"
        )

        self.clear_data()

        self.component_manager.long_running_result_callback(
            self.component_manager.command_id,
            ResultCode.FAILED,
            exception_msg=message,
        )

        self.component_manager.observable.notify_observers(
            command_exception=True
        )

    def stop_scan_timeout_timer(self):
        """Stop Scan timeout timer"""
        self.logger.debug("Checking scan timeout timer")
        if (
            self.mapping_scan_timeout_timer
            and self.mapping_scan_timeout_timer.is_alive()
        ):
            self.logger.debug("Stopping Scan timeout timer")
            self.mapping_scan_timeout_timer.cancel()
            self.mapping_scan_timeout_timer = None

    def execute_mapping_scan(self):
        """Execute mapping scan when scan ids are provided"""
        json_argument = self.scan_json
        scan_ids = json_argument["scan_ids"]
        self.current_scan_id_index += 1
        json_argument["scan_id"] = scan_ids[self.current_scan_id_index]
        self.logger.info("Invoking Scan with id %s", json_argument["scan_id"])

        total_scan_duration = self.component_manager.get_scan_duration()

        self.mapping_scan_timeout_timer = threading.Timer(
            total_scan_duration,
            self.set_failure_on_task_callback,
            args=(scan_ids[self.current_scan_id_index],),
        )
        self.mapping_scan_timeout_timer.start()
        self.is_scan_on_all = False
        self.is_endscan_on_all = False
        if self.current_scan_id_index == len(scan_ids) - 1:
            self.is_endscan_on_all = True
        scan_duration_per_scan = self.get_scan_duration_for_mapping_scan(
            len(scan_ids)
        )
        result_code, message = self.send_scan_on_leaf_nodes(
            json_argument, scan_duration=scan_duration_per_scan
        )
        if result_code == ResultCode.FAILED:
            # reset multiple scan flag to false
            self.clear_data()

            self.component_manager.long_running_result_callback(
                self.component_manager.command_id,
                ResultCode.FAILED,
                exception_msg=message,
            )

            self.component_manager.observable.notify_observers(
                command_exception=True
            )
        if self.current_scan_id_index == len(scan_ids) - 1:
            self.clear_data()

            self.component_manager.mapping_scan_completed = True

            self.component_manager.observable.notify_observers(
                attribute_value_change=True
            )

    def send_scan_on_leaf_nodes(self, json_argument: str, **kwargs):
        """Send scan command on DLN, CSP, SDP
        :param json_argument: scan json argument
        :type json_argument: str
        """
        self.scan_id = json_argument["scan_id"]
        self.component_manager.set_scan_id(str(self.scan_id))
        for method in [self.scan_dishes, self.scan_sdp, self.scan_csp]:
            result_code, message = method(json_argument)
            if result_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    result_code,
                    message,
                )

        self.logger.info("Setting scan timer")
        if not kwargs.get("scan_duration"):
            scan_duration = self.component_manager.get_scan_duration()
        else:
            scan_duration = kwargs.get("scan_duration")
        self.start_scan_timer(scan_duration)

        self.logger.info("Scan command is invoked on SubarrayNode.")

        return (ResultCode.OK, COMMAND_COMPLETION_MESSAGE)

    def submit_endscan_command(self, is_only_on_trajectory_dishes):
        """
        Will submit the end scan
        """

        if is_only_on_trajectory_dishes is True:
            end_scan_cmd_obj = EndScan(
                self.component_manager,
                self.obs_state_model,
                self.adapter_factory,
                self.logger,
                is_only_on_trajectory_dishes=is_only_on_trajectory_dishes,
            )

            end_scan_cmd_obj.end_mapping_scan()

        else:
            # New Command Id will be created for EndScan
            end_scan_command = SubmittedSlowCommand(
                "EndScan",
                self.component_manager.command_tracker,
                self.component_manager,
                "end_singular_scan",  # changed to new
                callback=None,
                logger=None,
            )

        (result_code, unique_id) = end_scan_command()

        self.logger.info(
            "EndScan command's status:result_code "
            + f"{result_code}, and unique_id: {unique_id}"
        )

    def start_scan_timer(self, scan_duration: int) -> None:
        """
        Method for starting scan timer
        :param scan_duration: scan duration in second
        :type scan_duration: int
        """
        is_only_on_trajectory_dishes = False
        if not self.is_endscan_on_all:
            is_only_on_trajectory_dishes = True

        self.component_manager.scan_timer = threading.Timer(
            scan_duration,
            self.submit_endscan_command,
            # args=(end_scan_cmd_obj,),
            args=(is_only_on_trajectory_dishes,),
        )

        self.logger.debug(f"Scan duration is: {scan_duration}")

        self.component_manager.scan_timer.start()

    def scan_sdp(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Scan command on SDP Subarray Leaf Node.

        :return: (ResultCode, message)
        """
        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        argin["interface"] = self.component_manager.sdp_scan_interface
        argin = json.dumps(argin)
        try:
            result_code, message_or_unique_id = tmc_sdp_sln_adapter.Scan(argin)
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of Scan command is failed. "
                + "Reason: Scan command call failed on SDP Subarray "
                + f"Leaf Node: {tmc_sdp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Scan on SDP Subarray "
                + f"Leaf Node: {tmc_sdp_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def scan_dishes(self, dish_scan_config: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Scan command on Dish Leaf Nodes.

        :return: (ResultCode, message)
        """

        self.logger.info("Scan ID in progress %s", dish_scan_config["scan_id"])
        if not self.is_scan_on_all:
            self.logger.debug("Sending scan on Trajectory Dishes")
            tmc_leaf_dish_adapters = self.get_dish_adapter_by_device_name(
                self.component_manager.get_trajectory_dish_device_names()
            )
        else:
            tmc_leaf_dish_adapters = self.get_dish_adapter_by_device_name(
                self.component_manager.get_tmc_dish_ln_device_names()
            )
        for dish_leaf_adapter in tmc_leaf_dish_adapters:
            try:
                result_code, message_or_unique_id = dish_leaf_adapter.Scan(
                    str(self.scan_id)
                )
                if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                    return result_code[0], message_or_unique_id[0]

                self.update_event_data_storage(
                    dish_leaf_adapter.dev_name,
                    message_or_unique_id,
                )
                self.logger.info(
                    "Scan command invoked on %s", dish_leaf_adapter.dev_name
                )
            except Exception as exception:
                log_msg = (
                    "Execution of Scan command is failed. "
                    + "Reason: Scan command call failed on Dish"
                    + f"Leaf Node: {dish_leaf_adapter.dev_name}: {exception}"
                )
                self.logger.exception(log_msg)
                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "Error while invoking Scan on Dish Leaf Node"
                    + f" -> {dish_leaf_adapter.dev_name}: {exception}",
                )
        return ResultCode.OK, ""

    def scan_csp(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Scan command on CSP Subarray Leaf Node.

        :return: (ResultCode, message)
        """
        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        argin["interface"] = self.component_manager.csp_scan_interface
        argin = json.dumps(argin)
        try:
            result_code, message_or_unique_id = tmc_csp_sln_adapter.Scan(argin)
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of Scan command is failed. "
                + "Reason: Scan command call failed on CSP Subarray "
                + f"Leaf Node: {tmc_csp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)

            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Scan on CSP Subarray "
                + f"Leaf Node: {tmc_csp_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def scan_csp_low(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Scan command on CSP Subarray Leaf Node Low.

        :return: (ResultCode, message)
        """
        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        csp_args = defaultdict(dict)
        csp_args["interface"] = self.component_manager.csp_scan_interface
        csp_args["common"][
            "subarray_id"
        ] = self.component_manager.get_subarray_id()
        csp_args["lowcbf"]["scan_id"] = argin["scan_id"]
        csp_args_json = json.dumps(csp_args)
        self.logger.info("TMC to CSP interface")
        self.logger.info(csp_args_json)

        try:
            result_code, message_or_unique_id = tmc_csp_sln_adapter.Scan(
                csp_args_json
            )
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of Scan command is failed. "
                + "Reason: Scan command call failed on SDP Subarray "
                + f"Leaf Node: {tmc_csp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Scan on SDP Subarray "
                + f"Leaf Node: {tmc_csp_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def do_low(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Scan command.

        :param argin: DevString. JSON string containing id.

        JSON string example as follows:
        .. code-block::

            {"interface": "https://schema.skao.int/ska-low-tmc-scan/4.0",
            "transaction_id": "txn-....-00001", "subarray_id": 1,
            "scan_id": 1 }

        Note: Above JSON string can be used as an input argument while
            invoking this command from JIVE.

        return: A tuple containing a return code and a string message
            indicating execution status of command.

        rtype: Tuple(ReturnCode, str)

        raises: DevFailed if the command execution is not successful
        """
        self.update_command_in_progress_id("Scan")

        ret_code, message = self.init_adapters()
        if ret_code == ResultCode.FAILED:
            return ret_code, message

        try:
            self.logger.info(argin)
            json_argument = json.loads(argin)
        except Exception as exception:
            log_msg = (
                "Execution of Scan command is failed."
                + f"Reason: JSON parsing failed with exception: {exception}."
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in loading the JSON string in Scan command: "
                + f"{exception}",
            )

        if self.component_manager.component_state_changed_callback is not None:
            self.component_manager.component_state_changed_callback(
                {"scanning": True}
            )
        input_to_mccs = self.update_mccs_json(json_argument.copy())
        result_code, message = self.scan_mccs(input_to_mccs)
        if result_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                result_code,
                message,
            )

        for method in [self.scan_sdp, self.scan_csp_low]:
            ret_code, message = method(json_argument)
            if ret_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    ret_code,
                    message,
                )

        self.logger.info("Setting scan timer")
        scan_duration = self.component_manager.get_scan_duration()
        self.start_scan_timer(scan_duration)
        self.logger.info("Scan command is invoked.")

        return (ResultCode.OK, COMMAND_COMPLETION_MESSAGE)

    def update_mccs_json(self, input_scan):
        """
        This Scan command input string is updated to
        send to MCCS Subarray Leaf Node.
        """
        input_scan["interface"] = self.component_manager.mccs_scan_interface
        if "transaction_id" in input_scan:
            del input_scan["transaction_id"]
        if "subarray_id" in input_scan:
            del input_scan["subarray_id"]
        self.logger.info("MCCS Scan json: %s", input_scan)
        return input_scan

    def scan_mccs(self, argin):
        """
        Initiates a scanning process on the MCCS Subarray Leaf Node with the
        specified configuration.

        This method sends the Scan command to the MCCS Subarray Leaf Node,
        initiating the scanning process  with the provided configuration.

        :param argin: A dictionary containing configuration parameters for the
            scanning process. The format and content of this dictionary should
            comply with the requirements of the MCCS Subarray Leaf Node.
        :type argin: dict

        :return: A tuple containing a return code and a string message
            indicating the status. The message is for information purposes only
        :rtype: Tuple[ResultCode, str]
        """
        argin = json.dumps(argin)
        tmc_mccs_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_mccs_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_mccs_sln_adapter.Scan(
                argin
            )
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_mccs_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = f"""Execution of Scan command is failed.
            Reason: Scan command call failed on MCCS Subarray Leaf Node
            {tmc_mccs_sln_adapter.dev_name}: {exception}"""
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                (
                    "Error in calling Scan on MCCS Subarray Leaf %s: %s",
                    tmc_mccs_sln_adapter.dev_name,
                    exception,
                ),
            )
        return ResultCode.OK, ""

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""

        result = kwargs.get("result")
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("message") or kwargs.get("exception")

        self.logger.info("Updating task status with kwargs: %s", kwargs)

        if status == TaskStatus.ABORTED:
            self.task_callback(
                result=(ResultCode.ABORTED, "Command has been aborted"),
                status=status,
            )
            return
        if result[0] == ResultCode.OK:
            self.logger.info(
                "command_in_progress - %s",
                self.component_manager.command_in_progress,
            )
            self.task_callback(result=result, status=status)
        else:
            self.task_callback(
                result=result,
                status=status,
                exception=Exception(message),
            )

            self.component_manager.command_in_progress = ""
            self.component_manager.command_in_progress_id = None
            self.component_manager.command_result_code = None

            with self.component_manager.event_data_manager.eventlock:
                self.component_manager.event_data_manager.clear_lrcr()

        self.component_manager.all_lrcr_ok = False
        self.component_manager.command_id = ""
