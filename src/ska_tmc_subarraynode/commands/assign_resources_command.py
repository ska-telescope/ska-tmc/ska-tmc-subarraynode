"""
AssignResourcesCommand class for SubarrayNode.
"""
import json
import time
from typing import Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterFactory, TimeKeeper, TimeoutCallback
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.transaction_id import (
    identify_with_id,
    inject_with_id,
)
from ska_tmc_subarraynode.utils.constants import COMMAND_STARTED_MESSAGE


class AssignResources(SubarrayNodeCommand):
    """
    A class for SubarrayNode's AssignResources() command.

    Assigns resources to CSP, SDP via respective subarray leaf nodes.
    It also sets AssignedResources attribute on SubarrayNode.
    """

    def __init__(
        self,
        component_manager,
        obs_state_model,
        logger,
        adapter_factory=AdapterFactory(),
    ):
        super().__init__(
            component_manager=component_manager,
            adapter_factory=adapter_factory,
            logger=logger,
        )
        self.component_manager = component_manager
        self.obs_state_model = obs_state_model
        self.logger = logger
        self.mccs_resources = None
        self.csp_resources = None
        self.sdp_resources = None
        self.sb_id = None
        self.subarray_id = None
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: TaskCallbackType
        self.receptor_ids: list = []

    @timeout_tracker
    @error_propagation_tracker(
        "get_subarray_obsstate", [ObsState.RESOURCING, ObsState.IDLE]
    )
    def invoke_assign_resources(
        self,
        argin: str,
    ) -> Tuple[ResultCode, str]:
        """This is a long running method for AssignResources command,
        it executes do hook, invoking AssignResources command.

        :param argin: Input argument for the command
        :type argin: `str`

        :returns: Result code and message
        :rtype: `Tuple[ResultCode, str]`
        """
        self.obs_state_model.perform_action("assign_invoked")
        return self.do(argin)

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""
        result = kwargs.get("result")
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("message") or kwargs.get("exception")
        self.logger.info("Updating task status with kwargs: %s", kwargs)

        if status == TaskStatus.ABORTED:
            self.task_callback(
                result=(ResultCode.ABORTED, "Command has been aborted"),
                status=status,
            )
            return
        if result[0] == ResultCode.OK:
            # Update SubarrayNode assigned_resources attribute
            if self.receptor_ids:
                self.component_manager.set_assigned_resources(
                    self.receptor_ids
                )
            self.task_callback(result=result, status=status)
        else:
            self.task_callback(
                result=result,
                status=status,
                exception=Exception(message),
            )

            self.component_manager.command_in_progress = ""
            self.component_manager.command_in_progress_id = None
            self.component_manager.command_result_code = None
            self.clear_resources()
            # Call for aggregation in case Subsystems raise exceptions
            # and remain in initial obsState EMPTY
            event_manager = self.component_manager.event_data_manager
            event_manager.update_aggragation_queue()
        self.clear_device_events()
        self.component_manager.command_id = ""

    @identify_with_id("assign", "argin")
    def do_mid(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke AssignResources command on subarraynode mid.

        :param argin: DevString.

        Example:

        .. code-block::

            {"interface":
            "https://schema.skao.int/ska-tmc-assignresources/2.1"
            ,"transaction_id":"txn-....-00001","subarray_id": 1,"dish":
            {"receptor_ids":["SKA001"]},"sdp":{"interface":
            "https://schema.skao.int/ska-sdp-assignres/0.4","execution_block":
            {"eb_id":"eb-mvp01-20200325-00001","max_length": 100,"context":{},
            "beams":[{"beam_id": "vis0","function":"visibilities"},{"beam_id":
            "pss1","search_beam_id":1,"function": "pulsar search"},{"beam_id":
            "pss2","search_beam_id": 2,"function":"pulsar search"},{"beam_id":
            "pst1","timing_beam_id": 1,"function": "pulsar timing"},{"beam_id":
            "pst2","timing_beam_id":2,"function": "pulsar timing"},{"beam_id":
            "vlbi1","vlbi_beam_id":1,"function": "vlbi"}],"channels":
            [{"channels_id":"vis_channels","spectral_windows":[{"spectral_
            window_id":"fsp_1_channels","count": 744,"start": 0,"stride": 2,
            "freq_min":350000000,"freq_max":368000000,"link_map": [[0,0],
            [200,1],[744,2],[944,3]]},{"spectral_window_id":"fsp_2_channels",
            "count": 744,"start": 2000,"stride": 1,"freq_min": 360000000,
            "freq_max":368000000,"link_map": [[2000,4],[2200,5]]},{"spectral_
            window_id":"zoom_window_1","count": 744,"start":4000,"stride": 1,
            "freq_min":360000000,"freq_max": 361000000,"link_map": [[4000,6],
            [4200,7]]}]},{"channels_id":"pulsar_channels","spectral_windows":
            [{"spectral_window_id":"pulsar_fsp_channels","count": 744,"start":
            0,"freq_min":350000000,"freq_max": 368000000}]}],"polarisations":
            [{"polarisations_id": "all","corr_type":["XX","XY","YY","YX"]}],
            "fields":[{"field_id": "field_a","phase_dir":{"ra":[123,0.1],"dec":
            [123,0.1],"reference_time": "...","reference_frame": "ICRF3"},
            "pointing_fqdn":"low-tmc/telstate/0/pointing"}]},"processing_blocks
            ":[{"pb_id": "pb-mvp01-20200325-00001","sbi_ids":["sbi-mvp01-
            20200325-00001"],"script":{},"parameters":{},"dependencies":{}},
            {"pb_id": "pb-mvp01-20200325-00002","sbi_ids":["sbi-mvp01-20200325-
            00002"],"script":{},"parameters":{},"dependencies":{}},{"pb_id":
            "pb-mvp01-20200325-00003","sbi_ids":["sbi-mvp01-20200325-00001",
            "sbi-mvp01-20200325-00002"],"script":{},"parameters": {},
            "dependencies":{}}],"resources":{"csp_links":[1,2,3,4],"receptors":
            ["FS4","FS8"],"receive_nodes":10}}}

        return:
            A tuple containing a return code and a string message.

        rtype:
            (ResultCode, str)

        raises:
            KeyError if JSON parsing failed
            Exception if the command execution is not successful
        """
        self.update_command_in_progress_id("AssignResources")
        try:
            self.logger.info(argin)
            json_argument = json.loads(argin)
            resources = json_argument
        except Exception as exception:
            log_msg = (
                "AssignResources command failed:JSON parsing"
                + f"failed with exception: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in parsing the JSON string "
                + f"in AssignResources command: {exception}.",
            )

        try:
            self.subarray_id = json_argument["subarray_id"]
        except KeyError as key_error:
            self.logger.exception(key_error)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'subarray_id' "
                + "key in the AssignResources json",
            )

        # set subarray id
        try:
            self.component_manager.set_subarray_id(self.subarray_id)
        except Exception as exception:
            self.logger.exception(exception)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in setting subarray_id",
            )

        try:
            receptor_list = json_argument["dish"]["receptor_ids"]
        except KeyError as key_error:
            log_msg = (
                "AssignResources command failed: JSON parsing failed"
                + f"with exception: {key_error}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'receptor_ids'"
                + "key in the AssignResources json",
            )

        try:
            sdp_resources = json_argument["sdp"]
        except KeyError as key_error:
            log_msg = (
                "AssignResources command failed: JSON parsing failed"
                + f"with exception: {key_error}"
            )
            self.logger.exception(log_msg)

            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'sdp' key in the AssignResources json",
            )

        try:
            sb_id = json_argument["sdp"]["execution_block"]["eb_id"]
        except KeyError as key_error:
            log_msg = (
                "AssignResources command failed: JSON parsing failed"
                + f"with exception: {key_error}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'eb_id' key in the AssignResources json",
            )
        try:
            # Store the scan_type and beam_id required for calibration scan
            for beams in json_argument["sdp"]["execution_block"]["beams"]:
                if beams["function"] == "visibilities":
                    visibility_beam_id = beams["beam_id"]
                    break

            for scan_type in json_argument["sdp"]["execution_block"][
                "scan_types"
            ]:
                if visibility_beam_id in scan_type["beams"].keys():
                    temp_scan_type_id = scan_type["scan_type_id"]
                    if not temp_scan_type_id.startswith("."):
                        scan_type_id = temp_scan_type_id
                        break

            self.component_manager.set_keys_required_for_getting_sdpqc_fqdn(
                [scan_type_id, visibility_beam_id]
            )

        except Exception as exception:
            self.logger.exception(
                "Error in getting scan type or visibility beam id: %s",
                exception,
            )

        try:
            self.component_manager.set_sb_id(sb_id)
        except Exception as exception:
            log_msg = (
                "AssignResources command failed:Error in setting"
                + f"the sb_id from component manager: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in setting the sb_id from component"
                + "manager for AssignResources command",
            )

        return_code, message = self.init_adapters()
        if return_code == ResultCode.FAILED:
            return return_code, message

        self.logger.info(
            f"SubarrayNode obsState is: {self.obs_state_model.obs_state.name}"
        )

        return_code, message = self.set_up_dish_data(receptor_list)
        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )

        if self.component_manager.component_state_changed_callback:
            self.component_manager.component_state_changed_callback(
                {"resourced": True}
            )

        # Invoke command on CSP Subarray Leaf Node
        input_csp_assign = resources.copy()
        if "sdp" in input_csp_assign:
            del input_csp_assign["sdp"]
        input_csp_assign[
            "interface"
        ] = self.component_manager.csp_assign_interface
        return_code, message = self.assign_csp_resources(input_csp_assign)

        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )

        self.logger.info(
            f"SubarrayNode obsState is: {self.obs_state_model.obs_state.name}"
        )

        # Invoke command on SDP Subarray Leaf Node
        return_code, message = self.assign_sdp_resources(sdp_resources)
        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )

        self.logger.info("AssignResources command is invoked on SubarrayNode.")
        self.logger.info(
            f"SubarrayNode obsState is: {self.obs_state_model.obs_state.name}"
        )
        return (ResultCode.STARTED, COMMAND_STARTED_MESSAGE)

    def set_up_dish_data(self, receptor_ids: str) -> Tuple[ResultCode, str]:
        """
        Creates dish leaf node and dish device FQDNs using input receptor ids.
        The healthState and pointingState attributes of all the dishes are
        subscribed.

        :param receptor_ids:
            List of receptor IDs to be allocated to subarray.
            Example: ['SKA001', 'SKA002']

        return:
            List of Resources added to the Subarray.
            Example: ['SKA001', 'SKA002']
        """
        dish_ln_prefix = self.component_manager.get_dish_leaf_node_prefix()
        dish_ln_fqdns = []
        dish_fqdns = []
        self.receptor_ids = receptor_ids

        for receptor_id in receptor_ids:
            if receptor_id[:3] == "MKT":
                continue
            # Generate FQDNs for SKA dishes only as
            # MeerKAT dishes are not present
            dish_ln_fqdn = f"{dish_ln_prefix}{receptor_id[3:]}"
            dish_ln_fqdns.append(dish_ln_fqdn)
            for dish in self.component_manager.input_parameter.dish_fqdn:
                if receptor_id.lower() in dish.lower():
                    dish_fqdns.append(dish)
            self.logger.info("Assigned dish devices are: %s", dish_fqdns)
        try:
            # Update Dish Leaf Node and Dish Master devices
            # from Input Parameter
            self.component_manager.set_tmc_leaf_dish_device_names(
                dish_ln_fqdns
            )
            self.component_manager.set_dish_device_names(dish_fqdns)
            self.logger.info("Dish FQDNs are %s", dish_fqdns)
        except Exception as exception:
            self.component_manager.set_assigned_resources([])
            self.component_manager.clear_assigned_resources()
            log_msg = (
                "AssignResources command failed: Error in receptor"
                + f"allocation: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "AssignResources command failed: Error in receptor"
                + f"allocation: {exception}",
            )
        return ResultCode.OK, ""

    def assign_csp_resources(
        self, json_argument: str
    ) -> Tuple[ResultCode, str]:
        """
        This function accepts the AssignResources input JSON and
        invokes the assign resources command on
        the CSP Subarray Leaf Node.

        :param json_argument:
            AssignResources input JSON string without SDP block

        return:
            A tuple containing a return code and a string message.
        """
        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        try:
            (
                result_code,
                message_or_unique_id,
            ) = tmc_csp_sln_adapter.AssignResources(json.dumps(json_argument))
            self.logger.info(
                "The resultcode and message %s and %s",
                result_code,
                message_or_unique_id,
            )
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )

            self.logger.info(
                "AssignResources command "
                + f"invoked on {tmc_csp_sln_adapter.dev_name}"
            )
        except Exception as exception:
            log_msg = (
                "AssignResources command failed: Error while invoking"
                + "AssignResources on CSP Subarray Leaf Node "
                + f"{tmc_csp_sln_adapter.dev_name} : {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking AssignResources on"
                + "CSP Subarray Leaf Node -"
                + f"{tmc_csp_sln_adapter.dev_name}: {exception}.",
            )
        return ResultCode.OK, ""

    @inject_with_id(0, "argin")
    def assign_sdp_resources(self, argin: str) -> Tuple[ResultCode, str]:
        """
        This function accepts SDP block from input AssignResources JSON and
        assigns SDP resources to SDP Subarray
        through SDP Subarray Leaf Node.

        :param argin: List of strings
            JSON string containing only SDP resources.

        return:
            A tuple containing a return code and a string message.
        """
        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        try:
            (
                result_code,
                message_or_unique_id,
            ) = tmc_sdp_sln_adapter.AssignResources(json.dumps(argin))
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                message_or_unique_id,
            )

            self.logger.info(
                f"AssignResources command invoked on {tmc_sdp_sln_adapter}"
            )
        except Exception as e:
            log_msg = (
                "AssignResources command failed:"
                + "Error while invoking AssignResources on SDP "
                + f"Subarray Leaf Node -{tmc_sdp_sln_adapter.dev_name}: {e}"
            )
            self.logger.exception(log_msg)

            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking AssignResources on"
                + "SDP Subarray Leaf Node -"
                + f" {tmc_sdp_sln_adapter.dev_name}: {e}",
            )
        return ResultCode.OK, ""

    def do_low(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke AssignResources command on subarraynode low.

        :param argin: DevString.

        Example:

        .. code-block::

            {'interface':
            'https://schema.skao.int/ska-low-tmc-assignresources/4.0',
            'transaction_id': 'txn-....-00001', 'subarray_id': 1, 'mccs':
            {'interface':
            'https://schema.skao.int/ska-low-mccs-controller-allocate/3.0',
            'subarray_beams': [{'subarray_beam_id': 1, 'apertures':
            [{'station_id': 1, 'aperture_id': 'AP001.01'},
            {'station_id': 1, 'aperture_id': 'AP001.02'}, {'station_id': 2,
            'aperture_id': 'AP002.01'}, {'station_id': 2, 'aperture_id':
              'AP002.02'}
            ], 'number_of_channels': 8}]}, 'csp': {'pss': {'pss_beam_ids':
            [1, 2, 3]}, 'pst': {'pst_beam_ids': [1]}}, 'sdp': {'interface': '
            https://schema.skao.int/ska-sdp-assignres/0.4', 'resources': {'r
            eceptors': ['C4', 'C57', 'C108', 'C165', 'C193', 'C200', 'S8-1',
              'S8-2', 'S9-1', 'S9-5', 'S10-1', 'S10-6', 'S16-3', 'S16-4',
              'S16-6'], 'receive_nodes': 1}, 'execution_block': {'eb_id':
              'eb-test-20220916-00000', 'context': {}, 'max_length': 3600.0,
              'beams': [{'beam_id': 'vis0', 'function': 'visibilities'}],
              'scan_types': [{'scan_type_id': '.default', 'beams':
              {'vis0': {'channels_id': 'vis_channels', 'polarisations_id':
              'all'}}}, {'scan_type_id': 'target:a', 'derive_from':
              '.default', 'beams': {'vis0': {'field_id': 'field_a'}}},
              {'scan_type_id': 'calibration:b', 'derive_from': '.default',
            'beams': {'vis0': {'field_id': 'field_b'}}}], 'channels':
            [{'channels_id': 'vis_channels', 'spectral_windows':
              [{'spectral_window_id': 'fsp_1_channels', 'count': 4,
              'start': 0, 'stride': 2, 'freq_min': 350000000.0, 'freq_max':
             368000000.0, 'link_map': [[0, 0], [200, 1], [744, 2],
             [944, 3]]}]}], 'polarisations': [{'polarisations_id':
             'all', 'corr_type': ['XX', 'XY', 'YX', 'YY']}], 'fields':
            [{'field_id': 'field_a', 'phase_dir': {'ra': [123.0],
            'dec': [-60.0], 'reference_time': '...', 'reference_frame':
            'ICRF3'}, 'pointing_fqdn': '...'}, {'field_id': 'field_b',
            'phase_dir': {'ra': [123.0], 'dec': [-60.0], 'reference_time':
            '...', 'reference_frame': 'ICRF3'}, 'pointing_fqdn': '...'}]},
            'processing_blocks': [{'pb_id': 'pb-test-20220916-00000',
            'script': {'kind': 'realtime', 'name': 'test-receive-addresses',
            'version': '0.7.1'}, 'sbi_ids': ['sbi-mvp01-20210623-00000'],
            'parameters': {}}]}}

        return:
            A tuple containing ResultCode and string.

        rtype:
            (ResultCode, str)

        raises:
            ValueError if input argument json string contains invalid value
            Exception if the command execution is not successful
        """
        self.update_command_in_progress_id("AssignResources")

        return_code, message = self.validate_low_json(argin)
        if return_code == ResultCode.FAILED:
            return return_code, message

        # set subarray id
        try:
            self.component_manager.set_subarray_id(self.subarray_id)
        except KeyError:
            log_msg = (
                "Assign command is failed due to missing key subarray_id."
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in executing command. 'subarray_id' key is missing",
            )

        try:
            self.component_manager.set_sb_id(self.sb_id)
        except Exception as exception:
            log_msg = (
                "AssignResources command failed: Error in setting the"
                + f"sb_id from component manager: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in setting the sb_id from component manager "
                + f"for AssignResources command: {exception}",
            )

        return_code, message = self.init_adapters()
        if return_code == ResultCode.FAILED:
            return return_code, message

        if self.component_manager.component_state_changed_callback:
            self.component_manager.component_state_changed_callback(
                {"resourced": True}
            )

        # Form input for CSP sub-system

        try:
            # Assign interface and subarray_id
            argin_csp = self.get_csp_resources_low(json.loads(argin))
            self.csp_resources = argin_csp
        except Exception as e:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                f"Failed to form input string for CSP sub-system: {str(e)}",
            )
        # Invoke command on CSP Subarray Leaf Node
        return_code, message = self.assign_low_csp_resources(argin_csp)
        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )

        # Invoke command on SDP Subarray Leaf Node
        return_code, message = self.assign_sdp_resources(self.sdp_resources)
        if return_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                return_code,
                message,
            )

        self.logger.info(
            f"SubarrayNode obsState is: {self.obs_state_model.obs_state.name}"
        )

        return (ResultCode.STARTED, COMMAND_STARTED_MESSAGE)

    def get_csp_resources_low(self, input_json: dict) -> dict:
        """Create the input JSON for AssignResources on CSP Subarray
        Leaf Node device."""
        argin_csp = {}

        # Attempt to populate from the provided input_json
        if "csp" in input_json:
            argin_csp = input_json["csp"]

            # Remap `pss_beam_ids` to `beams_id` if `pss` and `pss_beam_ids`
            # exist
            if "pss" in argin_csp:
                pss = argin_csp["pss"]
                pss_beam_ids = pss.pop("pss_beam_ids", None)
                if pss_beam_ids is not None:
                    pss["beams_id"] = pss_beam_ids

            # Remap `pst_beam_ids` to `beams_id` if `pst` and `pst_beam_ids`
            #  exist
            if "pst" in argin_csp:
                pst = argin_csp["pst"]
                pst_beam_ids = pst.pop("pst_beam_ids", None)
                if pst_beam_ids is not None:
                    pst["beams_id"] = pst_beam_ids
        else:
            self.logger.info("No 'csp' configuration found in the input JSON.")

        # Ensure the CSP interface and subarray ID fields are added
        argin_csp["interface"] = self.component_manager.csp_assign_interface
        subarray_id = self.component_manager.get_subarray_id()
        argin_csp["common"] = {"subarray_id": subarray_id}

        # Ensure the 'lowcbf' key is present
        argin_csp.setdefault("lowcbf", {})

        return argin_csp

    def assign_low_csp_resources(self, argin: dict) -> Tuple[ResultCode, str]:
        """
        This function accepts the CSP Resources as input and
        assigns CSP resources to CSP Subarray
        through CSP Subarray Leaf Node.

        :param argin: JSON string including CSP resources.

        :return: A tuple containing ResultCode and message.

        """
        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        try:
            (
                result_code,
                message_or_unique_id,
            ) = tmc_csp_sln_adapter.AssignResources(json.dumps(argin))
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )
            self.logger.info(
                "AssignResources command invoked on %s",
                tmc_csp_sln_adapter.dev_name,
            )
        except Exception as exception:
            log_msg = (
                "AssignResources command failed:Error while invoking "
                + "AssignResources on CSP Subarray Leaf Node %s: %s",
                tmc_csp_sln_adapter.dev_name,
                exception,
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking AssignResources on "
                + "CSP Subarray Leaf Node - "
                + f"{tmc_csp_sln_adapter.dev_name}: {exception}",
            )

        return ResultCode.OK, ""

    def validate_low_json(self, argin: json):
        """
        Method to validate low input jsons for AssignResources command
        """
        try:
            self.logger.info(argin)
            json_argument = json.loads(argin)
        except Exception as exception:
            log_msg = (
                "AssignResources command failed: JSON parsing failed"
                + f"with exception: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in parsing the JSON string for "
                + f"AssignResources command: {exception}",
            )
        try:
            self.mccs_resources = json_argument["mccs"]
        except KeyError:
            log_msg = (
                "AssignResources command has failed due to missing key 'mccs'"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'mccs' key in the AssignResources json",
            )

        try:
            self.sdp_resources = json_argument["sdp"]
        except KeyError:
            log_msg = (
                "AssignResources command has failed due to missing key 'sdp'"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'sdp' key in the AssignResources json",
            )

        try:
            self.sb_id = json_argument["sdp"]["execution_block"]["eb_id"]
        except KeyError:
            log_msg = (
                "AssignResources command has failed due to missing key 'eb_id'"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'eb_id' key in the AssignResources json",
            )

        try:
            self.subarray_id = json_argument["subarray_id"]
        except KeyError:
            log_msg = (
                "AssignResources command has failed"
                + "due to missing key 'subarray_id'."
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'subarray_id' "
                + "key in the AssignResources json",
            )

        return (ResultCode.OK, "")

    def clear_resources(self):
        """Method for clearing resources in case of failure"""
        # Update SubarrayNode assigned_resources attribute
        self.component_manager.reset_sb_id()
        self.logger.info("Setting assigned resources to be EMPTY.")
        self.component_manager.set_assigned_resources([])
        self.component_manager.clear_assigned_resources()
        self.component_manager.reset_subarray_id()
        with self.component_manager.event_data_manager.eventlock:
            self.component_manager.event_data_manager.clear_lrcr()

        self.logger.info("Clean up is complete.")
