"""Class for setting the adminMode of lower level devices"""
import json
import threading
from typing import Optional, Tuple

from ska_control_model import AdminMode
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterFactory

from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.utils.constants import COMMAND_COMPLETION_MESSAGE


class SetAdminMode(SubarrayNodeCommand):
    """
    A class for SubarrayNode's SetAdminMode command.
    """

    def __init__(
        self,
        component_manager,
        adapter_factory=AdapterFactory(),
        logger=None,
    ):
        super().__init__(component_manager, adapter_factory, logger=logger)

        self.component_manager = component_manager
        self.task_callback: TaskCallbackType = self.task_callback_default

    def invoke_setadminmode(
        self,
        argin: str,
        task_callback: TaskCallbackType,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        This is a long-running method for the SetAdminMode command.
        It executes the `do` hook and invokes SetAdminMode on
        Subarray Leaf Nodes.

        :param logger: Logger instance.
        :type logger: logging.Logger
        """
        # Indicate that the task has started
        task_callback(status=TaskStatus.IN_PROGRESS)
        self.component_manager.command_in_progress = "SetAdminMode"
        result_code, message = self.do(argin)
        self.logger.info(message)
        if result_code == ResultCode.FAILED:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.FAILED, message),
                exception=message,
            )
        elif result_code == ResultCode.NOT_ALLOWED:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.NOT_ALLOWED, message),
            )
        else:
            task_callback(
                status=TaskStatus.COMPLETED, result=(ResultCode.OK, message)
            )

    def do_low(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke SetAdminMode command on the Subarray Leaf Nodes.

        :param argin: Input JSON for the command,
            containing the admin mode to be set.
        :type argin: str, optional

        :return: A tuple containing a return code and a string message
            indicating the execution status of the command.
        :rtype: (ResultCode, str)
        """
        if not self.component_manager.is_admin_mode_enabled:
            self.logger.info(
                "AdminMode functionality is disabled, "
                + "Device will function normally"
            )
            return (
                ResultCode.NOT_ALLOWED,
                (
                    "AdminMode functionality is disabled, "
                    + "Device will function normally"
                ),
            )
        self.update_command_in_progress_id("SetAdminMode")
        return_code, message = self.init_adapters()
        if return_code == ResultCode.FAILED:
            return return_code, message
        failed_devices = []
        try:
            admin_mode_config = json.loads(argin)

            # Validate the input JSON structure
            if "devices" not in admin_mode_config:
                return ResultCode.FAILED, "Missing devices field in input."

            devices_config = admin_mode_config["devices"]

            # Process 'low' devices (csp, sdp, mccs)
            if "low" in devices_config:
                low_devices = devices_config["low"]
                for device_type, method in [
                    ("sdp", self.set_admin_mode_sdp),
                    ("csp", self.set_admin_mode_csp),
                    ("mccs", self.set_admin_mode_mccs),
                ]:
                    if device_type in low_devices:
                        admin_mode = low_devices[device_type].get("adminMode")
                        if admin_mode is not None:
                            result_code, message = method(admin_mode)
                            if result_code == ResultCode.FAILED:
                                failed_devices.append(
                                    f"{device_type}: {message}"
                                )
                                self.logger.error(
                                    "Failed to set adminMode "
                                    + f"for {device_type}"
                                )
                        else:
                            self.logger.info(
                                f"No admin mode specified for {device_type}. "
                                + "Skipping."
                            )
                    else:
                        self.logger.info(
                            f"Device type '{device_type}' is "
                            + "not provided in the input."
                        )

            # Determine final status
            if failed_devices:
                return ResultCode.FAILED, "Failed devices: " + ", ".join(
                    failed_devices
                )

            return ResultCode.OK, COMMAND_COMPLETION_MESSAGE
        except json.JSONDecodeError:
            return ResultCode.FAILED, "Invalid JSON input format."
        except Exception as e:
            self.logger.exception(
                f"Unexpected error in SetAdminMode command: {e}"
            )
            return ResultCode.FAILED, str(e)

    def set_admin_mode_sdp(self, admin_mode: int) -> Tuple[ResultCode, str]:
        """
        SetAdminMode command on SDP Subarray Leaf Node.

        :param admin_mode: The admin mode to be set.
        :type admin_mode: int

        :return: A tuple containing the result code and a message.
        :rtype: (ResultCode, str)
        """
        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        self.logger.info(
            "Invoking SetAdminMode command with adminMode "
            f"{AdminMode(admin_mode).name} on sdp subarray leaf node."
        )
        try:
            (
                result_code,
                message_or_unique_id,
            ) = tmc_sdp_sln_adapter.SetAdminMode(admin_mode)
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return (result_code[0], message_or_unique_id[0])

        except Exception as exception:
            log_msg = (
                "Execution of SetAdminMode command failed."
                + "Reason: SetAdminMode command call failed on "
                + "SDP Subarray Leaf Node "
                + f"{tmc_sdp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return (ResultCode.FAILED, log_msg)
        self.logger.info("Command successfully invoked on sdpsln.")
        return (ResultCode.OK, "")

    def set_admin_mode_csp(self, admin_mode: int) -> Tuple[ResultCode, str]:
        """
        SetAdminMode command on CSP Subarray Leaf Node.

        :param admin_mode: The admin mode to be set.
        :type admin_mode: int

        :return: A tuple containing the result code and a message.
        :rtype: (ResultCode, str)
        """
        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        self.logger.info(
            "Invoking SetAdminMode command with adminMode "
            f"{AdminMode(admin_mode).name} on csp subarray leaf node."
        )
        try:
            (
                result_code,
                message_or_unique_id,
            ) = tmc_csp_sln_adapter.SetAdminMode(admin_mode)
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

        except Exception as exception:
            log_msg = (
                "Execution of SetAdminMode command failed."
                + "Reason: SetAdminMode command call failed on CSP Subarray "
                + f"Leaf Node {tmc_csp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return ResultCode.FAILED, log_msg
        self.logger.info("Command successfully invoked on cspsln.")
        return ResultCode.OK, ""

    def set_admin_mode_mccs(self, admin_mode: int) -> Tuple[ResultCode, str]:
        """
        SetAdminMode command on MCCS Subarray Leaf Node.

        :param admin_mode: The admin mode to be set.
        :type admin_mode: int

        :return: A tuple containing the result code and a message.
        :rtype: (ResultCode, str)
        """
        tmc_mccs_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_mccs_sln_device_name()
        )
        self.logger.info(
            "Invoking SetAdminMode command with adminMode "
            f"{AdminMode(admin_mode).name} on mccs subarray leaf node."
        )
        try:
            (
                result_code,
                message_or_unique_id,
            ) = tmc_mccs_sln_adapter.SetAdminMode(admin_mode)
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

        except Exception as exception:
            log_msg = (
                "Execution of SetAdminMode command failed."
                + "Reason: SetAdminMode command call failed on MCCS Subarray "
                + f"Leaf Node {tmc_mccs_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return ResultCode.FAILED, log_msg
        self.logger.info("Command successfully invoked on mccssln.")
        return ResultCode.OK, ""
