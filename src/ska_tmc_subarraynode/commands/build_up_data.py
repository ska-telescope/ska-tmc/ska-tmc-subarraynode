"Class to generate schema."
import json
from typing import Tuple

from ska_tango_base.commands import ResultCode
from ska_telmodel.csp.interface import make_csp_config

from ska_tmc_subarraynode.utils.subarray_node_utils import (
    split_interface_version,
)


class ElementDeviceData:
    """
    A class representing data for an element device.
    """

    def __init__(
        self,
        scan_config: dict,
        component_manager,
    ):
        self.scan_config = scan_config
        self.component_manager = component_manager
        self.csp_scan_config = ""
        self.csp_interface_version = ""
        self.sdp_interface_version = ""
        self.scan_type = ""

    def build_up_sdp_cmd_data(self) -> Tuple[ResultCode, str]:
        """
        Method to build up sdp command data
        """
        try:
            sdp_scan_config = self.scan_config["sdp"]
            if not sdp_scan_config:
                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "SDP configuration must be given.",
                )
        except Exception as exception:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                f"Error in building SDP configuration data: {exception}",
            )
        try:
            scan_type = sdp_scan_config["scan_type"]
            if not scan_type:
                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "SDP Subarray scan_type must be given.",
                )
        except Exception as exception:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                f"Error in building SDP configuration data: {exception}",
            )

        return ResultCode.OK, json.dumps(sdp_scan_config)

    def build_up_csp_cmd_data(
        self,
        receive_addresses_map: str,
    ) -> Tuple[ResultCode, str]:
        """
        Builds the input data for CSP configuration.

        :param receive_addresses_map: Map of receive addresses
        :param telescope: Specify if configuration is for "mid" or "low"
        :return: Result code and CSP configuration schema
        """
        try:
            self.csp_scan_config = self.scan_config["csp"]
            self.csp_interface_version = self.scan_config["csp"]["interface"]
            self.sdp_interface_version = self.scan_config["sdp"]["interface"]
        except KeyError as key_error:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                f"'csp' key missing: {key_error}",
            )

        try:
            self.scan_type = self.scan_config["sdp"]["scan_type"]
        except KeyError as key_error:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                f"'sdp' key missing: {key_error}",
            )
        if not receive_addresses_map:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Receive addresses must be given.",
            )

        return ResultCode.OK, ""


class ElementDeviceDataLow(ElementDeviceData):
    """
    A class representing data for an Low element device.
    """

    def __init__(
        self,
        scan_config: dict,
        component_manager,
    ):
        super().__init__(
            scan_config=scan_config, component_manager=component_manager
        )

    def build_up_csp_cmd_data(
        self,
        receive_addresses_map: str,
    ) -> Tuple[ResultCode, str]:
        """
        Builds the input data for CSP configuration.

        :param receive_addresses_map: Map of receive addresses
        :param telescope: Specify if configuration is for "mid" or "low"
        :return: Result code and CSP configuration schema
        """
        result_code, message = super().build_up_csp_cmd_data(
            receive_addresses_map
        )
        if result_code == ResultCode.FAILED:
            return result_code, message
        try:
            csp_config_schema = make_csp_config(
                str(self.csp_interface_version),
                str(self.sdp_interface_version),
                self.scan_type,
                self.csp_scan_config,
                receive_addresses_map,
                telescope_branch="low",
            )
        except Exception as exception:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in receiving CSP Config schema from "
                + f"the TelModel library: {exception}",
            )
        return ResultCode.OK, csp_config_schema


class ElementDeviceDataMid(ElementDeviceData):
    """
    A class representing data for an Mid element device.
    """

    def __init__(
        self,
        scan_config: dict,
        component_manager,
    ):
        super().__init__(
            scan_config=scan_config, component_manager=component_manager
        )

    def build_up_csp_cmd_data(
        self,
        receive_addresses_map: str,
    ) -> Tuple[ResultCode, str]:
        """
        Builds the input data for CSP configuration.

        :param scan_config: Configuration for the scan
        :param delay_model_subscription: Subscription point for the delay model
        :param receive_addresses_map: Map of receive addresses
        :param component_manager: Manager for components
        :param telescope: Specify if configuration is for "mid" or "low"
        :return: Result code and CSP configuration schema
        """
        result_code, message = super().build_up_csp_cmd_data(
            receive_addresses_map
        )
        if result_code == ResultCode.FAILED:
            return result_code, message
        try:
            csp_config_schema = make_csp_config(
                str(self.csp_interface_version),
                str(self.sdp_interface_version),
                self.scan_type,
                self.csp_scan_config,
                receive_addresses_map,
                telescope_branch="mid",
            )
        except Exception as exception:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in receiving CSP Config schema from "
                + f"the TelModel library: {exception}",
            )

        # In TMC-CSP interface below v.3.0 zoom_factor, zoom_window_tuning and
        # subarray keys not required hence deleting them before sending
        # command on cspleafnode
        csp_configure_schema = json.loads(csp_config_schema)
        csp_interface_version = csp_configure_schema["interface"]
        (major, minor) = split_interface_version(csp_interface_version)
        if (major, minor) <= (3, 0):
            csp_configure_schema[
                "interface"
            ] = "https://schema.skao.int/ska-csp-configurescan/3.0"
            for fsp in csp_configure_schema["cbf"]["fsp"]:
                fsp.pop("zoom_factor", None)
                fsp.pop("zoom_window_tuning", None)
            if "subarray" in csp_configure_schema:
                csp_configure_schema.pop("subarray", None)
        csp_configure_schema["pointing"] = self.scan_config.get("pointing")
        return ResultCode.OK, json.dumps(csp_configure_schema)

    def build_up_dish_cmd_data(self) -> Tuple[ResultCode, str]:
        """
        Method to build up dish command data
        """
        scan_config = self.scan_config.copy()
        if not set(["pointing", "dish"]).issubset(scan_config.keys()):
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Dish configuration must be given.",
            )
        scan_config.pop("sdp", None)
        scan_config.pop("csp", None)
        scan_config.pop("tmc", None)
        return ResultCode.OK, json.dumps(scan_config)
