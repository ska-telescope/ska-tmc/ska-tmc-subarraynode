"""
ReleaseAllResources Command for SubarrayNode
"""
import time
from typing import TYPE_CHECKING, Tuple, Union

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterFactory, TimeKeeper, TimeoutCallback
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.model.input import InputParameterMid
from ska_tmc_subarraynode.utils.constants import COMMAND_STARTED_MESSAGE

if TYPE_CHECKING:
    from ska_tmc_subarraynode.manager import (
        SubarrayNodeComponentManagerLow,
        SubarrayNodeComponentManagerMid,
    )


class ReleaseAllResources(SubarrayNodeCommand):
    """
    A class for TMC SubarrayNode's ReleaseAllResources() command.

    It checks whether all resources are already released.
    If yes then it returns code FAILED. If not it Releases all the resources
    from the subarray i.e. Releases
    resources from TMC Subarray Node, CSP Subarray and SDP Subarray.
    Upon successful execution, all the resources of a given
    subarray get released and empty array is returned.
    Selective release is not yet supported.

    """

    def __init__(
        self,
        component_manager,
        obs_state_model,
        logger,
        adapter_factory=AdapterFactory(),
    ):
        super().__init__(
            component_manager=component_manager,
            adapter_factory=adapter_factory,
            logger=logger,
        )
        self.component_manager: Union[
            SubarrayNodeComponentManagerLow, SubarrayNodeComponentManagerMid
        ] = component_manager
        self.obs_state_model = obs_state_model
        self.logger = logger
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback = TimeoutCallback(self.timeout_id, logger)
        self.task_callback: TaskCallbackType

    @timeout_tracker
    @error_propagation_tracker(
        "get_subarray_obsstate", [ObsState.RESOURCING, ObsState.EMPTY]
    )
    def invoke_release_resources(
        self,
    ) -> Tuple[ResultCode, str]:
        """This is a long running method for ReleaseAllResources command,
        it executes do hook, invoking ReleaseAllResources command.

        :returns: Result code and message
        :rtype: `Tuple[ResultCode, str]`
        """
        self.obs_state_model.perform_action("release_invoked")
        return self.do(argin=None)

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""
        result = kwargs.get("result")
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("message") or kwargs.get("exception")
        self.logger.info("Updating task status with kwargs: %s", kwargs)

        if status == TaskStatus.ABORTED:
            self.task_callback(
                result=(ResultCode.ABORTED, "Command has been aborted"),
                status=status,
            )
            return
        if result[0] == ResultCode.OK:
            if isinstance(
                self.component_manager.input_parameter, InputParameterMid
            ):
                self.clean_up_mid_resources()
            else:
                self.clean_up_low_resources()

            self.task_callback(result=result, status=status)
        else:
            self.task_callback(
                result=result,
                status=status,
                exception=Exception(message),
            )
            self.component_manager.command_in_progress = ""
            self.component_manager.command_in_progress_id = None
            self.component_manager.command_result_code = None

        self.clear_device_events()
        self.component_manager.command_id = ""

    def do_mid(self, argin=None):
        """
        Method to invoke ReleaseAllResources command.

        return:
            A tuple containing a return code and "" as a string
            on successful release all resources.

        rtype:
            (ResultCode, str)

        """
        self.update_command_in_progress_id("ReleaseAllResources")
        self.component_manager.kvalue_validation_failed = False
        if self.component_manager.component_state_changed_callback:
            self.logger.debug(
                "SubarrayNode obsState is: %s", self.obs_state_model.obs_state
            )
            self.component_manager.component_state_changed_callback(
                {"resourced": False}
            )

        dishes_assigned = self.component_manager.get_assigned_resources()
        if dishes_assigned == []:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Subarray doesn't have any resources to release",
            )
        self.logger.info(f"Resources to be released: {dishes_assigned}")

        self.component_manager.unsubscribe_dish_events()

        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        for method in [
            self.release_csp_resources,
            self.release_sdp_resources,
        ]:
            result_code, message = method()
            if result_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    result_code,
                    message,
                )

        self.logger.info(
            "ReleaseAllResources command is invoked on SubarrayNode."
        )
        return (ResultCode.STARTED, COMMAND_STARTED_MESSAGE)

    def do_low(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke ReleaseAllResources command.

        return:
            A tuple containing a return code STARTED on
            successful release all resources and message.

        rtype:
            (ResultCode, str)

        """
        self.update_command_in_progress_id("ReleaseAllResources")

        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        for method in [
            self.release_csp_resources,
            self.release_sdp_resources,
        ]:
            result_code, message = method()
            if result_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    result_code,
                    message,
                )

        if self.component_manager.component_state_changed_callback:
            self.logger.debug(
                "SubarrayNode obsState is: %s", self.obs_state_model.obs_state
            )
            self.component_manager.component_state_changed_callback(
                {"resourced": False}
            )

        self.logger.info(
            "ReleaseAllResources command is invoked on SubarrayNode."
        )
        return (ResultCode.STARTED, COMMAND_STARTED_MESSAGE)

    def release_csp_resources(self) -> Tuple[ResultCode, str]:
        """
        This function invokes releaseAllResources
        command on CSP Subarray via CSP Subarray Leaf
        Node.

        :param argin: DevVoid

        return:
            DevVoid

        """
        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        try:
            (
                result_code,
                message_or_unique_id,
            ) = tmc_csp_sln_adapter.ReleaseAllResources()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )
            self.logger.info(
                "ReleaseAllResources command invoked on CSP Subarray "
                + f"Leaf Node  {tmc_csp_sln_adapter.dev_name}"
            )
        except Exception as e:
            log_msg = (
                "ReleaseAllResources command failed: Error while invoking "
                + "command on CSP "
                + f"Subarray Leaf Node  {tmc_csp_sln_adapter.dev_name}: {e}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking ReleaseAllResources command on CSP "
                + f"Subarray Leaf Node  {tmc_csp_sln_adapter.dev_name}: {e}",
            )
        return ResultCode.OK, ""

    def release_sdp_resources(self) -> Tuple[ResultCode, str]:
        """
        This function invokes releaseAllResources command on
        SDP Subarray via SDP Subarray Leaf Node.

        :param argin: DevVoid

        return:
            DevVoid

        """
        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        try:
            (
                result_code,
                message_or_unique_id,
            ) = tmc_sdp_sln_adapter.ReleaseAllResources()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                message_or_unique_id,
            )
            self.logger.info(
                "ReleaseAllResources command invoked on SDP Subarray "
                + f"Leaf Node  {tmc_sdp_sln_adapter.dev_name}"
            )
        except Exception as exception:
            log_msg = (
                "ReleaseAllResources command failed: "
                + "Error while invoking command on SDP Subarray Leaf Node "
                + f" {tmc_sdp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                f"Error while invoking ReleaseAllResources \
                command on SDP Subarray Leaf Node  \
                {tmc_sdp_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def clean_up_mid_resources(self) -> Tuple[ResultCode, str]:
        """
        Clears the AssignedResources attribute.

        Cleans dictionaries of the resources across the subarraynode.

        Note: Currently there are only receptors allocated
        so only the receptor ids details are stored.

        :param argin:
            None
        :return:
            None
        """
        try:
            # Update SubarrayNode assigned_resources attribute
            self.logger.info("Set assigned resources to be EMPTY.")
            self.component_manager.set_assigned_resources([])
            # Update dish mode data and pointing state data
            # from event data manager
            with self.component_manager.event_data_manager.eventlock:
                self.component_manager.event_data_manager.clear_dish_data()
            self.component_manager.clear_assigned_resources()
            self.logger.info("Clean up is complete.")

        except Exception as e:
            log_msg = (
                "Execution of ReleaseAllResources command is failed."
                + f"Reason: Failed to clear resources on SubarrayNode: {e}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                f"Error in releasing the Resources from the Subarray: {e}",
            )

        try:
            self.component_manager.reset_sb_id()
        except Exception as e:
            log_msg = (
                "Execution of ReleaseAllResources command is failed."
                + f"Reason: Failed to reset sb_id on SubarrayNode: {e}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the sb_id in "
                + f"ReleaseAllResources command: {e}",
            )
        return ResultCode.OK, ""

    def clean_up_low_resources(self) -> Tuple[ResultCode, str]:
        """
        Clears the assignedResources attribute.

        Cleans dictionaries of the resources across the subarraynode.

        Note: Currently there are only receptors allocated
        so only the receptor ids details are stored.

        :param argin:
            None
        :return:
            None
        """

        try:
            self.component_manager.reset_sb_id()
            self.component_manager.reset_subarray_id()
        except Exception as exception:
            log_msg = (
                "Execution of ReleaseAllResources command is failed."
                + "Reason: Failed to reset sb_id and subarray_id"
                + f" on SubarrayNode: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the sb_id and subarray_id in "
                + f"ReleaseAllResources command: {exception}",
            )
        return ResultCode.OK, ""
