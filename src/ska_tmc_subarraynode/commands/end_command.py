"""
A class for TMC SubarrayNode's End() command
"""

import time
from typing import Callable, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.adapters import AdapterFactory
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.utils.constants import COMMAND_COMPLETION_MESSAGE


class End(SubarrayNodeCommand):
    """
    A class for SubarrayNode's End() command.

    This command on Subarray Node invokes End command on
    CSP Subarray Leaf Node and SDP
    Subarray Leaf Node, and stops tracking of all the assigned dishes.

    """

    def __init__(
        self,
        component_manager,
        obs_state_model,
        adapter_factory=AdapterFactory(),
        logger=None,
    ):
        super().__init__(
            component_manager=component_manager,
            adapter_factory=adapter_factory,
            logger=logger,
        )
        self.component_manager = component_manager
        self.obs_state_model = obs_state_model

        self.timeout_id = f"{time.time()}_{__class__.__name__}"

        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: TaskCallbackType = self.task_callback_default

    @timeout_tracker
    @error_propagation_tracker("get_subarray_obsstate", [ObsState.IDLE])
    def invoke_end(
        self,
    ) -> None:
        """This is a long running method for End command, it executes do hook,
        invokes End Command SdpSubarrayleafnode.

        :param logger: logger
        :type logger: logging.Logger
        :type task_callback: TaskCallbackType, optional
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """

        self.obs_state_model.perform_action("end_invoked")
        return self.do()

    def do_mid(self, argin=None) -> Tuple[ResultCode, str]:
        """
        Method to invoke End command on CSP Subarray Leaf Node,
        SDP Subarray Leaf Node and Dish Leaf Nodes.

        return:
            A tuple containing a return code and a
            string message indicating execution status of command.

        rtype:
            (ResultCode, str)
        """

        self.update_command_in_progress_id("End")

        try:
            self.component_manager.reset_scan_duration()
            self.component_manager.pattern_scan_data.clear()
        except Exception as exception:
            log_msg = (
                "Execution of End command is failed."
                + f"Reason: Failed to reset the scan duration: {exception}"
            )
            self.logger.exception(log_msg)

            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the scan duration in End command.",
            )
        ret_code, message = self.init_adapters()
        if ret_code == ResultCode.FAILED:
            return ret_code, message

        for method in [
            self.end_sdp,
            self.end_csp,
            self.stop_dish_tracking,
        ]:
            ret_code, message = method()
            if ret_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    ret_code,
                    message,
                )

        return (ResultCode.OK, COMMAND_COMPLETION_MESSAGE)

    def end_csp(self) -> Tuple[ResultCode, str]:
        """
        End command on CSP Subarray Leaf Node
        """
        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_csp_sln_adapter.End()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of End command is failed. "
                + "Reason: End command call failed on CSP Subarray "
                + f"Leaf Node {tmc_csp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking End on CSP Subarray Leaf Node: "
                + f"{tmc_csp_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def end_sdp(self) -> Tuple[ResultCode, str]:
        """
        End command on SDP Subarray Leaf Node
        """
        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_sdp_sln_adapter.End()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of End command is failed."
                + "Reason: End command call failed on SDP Subarray Leaf Node: "
                + f"{tmc_sdp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking End on SDP Subarray Leaf Node "
                + f"{tmc_sdp_sln_adapter.dev_name}: {exception}",
            )
        return ResultCode.OK, ""

    def stop_dish_tracking(self) -> Tuple[ResultCode, str]:
        """
        Method to stop dish tracking
        """
        for adapter, dln_device in zip(
            self.get_dish_adapter_by_device_name(
                self.component_manager.get_tmc_dish_ln_device_names()
            ),
            self.component_manager.get_tmc_dish_ln_device_names(),
        ):
            try:
                result_code, message_or_unique_id = adapter.TrackStop()
                if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                    return result_code[0], message_or_unique_id[0]

                self.update_event_data_storage(
                    dln_device,
                    message_or_unique_id,
                )
            except Exception as exception:
                log_msg = (
                    "Execution of TrackStop command is failed. Reason: End "
                    + f"command call failed on Dish Leaf Nodes: {exception}"
                )
                self.logger.exception(log_msg)

                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "Error while invoking TrackStop on "
                    + f"Dish Leaf Node {adapter.dev_name}: {exception}",
                )
        return ResultCode.OK, ""

    def do_low(self, argin: str = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke End command on MCCS Subarray Leaf Node.

        return:
            A tuple containing a return code and a string
            message indicating execution status of command.

        rtype:
            (ResultCode, str)
        """

        self.update_command_in_progress_id("End")

        try:
            self.component_manager.reset_scan_duration()
        except Exception as exception:
            log_msg = (
                "Execution of End command is failed."
                + f"Reason: Failed to reset the scan duration: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in clearing the scan duration in End command: "
                + f"{exception}",
            )

        ret_code, message = self.init_adapters()
        if ret_code == ResultCode.FAILED:
            return ret_code, message

        for method in [self.end_sdp, self.end_csp, self.end_mccs]:
            ret_code, message = method()
            if ret_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    ret_code,
                    message,
                )

        return (ResultCode.OK, COMMAND_COMPLETION_MESSAGE)

    def end_mccs(self) -> Tuple[ResultCode, str]:
        """
        End command on Mccs Subarray Leaf Node.
        """
        tmc_mccs_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_mccs_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_mccs_sln_adapter.End()
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_mccs_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of End command is failed. "
                + "Reason: End command call failed on Mccs Subarray Leaf Node "
                + f"{tmc_mccs_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                (
                    "Error in calling End on MCCS Subarray Leaf Node %s: %s",
                    tmc_mccs_sln_adapter.dev_name,
                    exception,
                ),
            )
        return ResultCode.OK, ""

    def update_task_status(self, **kwargs) -> None:
        """Method to update task status with result code and exception message
        if any."""

        result = kwargs.get("result")
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("message") or kwargs.get("exception")

        self.logger.info("Updating task status with kwargs: %s", kwargs)

        if status == TaskStatus.ABORTED:
            self.task_callback(
                result=(ResultCode.ABORTED, "Command has been aborted"),
                status=status,
            )
            return
        if result[0] == ResultCode.OK:
            self.logger.info(
                "command_in_progress - %s",
                self.component_manager.command_in_progress,
            )
            self.task_callback(result=result, status=status)
        else:
            self.task_callback(
                result=result,
                status=status,
                exception=Exception(message),
            )

            self.component_manager.command_in_progress = ""
            self.component_manager.command_in_progress_id = None
            self.component_manager.command_result_code = None

            with self.component_manager.event_data_manager.eventlock:
                self.component_manager.event_data_manager.clear_lrcr()

        self.component_manager.all_lrcr_ok = False
        self.component_manager.command_id = ""
