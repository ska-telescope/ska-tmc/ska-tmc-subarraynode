"""
Configure Command class for SubarrayNode.
"""

# pylint: disable = W0611
import json
import time
from copy import deepcopy
from typing import Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import AdapterFactory, TimeKeeper, TimeoutCallback
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

from ska_tmc_subarraynode.commands.build_up_data import (
    ElementDeviceData,
    ElementDeviceDataLow,
    ElementDeviceDataMid,
)
from ska_tmc_subarraynode.commands.subarray_node_command import (
    SubarrayNodeCommand,
)
from ska_tmc_subarraynode.transaction_id import (
    identify_with_id,
    inject_with_id,
)
from ska_tmc_subarraynode.utils.constants import COMMAND_STARTED_MESSAGE
from ska_tmc_subarraynode.utils.subarray_node_utils import (
    split_interface_version,
)


class Configure(SubarrayNodeCommand):
    """
    A class for SubarrayNode's Configure() command.

    Configures the resources assigned to the Subarray.
    The configuration data for SDP, CSP and Dish for mid telescope and MCCS
    for low telescope is extracted out of the input configuration string and
    relayed to the respective underlying devices (SDP Subarray Leaf Node,
    CSP Subarray Leaf Node and Dish Leaf Node for mid telescope and
    MCCS Leaf Node for low telescope ).
    """

    def __init__(
        self,
        component_manager,
        obs_state_model,
        logger,
        adapter_factory=AdapterFactory(),
    ):
        super().__init__(
            component_manager=component_manager,
            adapter_factory=adapter_factory,
            logger=logger,
        )

        self.component_manager = component_manager
        self.obs_state_model = obs_state_model
        self._adapter_factory = adapter_factory
        self.scan_duration = 10
        self.mccs_config = None
        self.only_dish_config_flag: bool = False
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: TaskCallbackType

    @timeout_tracker
    @error_propagation_tracker(
        "get_subarray_obsstate", [ObsState.CONFIGURING, ObsState.READY]
    )
    def invoke_configure(
        self,
        argin: str,
    ) -> Tuple[ResultCode, str]:
        """This is a long running method for Configure command,
        it executes do hook, invoking Configure command.

        :param argin: Input argument for the command
        :type argin: `str`

        :returns: Result code and message
        :rtype: `Tuple[ResultCode, str]`
        """
        self.obs_state_model.perform_action("configure_invoked")
        result_code, message = self.do(argin)
        return result_code, message

    def update_task_status(self, **kwargs) -> None:
        """
        Method to update task status with result code and exception message
        if any.
        """
        result = kwargs.get("result")
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("message") or kwargs.get("exception")
        self.logger.info("Updating task status with kwargs: %s", kwargs)

        if status == TaskStatus.ABORTED:
            self.task_callback(
                result=(ResultCode.ABORTED, "Command has been aborted"),
                status=status,
            )

            return
        if result[0] == ResultCode.OK:
            self.task_callback(result=result, status=status)
        else:
            self.task_callback(
                result=result,
                status=status,
                exception=Exception(message),
            )
            self.component_manager.command_in_progress = ""
            self.component_manager.command_in_progress_id = None
            self.component_manager.command_result_code = None
            # Call for aggregation in case Subsystems raise exceptions
            # and remain in initial obsState IDLE
            event_manager = self.component_manager.event_data_manager
            event_manager.update_aggragation_queue()
        self.clear_device_events()
        self.component_manager.command_id = ""
        self.component_manager.is_partial_configuration = False

    @identify_with_id("configure", "argin")
    def do_mid(self, argin: str) -> Tuple[ResultCode, str]:
        """
        Method to invoke Configure command.

        :param argin: DevString.

        JSON string that includes pointing parameters of Dish -
        Azimuth and Elevation Angle, CSP
        Configuration and SDP Configuration parameters.
        JSON string example is:

        .. code-block::

            {'interface':'https://schema.skao.int/ska-tmc-configure/4.1',
            'transaction_id':'','pointing':{'target':{'reference_frame':'ICRS',
            'target_name':'Polaris Australis','ra':'21:08:47.92',
            'dec':'-88:57:22.9','ca_offset_arcsec':0.0,'ie_offset_arcsec':0.0},
            'correction':'UPDATE'},'csp':{'interface':
            'https://schema.skao.int/ska-csp-configurescan/4.1','common':
            {'config_id':'sbi-mvp01-20200325-00001-science_A',
            'frequency_band':'2','eb_id':'eb-m001-20230712-56789'},'pst':{},
            'transaction_id':'txn-....-00001','midcbf':{'correlation':
            {'processing_regions':[{'fsp_ids':[1],'start_freq':950000000,
            'channel_width':13440,'channel_count':40,'sdp_start_channel_id':0,
            'integration_factor':1}]}}},'sdp':{'interface':
            'https://schema.skao.int/ska-sdp-configure/0.4','scan_type':
            'target:a'},'tmc':{'scan_duration':10.0,'partial_configuration':
            False}}

        Note: While invoking this command from JIVE, provide
            above JSON string without any space.

        return: A tuple containing a return code and a string message
            indicating status.The message is for information purpose only.

        rtype: (ReturnCode, str)
        """
        self.update_command_in_progress_id("Configure")
        self.only_dish_config_flag = False
        try:
            scan_configuration = json.loads(argin)
        except Exception as exception:
            log_msg = (
                "Execution of Configure command is failed."
                + f"Reason: JSON parsing failed with exception: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in loading json in Configure command",
            )

        if "transaction_id" in scan_configuration:
            del scan_configuration["transaction_id"]

        try:
            tmc_configure = scan_configuration["tmc"]
        except KeyError as key_error:
            log_msg = (
                "Execution of Configure command is failed."
                + f"Reason: JSON parsing failed with exception: {key_error}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'tmc' key in the Configure json",
            )

        if tmc_configure.get("partial_configuration"):
            self.only_dish_config_flag = tmc_configure["partial_configuration"]
            # Setting the component manager property for partial configuration
            # which will be used in the update_long_running_command_result
            # method to trigger aggregation.
            self.component_manager.is_partial_configuration = tmc_configure[
                "partial_configuration"
            ]

        try:
            scan_duration = (
                int(tmc_configure.get("scan_duration", 0))
                or self.component_manager.get_scan_duration()
            )
        except KeyError as key_error:
            log_msg = (
                "Execution of Configure command is failed."
                + f"Reason: JSON parsing failed with exception: {key_error}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "JSON Error: Missing 'scan_duration' "
                + "key in the Configure json",
            )

        try:
            self.component_manager.set_scan_duration(scan_duration)
        except Exception as exception:
            log_msg = (
                "Execution of Configure command is failed."
                + "Reason: setting the scan duration failed"
                + f"with exception: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in setting the scan duration in Configure "
                + f"command: {exception}",
            )

        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        return_code, message = self.get_and_verify_k_values()
        if return_code == ResultCode.FAILED:
            self.component_manager.kvalue_validation_failed = True
            return self.component_manager.generate_command_result(
                return_code, message
            )

        if self.component_manager.kvalue_validation_failed:
            self.component_manager.kvalue_validation_failed = False

        self.logger.info(
            "SubarrayNode obsState is: "
            + f"{ObsState(self.obs_state_model.obs_state).name}"
        )

        if self.only_dish_config_flag is False:
            for method in [self._configure_csp, self._configure_sdp]:
                result_code, message = method(scan_configuration)
                if result_code == ResultCode.FAILED:
                    return self.component_manager.generate_command_result(
                        result_code,
                        message,
                    )

        for method in [self._configure_dish, self.check_only_dish_config]:
            result_code, message = method(scan_configuration)
            if result_code == ResultCode.FAILED:
                return self.component_manager.generate_command_result(
                    result_code,
                    message,
                )

        self.logger.info("Configure command is invoked from SubarrayNode.")

        return (ResultCode.STARTED, COMMAND_STARTED_MESSAGE)

    def _configure_sdp(
        self, scan_configuration: dict
    ) -> Tuple[ResultCode, str]:
        """Method to configure Sdp Subarray"""
        sdp_data = ElementDeviceData(
            scan_configuration, self.component_manager
        )
        result_code, cmd_data = sdp_data.build_up_sdp_cmd_data()
        if result_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                result_code,
                cmd_data,
            )

        tmc_sdp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_sdp_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_sdp_sln_adapter.Configure(
                cmd_data
            )
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_sdp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of Configure command is failed. "
                + "Reason: Error while invoking Configure in SDP Subarray "
                + f"Leaf Node {tmc_sdp_sln_adapter.dev_name}: {exception}"
            )

            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Configure in "
                + "SDP Subarray Leaf Node: "
                + f"{tmc_sdp_sln_adapter.dev_name}: {exception}",
            )

        return ResultCode.OK, ""

    def _configure_csp(
        self, scan_configuration: dict
    ) -> Tuple[ResultCode, str]:
        """Method to configure Csp Subarray"""
        delay_model_subscription = (
            self.component_manager.get_tmc_csp_sln_device_name()
            + "/delayModel"
        )
        tmc_major = self.component_manager.tmc_major
        tmc_minor = self.component_manager.tmc_minor
        if (tmc_major, tmc_minor) >= (4, 0):
            # Add the entries according to ADR-99
            csp_interface = scan_configuration["csp"]["interface"]
            (csp_minor, csp_major) = split_interface_version(csp_interface)
            if (csp_minor, csp_major) >= (0, 4):
                scan_configuration["csp"]["common"][
                    "subarray_id"
                ] = self.component_manager.get_subarray_id()
                scan_configuration["csp"]["midcbf"][
                    "delay_model_subscription_point"
                ] = delay_model_subscription
                for instances in scan_configuration["csp"]["midcbf"][
                    "correlation"
                ]["processing_regions"]:
                    self.logger.info(f"instances:: {type(instances)}")
                    instances["output_link_map"] = []
                    instances["output_link_map"].append(
                        [instances["sdp_start_channel_id"], 1]
                    )
        else:
            scan_configuration["csp"]["cbf"][
                "delay_model_subscription_point"
            ] = delay_model_subscription
        self.logger.debug(f"Scan configuration for CSP: {scan_configuration}")
        dev_name = self.component_manager.get_sdp_subarray_dev_name()
        sdp_subarray_info = self.component_manager.get_device(dev_name)
        csp_data = ElementDeviceDataMid(
            scan_configuration, self.component_manager
        )
        result_code, cmd_data = csp_data.build_up_csp_cmd_data(
            sdp_subarray_info.receive_addresses
        )
        if result_code == ResultCode.FAILED:
            return result_code, cmd_data

        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        try:
            result_code, message_or_unique_id = tmc_csp_sln_adapter.Configure(
                cmd_data
            )
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of Configure command is failed. "
                + "Reason: Error while invoking Configure in CSP Subarray "
                + f"Leaf Node {tmc_csp_sln_adapter.dev_name}:{exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Configure in "
                + "CSP Subarray Leaf Node: "
                + f"{tmc_csp_sln_adapter.dev_name}: {exception}",
            )

        return ResultCode.OK, ""

    def configure_adapters(self, adapters, config) -> Tuple[ResultCode, str]:
        "Invoke configure command on adapters"
        for adapter in adapters:
            self.logger.info(f"Invoking configure on dish {adapter.dev_name}")
            try:
                result_code, unique_id = adapter.Configure(config)
                if result_code[0] in (
                    ResultCode.FAILED,
                    ResultCode.REJECTED,
                ):
                    return result_code[0], unique_id[0]

                # Store the unique_id for tracking
                self.update_event_data_storage(adapter.dev_name, unique_id)
                self.component_manager.dish_unique_ids[
                    adapter.dev_name
                ] = unique_id[0]
            except Exception as exception:
                self.logger.exception(
                    "Execution of Configure command failed on "
                    + "Dish Leaf Node -> %s: %s",
                    adapter.dev_name,
                    exception,
                )
                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "Error while invoking Configure on Dish Leaf "
                    + f"Node -> {adapter.dev_name}: {exception}",
                )
        return ResultCode.OK, ""

    @inject_with_id(0, "scan_configuration")
    def _configure_dish(
        self, scan_configuration: dict
    ) -> Tuple[ResultCode, str]:
        """Method to configure Dishes"""
        dish_scan_config = json.dumps(scan_configuration)

        # Check if it is a Partial Configuration
        if not self.only_dish_config_flag:
            dish_data = ElementDeviceDataMid(
                scan_configuration, self.component_manager
            )
            result_code, dish_scan_config = dish_data.build_up_dish_cmd_data()
            if result_code == ResultCode.FAILED:
                return result_code, dish_scan_config
        else:
            # Manually setting subarray obsState to CONFIGURING
            self.component_manager.subarray_obsstate = ObsState.CONFIGURING
        dish_scan_config_holography = json.loads(dish_scan_config)
        # Retrieve the receptor groups from the JSON
        pointing_groups = dish_scan_config_holography.get("pointing", {}).get(
            "groups", []
        )
        # Get all dish adapters and map them by last three digits of names
        tmc_leaf_dish_adapters = self.get_dish_adapter_by_device_name(
            self.component_manager.get_tmc_dish_ln_device_names()
        )
        adapter_map = {
            adapter.dev_name[-3:]: adapter
            for adapter in tmc_leaf_dish_adapters
        }
        # Loop through each group in the JSON
        # If no groups are specified, configure all receptors
        if not pointing_groups:
            self.logger.info(
                "No pointing groups specified; configuring all receptors."
            )
            return self.configure_adapters(
                tmc_leaf_dish_adapters, dish_scan_config
            )
        return self.configure_mapping_scan_dishes(
            pointing_groups, dish_scan_config_holography, adapter_map
        )

    def configure_mapping_scan_dishes(
        self, pointing_groups, dish_scan_config_holography, adapter_map
    ):
        """Method to configure Dishes based on receptor groups in JSON."""
        for group in pointing_groups:
            receptors = group.get("receptors", [])
            group_config = {"field": group.get("field")}

            # Add trajectory only if it's provided
            if "trajectory" in group:
                group_config["trajectory"] = group["trajectory"]

            # Add projection only if it's provided
            if "projection" in group:
                group_config["projection"] = group["projection"]

            group_scan_config = json.dumps(
                {**dish_scan_config_holography, **{"pointing": group_config}}
            )

            self.logger.info(f"Holography JSON for group: {group_scan_config}")
            adapters_to_configure = [
                adapter_map.get(receptor[-3:])
                for receptor in receptors
                if adapter_map.get(receptor[-3:])
            ]
            result_code, result_message = self.configure_adapters(
                adapters_to_configure, group_scan_config
            )
            if result_code == ResultCode.FAILED:
                return result_code, result_message

        return ResultCode.OK, ""

    def check_only_dish_config(
        self, scan_configuration: dict
    ) -> Tuple[ResultCode, str]:
        """Method to check only dish configuration"""
        if not self.only_dish_config_flag:
            try:
                config_keys = scan_configuration.keys()
                if "dish" in config_keys and not set(["sdp", "csp"]).issubset(
                    config_keys
                ):
                    self.only_dish_config_flag = True
                else:
                    self.only_dish_config_flag = False
            except Exception as exception:
                self.logger.exception(
                    "Execution of Configure command is failed. Reason: Error "
                    + "in checking whether only dishes need to be configured "
                    + ": %s",
                    exception,
                )
                return self.component_manager.generate_command_result(
                    ResultCode.FAILED,
                    "Error in checking whether only dishes need "
                    + f"to be configured: {exception}.",
                )

        return ResultCode.OK, ""

    def get_and_verify_k_values(self) -> Tuple[ResultCode, str]:
        """
        Retrieves the k-values from dish leaf node adapters
        and verifies if they are all unique or the same.

        return:
            A tuple containing a return code and a string message.
        """
        k_values = []
        # Iterate over receptor_list to extract dish IDs
        tmc_leaf_dish_adapters = self.get_dish_adapter_by_device_name(
            self.component_manager.get_tmc_dish_ln_device_names()
        )

        # Iterate over self.dish_ln to retrieve k-values
        try:
            # Retrieve k-value attribute from adapter
            k_values = [
                adapter.kValue
                for adapter in tmc_leaf_dish_adapters
                if adapter.kValue
            ]
        except Exception as exception:
            return (
                ResultCode.FAILED,
                f"Failed to retrieve k-values: {exception}",
            )

        # Check if all k-values are either unique or all same
        unique_values = set(k_values)
        if len(unique_values) == 1 or len(unique_values) == len(k_values):
            return ResultCode.OK, ""

        return (
            ResultCode.FAILED,
            "Configure command failed: "
            + "Error while invoking Configure command: "
            + "K-values must be either all same or all different.",
        )

    @identify_with_id("configure", "argin")
    def do_low(self, argin: str) -> Tuple[ResultCode, str]:
        """
        This method executes the Configure workflow of the Subarray Node Low.
        It will invoke Configure command on the CSP
        Subarray Leaf Node, SDP Subarray Leaf Node

        :param argin: DevString.

        .. code-block::

            {'interface': 'https://schema.skao.int/ska-low-tmc-configure/4.1',
            'transaction_id': 'txn-....-00001', 'mccs': {'subarray_beams':
            [{'subarray_beam_id': 1, 'update_rate': 0.0, 'logical_bands':
            [{'start_channel': 80, 'number_of_channels': 16}, {'start_channel'
            : 384, 'number_of_channels': 16}], 'apertures': [{'aperture_id':
            'AP001.01', 'weighting_key_ref': 'aperture2'}, {'aperture_id':
            'AP002.01', 'weighting_key_ref': 'aperture2'}],
            'sky_coordinates': {'reference_frame': 'ICRS',
            'c1': 180.0, 'c2': 45.0}}]}, 'sdp':
            {'interface': 'https://schema.skao.int/ska-sdp-configure/0.4',
            'scan_type': 'target:a'}, 'csp': {'interface':
            'https://schema.skao.int/ska-low-csp-configure/3.2',
            'common': {'config_id': 'sbi-mvp01-20200325-00001-science_A',
             'eb_id': 'eb-test-20220916-00000'}, 'lowcbf':
             {'stations': {'stns': [[1, 1], [2, 1], [3, 1],
            [4, 1], [5, 1], [6, 1]], 'stn_beams': [{'beam_id': 1,
             'freq_ids': [400]}]}, 'vis': {'fsp': {'firmware': 'vis',
            'fsp_ids': [1]}, 'stn_beams': [{'stn_beam_id': 1,
            'integration_ms': 849}]}, 'timing_beams': {'fsp':
            {'firmware': 'pst', 'fsp_ids': [2]}, 'beams':
            [{'pst_beam_id': 1, 'field': {'target_name':
            'PSR J0024-7204R', 'reference_frame': 'icrs',
            'attrs': {'c1': 6.023625, 'c2': -72.08128333,
            'pm_c1': 4.8, 'pm_c2': -3.3}}, 'stn_beam_id':
            1, 'stn_weights': [0.9, 1.0, 1.0, 1.0, 0.9,
            1.0]}]}}, 'pst': {'beams': [{'beam_id': 1,
            'scan': {'activation_time':'2022-01-19T23:07:45Z',
            'bits_per_sample': 32, 'num_of_polarizations': 2,
            'udp_nsamp': 32, 'wt_nsamp': 32, 'udp_nchan': 24,
            'num_frequency_channels': 432, 'centre_frequency':
            200000000.0, 'total_bandwidth': 1562500.0,
            'observation_mode': 'VOLTAGE_RECORDER',
            'observer_id': 'jdoe', 'project_id': 'project1', 'pointing_id':
            'pointing1', 'source': 'J1921+2153', 'itrf':
            [5109360.133, 2006852.586, -3238948.127], 'receiver_id':
            'receiver3', 'feed_polarization': 'LIN', 'feed_handedness': 1,
            'feed_angle': 1.234, 'feed_tracking_mode': 'FA',
            'feed_position_angle': 10.0, 'oversampling_ratio': [8, 7],
            'coordinates': {'equinox': 2000.0, 'ra': '19:21:44.815',
            'dec': '21:53:02.400'}, 'max_scan_length': 20000.0,
            'subint_duration': 30.0, 'receptors': ['receptor1',
            'receptor2'], 'receptor_weights': [0.4, 0.6],
            'num_channelization_stages': 2, 'channelization_stages':
            [{'num_filter_taps': 1, 'filter_coefficients':
            [1.0], 'num_frequency_channels': 1024, 'oversampling_ratio':
            [32, 27]}, {'num_filter_taps': 1, 'filter_coefficients':
            [1.0], 'num_frequency_channels': 256, 'oversampling_ratio':
            [4, 3]}]}}]}}, 'tmc': {'scan_duration': 10.0}}

        return:
            A tuple containing a return code and a string
            message indicating status.
            The message is for information purpose only.

        rtype:
            (ReturnCode, str)
        """
        self.logger.debug(f"Configure argin:{argin}")
        self.update_command_in_progress_id("Configure")

        try:
            scan_configuration = json.loads(argin)
            if "transaction_id" in scan_configuration:
                scan_configuration.pop("transaction_id")
            tmc_configure = scan_configuration["tmc"]
            self.scan_duration = int(tmc_configure["scan_duration"])
            self.mccs_config = deepcopy(scan_configuration["mccs"])
        except Exception as exception:
            return self.component_manager.generate_command_result(
                ResultCode.FAILED, str(exception)
            )
        try:
            self.component_manager.set_scan_duration(self.scan_duration)
        except Exception as exception:
            log_msg = (
                "Execution of Configure command is failed. "
                + "Reason: Error in setting the scan duration "
                + f"in Configure command: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error in setting the scan duration in Configure "
                + f"command: {exception}",
            )

        result_code, message = self.init_adapters()
        if result_code == ResultCode.FAILED:
            return result_code, message

        # Delete the "target" key from mccs_config
        if self.mccs_config["subarray_beams"][0].get("target"):
            del self.mccs_config["subarray_beams"][0]["target"]

        result_code, message = self._configure_mccs_subarray(self.mccs_config)
        if result_code == ResultCode.FAILED:
            return result_code, message

        result_code, message = self.configure_low_csp(scan_configuration)
        if result_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                result_code,
                message,
            )

        result_code, message = self._configure_sdp(scan_configuration)
        if result_code == ResultCode.FAILED:
            return self.component_manager.generate_command_result(
                result_code,
                message,
            )

        self.logger.info("Configure command invoked on TMC SubarrayNode")

        return (ResultCode.STARTED, COMMAND_STARTED_MESSAGE)

    def _configure_mccs_subarray(self, mccs_config):
        """
        Configure the MCCS Subarray Leaf Node.

        This method sends the Configure command to the MCCS Subarray Leaf Node
        with the provided MCCS configuration.

        :param mccs_config: A dictionary containing the MCCS configuration.
        :type mccs_config: dict

        :return: A tuple containing a return code and a string message
            indicating the status. The message is for information purposes only
        :rtype: Tuple[ResultCode, str]
        """
        mccs_config[
            "interface"
        ] = self.component_manager.mccs_configure_interface

        input_to_mccs = json.dumps(mccs_config)

        tmc_mccs_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_mccs_sln_device_name()
        )

        try:
            result_code, message_or_unique_id = tmc_mccs_sln_adapter.Configure(
                input_to_mccs
            )
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_mccs_sln_device_name(),
                message_or_unique_id,
            )
        except Exception as exception:
            log_msg = (
                "Execution of Configure command is failed. "
                + "Reason: Error in calling Configure command on MCCS Subarray"
                + f" Leaf Node {tmc_mccs_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                (
                    "Error in calling Configure in MCCS"
                    + " Subarray Leaf Node %s: %s",
                    tmc_mccs_sln_adapter.dev_name,
                    exception,
                ),
            )
        return ResultCode.OK, ""

    def configure_low_csp(self, scan_config: dict) -> Tuple[ResultCode, str]:
        """Method to configure low CSP"""
        scan_config["csp"]["common"][
            "subarray_id"
        ] = self.component_manager.get_subarray_id()
        delay_model_subscription = (
            self.component_manager.get_tmc_csp_sln_device_name()
            + "/delayModel"
        )

        # Updating delay model subscription for each station beam
        for station_beam in scan_config["csp"]["lowcbf"]["stations"][
            "stn_beams"
        ]:
            station_beam["delay_poly"] = delay_model_subscription

        pst_delay_model_subscription = (
            self.component_manager.get_tmc_csp_sln_device_name()
            + "/delayModelPSTBeam1"
        )
        # Updating delay model subscription for each pst beam
        if scan_config["csp"]["lowcbf"].get("timing_beams"):
            for beam in scan_config["csp"]["lowcbf"]["timing_beams"]["beams"]:
                beam["delay_poly"] = pst_delay_model_subscription
                beam["jones"] = self.component_manager.jones_uri

        dev_name = self.component_manager.get_sdp_subarray_dev_name()
        sdp_subarray_info = self.component_manager.get_device(dev_name)

        csp_data = ElementDeviceDataLow(scan_config, self.component_manager)
        (
            result_code,
            cmd_data_or_message,
        ) = csp_data.build_up_csp_cmd_data(sdp_subarray_info.receive_addresses)

        if result_code == ResultCode.FAILED:
            return result_code, cmd_data_or_message

        tmc_csp_sln_adapter = self.get_adapter_by_device_name(
            self.component_manager.get_tmc_csp_sln_device_name()
        )
        scan_config["csp"] = json.loads(cmd_data_or_message)
        try:
            result_code, message_or_unique_id = tmc_csp_sln_adapter.Configure(
                json.dumps(scan_config)
            )
            if result_code[0] in (ResultCode.FAILED, ResultCode.REJECTED):
                return result_code[0], message_or_unique_id[0]

            self.update_event_data_storage(
                self.component_manager.get_tmc_csp_sln_device_name(),
                message_or_unique_id,
            )

        except Exception as exception:
            log_msg = (
                "Execution of Configure command is failed. "
                + "Reason: Error while invoking Configure in Low "
                + "CSP Subarray Leaf Node "
                + f"{tmc_csp_sln_adapter.dev_name}: {exception}"
            )
            self.logger.exception(log_msg)
            return self.component_manager.generate_command_result(
                ResultCode.FAILED,
                "Error while invoking Configure in low CSP Subarray Leaf Node "
                + f"{tmc_csp_sln_adapter.dev_name}: {exception}",
            )

        return ResultCode.OK, ""
