"""
Subarray Node Mid provides the monitoring and control interface
required by users as well as other TM Components
(such as OET, Central Node) for a Subarray Mid.
"""
from ska_tango_base.commands import ResultCode
from tango.server import device_property, run

from ska_tmc_subarraynode.manager.subarray_node_component_manager_mid import (
    SubarrayNodeComponentManagerMid,
)
from ska_tmc_subarraynode.model.input import InputParameterMid
from ska_tmc_subarraynode.subarray_node import BaseSubarrayNode

_all__ = [
    "MidTmcSubarray",
    "main",
]


class MidTmcSubarray(BaseSubarrayNode):
    """
    Provides the monitoring and control interface required by users as well as
    other TM Components (such as OET, Central Node) for a Subarray.

    :Device Properties:

        DishLeafNodePrefix:
            Device name prefix for the Dish Leaf Node.

    :Device Attributes:

    """

    # -----------------
    # Device Properties
    # -----------------

    DishLeafNodePrefix = device_property(
        dtype="str",
        doc="Device name prefix for the Dish Leaf Node",
    )

    DishMasterFQDNs = device_property(
        dtype=("str",),
        doc="List of Dish Master devices",
        default_value=tuple(),
    )

    DishMasterIdentifier = device_property(
        dtype="str",
        doc="Device name tag for the Dish Master",
    )

    # ---------------
    # General methods
    # ---------------

    class InitCommand(BaseSubarrayNode.InitCommand):
        """
        A class for the TMC MidTmcSubarray's init_device() method.
        """

        def do(self):
            """
            Initializes the attributes and properties of the
             Subarray Node Mid.

            return:
                A tuple containing a return code and a string
                message indicating status.
                The message is for information purpose only.

            rtype:
                (ReturnCode, str)

            raises:
                DevFailed if the error while subscribing the tango attribute
            """
            super().do()
            return ResultCode.OK, ""

    def create_component_manager(self):
        super().create_component_manager()

        cm = SubarrayNodeComponentManagerMid(
            self.op_state_model,
            self.obs_state_model,
            self._command_tracker,
            _input_parameter=InputParameterMid(None),
            dev_family=self.DevFamily,
            logger=self.logger,
            _update_device_callback=self.update_device_callback,
            _update_subarray_health_state_callback=(
                self.update_subarray_health_state_callback
            ),
            _update_assigned_resources_callback=(
                self.update_assigned_resources_callback
            ),
            _update_subarray_availability_status_callback=(
                self.update_subarray_availability_status_callback
            ),
            communication_state_changed_callback=None,
            component_state_changed_callback=(
                self._component_state_changed_callback
            ),
            command_timeout=self.CommandTimeOut,
            abort_command_timeout=self.AbortCommandTimeOut,
            proxy_timeout=self.ProxyTimeout,
            event_subscription_check_period=self.EventSubscriptionCheckPeriod,
            liveliness_check_period=self.LivelinessCheckPeriod,
            csp_assign_interface=self.CspAssignResourcesInterfaceURL,
            csp_scan_interface=self.CspScanInterfaceURL,
            sdp_scan_interface=self.SdpScanInterfaceURL,
        )

        cm.input_parameter.tmc_dish_ln_device_names = []
        cm.input_parameter.dish_dev_names = []
        cm.input_parameter.dish_fqdn = []
        cm.input_parameter.dish_ln_prefix = self.DishLeafNodePrefix or ""
        cm.input_parameter.dish_master_identifier = (
            self.DishMasterIdentifier or ""
        )

        for dish_name in self.DishMasterFQDNs:
            if ("ska" in dish_name) or ("SKA" in dish_name):
                cm.input_parameter.dish_fqdn.append(dish_name)
        self.logger.info("Dish FQDNs are: %s", cm.input_parameter.dish_fqdn)
        cm.input_parameter.tmc_csp_sln_device_name = (
            self.CspSubarrayLNFQDN or ""
        )
        cm.input_parameter.tmc_sdp_sln_device_name = (
            self.SdpSubarrayLNFQDN or ""
        )
        cm.input_parameter._csp_subarray_dev_name = self.CspSubarrayFQDN or ""
        cm.input_parameter._sdp_subarray_dev_name = self.SdpSubarrayFQDN or ""
        cm.update_input_parameter()

        return cm


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """
    Runs the SubarrayNode.
    :param args: Arguments internal to TANGO
    :param kwargs: Arguments internal to TANGO
    :return: SubarrayNode TANGO object.
    """
    return run((MidTmcSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
