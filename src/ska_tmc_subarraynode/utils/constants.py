"""
Common constant used in subarraynode

"""
REQUIRED_MID_CONFIGURE_KEYS = [
    "pointing",
    "dish",
    "csp",
    "sdp",
    "tmc",
]

# Only TMC key needs to be validated using custom validations, rest are handled
# by CDM.
REQUIRED_LOW_CONFIGURE_KEYS = ["tmc"]

REQUIRED_LOW_SCAN_KEYS = ["scan_id"]

MAJOR_CONFIG_VERSION = 2
MINOR_CONFIG_VERSION = 2
ERROR_MESSAGE = "Malformed input string. Missing key: '{key}'"
REQUIRED_KEYS: dict[str, list] = {
    "MID": REQUIRED_MID_CONFIGURE_KEYS,
    "LOW": REQUIRED_LOW_CONFIGURE_KEYS,
}

NUMBER_OF_DEVICE_EVENTS_COUNT_MID = 2
NUMBER_OF_DEVICE_EVENTS_COUNT_LOW = 3

COMMAND_COMPLETION_MESSAGE = "Command Completed"
COMMAND_STARTED_MESSAGE = "Command Started"

OSO_TMC_LOW_SCAN_INTERFACE = "https://schema.skao.int/ska-low-tmc-scan/4.0"
OSO_TMC_LOW_CONFIGURE_INTERFACE = (
    "https://schema.skao.int/ska-low-tmc-configure/4.1"
)
OSO_TMC_MID_CONFIGURE_INTERFACE = (
    "https://schema.skao.int/ska-tmc-configure/4.1"
)
