"""
This module is inherited from SubarrayNodeComponentManager.

It is component Manager for Low Telecope.

It is provided for explanatory purposes, and to support testing of this
package.
"""
import json
import re
from datetime import datetime
from typing import Tuple

from ska_control_model import ObsState
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tmc_common import SubArrayDeviceInfo

from ska_tmc_subarraynode.manager.aggregate_process import (
    AggregationProcessLow,
)
from ska_tmc_subarraynode.manager.aggregators import (
    HealthStateAggregatorLow,
    SubarrayAvailabilityAggregatorLow,
)
from ska_tmc_subarraynode.manager.subarray_node_component_manager import (
    SubarrayNodeComponentManager,
)
from ska_tmc_subarraynode.model.component import SubarrayComponent
from ska_tmc_subarraynode.utils.constants import (
    NUMBER_OF_DEVICE_EVENTS_COUNT_LOW,
)

SLEEP_TIME = 0.2
TIMEOUT = 80


class SubarrayNodeComponentManagerLow(SubarrayNodeComponentManager):
    """
    Initialise a new ComponentManager instance for low.
    """

    def __init__(
        self,
        op_state_model,
        obs_state_model,
        _command_tracker,
        _input_parameter,
        dev_family,
        logger=None,
        _component=None,
        _liveliness_probe: bool = True,
        _event_receiver: bool = True,
        component_state_changed_callback=None,
        communication_state_changed_callback=None,
        _update_device_callback=None,
        _update_subarray_health_state_callback=None,
        _update_assigned_resources_callback=None,
        _update_subarray_availability_status_callback=None,
        command_timeout: int = 30,
        abort_command_timeout: int = 40,
        proxy_timeout: int = 500,
        event_subscription_check_period=1,
        liveliness_check_period=1,
        csp_assign_interface: str = "https://schema.skao.int/"
        + "ska-low-csp-assignresources/3.0",
        csp_scan_interface: str = "https://schema.skao.int/"
        + "ska-low-csp-scan/0.0",
        sdp_scan_interface: str = "https://schema.skao.int/"
        + "ska-sdp-scan/0.4",
        mccs_configure_interface: str = "https://schema.skao.int/"
        + "ska-low-mccs-configure/1.0",
        mccs_scan_interface: str = "https://schema.skao.int/"
        + "ska-low-mccs-scan/3.0",
        jones_uri: str = "tango://jones.skao.int/low/stn-beam/1",
    ):
        """
        Initialise a new ComponentManager instance for low.

        :param op_state_model: the op state model used by this component
            manager
        :param logger: a logger for this component manager
        :param _component: allows setting of the component to be
            managed; for testing purposes only
        :param _input_parameter : specify input parameter for low and mid.
        :param _liveliness_probe:allows to enable/disable LivelinessProbe usage
        :param _event_receiver : allows to enable/disable EventReceiver usage
        :param max_workers: Optional. Maximum worker threads for
            monitoring purpose.
        :param proxy_timeout: Optional. Time period to wait for
            event and responses.
        :param event_subscription_check_period: (int) Time in seconds for sleep
            intervals in the event subsription thread.
        :param liveliness_check_period: (int) Period for the liveliness probe
            to monitor each device in a loop
        :param timeout : Optional. Time period to wait for
            intialization of adapter.


        """

        super().__init__(
            op_state_model,
            obs_state_model=obs_state_model,
            _command_tracker=_command_tracker,
            _input_parameter=_input_parameter,
            dev_family=dev_family,
            _event_receiver=_event_receiver,
            _liveliness_probe=_liveliness_probe,
            command_timeout=command_timeout,
            abort_command_timeout=abort_command_timeout,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
            _component=_component or SubarrayComponent(logger),
            logger=logger,
            component_state_changed_callback=(
                component_state_changed_callback
            ),
            _update_device_callback=_update_device_callback,
            _update_subarray_health_state_callback=(
                _update_subarray_health_state_callback
            ),
            _update_assigned_resources_callback=(
                _update_assigned_resources_callback
            ),
            _update_subarray_availability_status_callback=(
                _update_subarray_availability_status_callback
            ),
            csp_scan_interface=csp_scan_interface,
            sdp_scan_interface=sdp_scan_interface,
            csp_assign_interface=csp_assign_interface,
            jones_uri=jones_uri,
        )
        self.dev_family = dev_family
        self.device_events_to_consider_for_aggregation: int = 3
        self.mccs_configure_interface: str = mccs_configure_interface
        self.mccs_scan_interface: str = mccs_scan_interface
        # start the aggregation process
        self.aggregation_process = AggregationProcessLow(
            self.event_data_queue,
            self.aggregated_obs_state,
            self.aggregate_value_update_event,
        )
        self.aggregation_process.start_aggregation_process()

    def add_device_to_lp(self, device_name: str) -> None:
        """
        Add device to the liveliness probe

        :param device_name: device name
        :type device_name: str
        """

        if device_name is None:
            return
        if ("sdp" in device_name.lower()) or ("csp" in device_name.lower()):
            self.add_similar_low_mid_device(device_name)
        elif "mccs" in device_name.lower():
            dev_info = SubArrayDeviceInfo(device_name, False)
            self.component.update_device(dev_info)
            if self.liveliness_probe:
                self.liveliness_probe.add_device(device_name)

    def _aggregate_health_state(self) -> None:
        """
        Aggregates all health states
        and call the relative callback if available
        """
        self._subarray_health_state_aggregator = HealthStateAggregatorLow(
            self, self.logger
        )
        super()._aggregate_health_state()

    def _aggregate_subarray_device_availability(self):
        """
        Aggregates subarrays availability
        and call the relative callback if available
        """
        if self._subarray_availability_aggregator is None:
            self._subarray_availability_aggregator = (
                SubarrayAvailabilityAggregatorLow(self, self.logger)
            )

        return super()._aggregate_subarray_device_availability()

    def get_devices_availability_dict(self):
        """Returns the dictionary with the devices and it's availabiltiy"""
        return self._subarray_availability_aggregator.devices_availability_dict

    @property
    def configured_capabilities(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def deconfigure(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def obsreset(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def release(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def standby(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def start_communicating(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def stop_communicating(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def update_long_running_command_result(
        self, dev_name: str, value: Tuple[str, str], timestamp: datetime
    ) -> None:
        """Stores the longRunningCommandResult values for devices and Updates
        the long_running_result_callback if the required criteria is met (i.e
        received events from all the expected devices for a given command).

        :param dev_name: Device name for which the longRunningCommandResult
            event was received
        :type dev_name: `str`
        :param value: longRunningCommandResult event value
        :type value: `Tuple[str, str]`

        :returns: None
        """
        unique_id = value[0]
        result, message = json.loads(value[1])

        self.logger.info(
            "Received longRunningCommandResult event for"
            + " device: %s, with value: %s",
            dev_name,
            value,
        )

        if self._should_ignore_lrcr_event(value):
            return

        command_name = re.findall(r"([A-Za-z]+)", unique_id)[0]
        if command_name != self.command_in_progress:
            self.logger.debug(
                "Ignoring event for command '%s' as no "
                "matching command is in progress.",
                command_name,
            )
            return

        if not value == ("", ""):
            value_to_be_stored = ([value[0]], value[1])
            self.event_data_manager.update_event_data(
                device=dev_name,
                data=value_to_be_stored,
                received_timestamp=timestamp,
                data_type="CommandResultData",
            )

        if self.command_in_progress in [
            "AssignResources",
            "ReleaseAllResources",
        ]:
            # If the command is Assign or Release, MCCS SLN is not involved,
            # so only SDP, and CSP Leaf Nodes are considered
            self.device_events_to_consider_for_aggregation = 2
        elif self.command_in_progress in [
            "Configure",
            "End",
            "EndScan",
            "Scan",
        ]:
            self.device_events_to_consider_for_aggregation = 3
        else:
            return
        try:
            if not self.device_events.get(self.command_in_progress_id):
                self.device_events[self.command_in_progress_id] = {}
                self.error_dict[self.command_in_progress_id] = {}
            unique_id, result_code_message = value
            if unique_id.endswith(self.command_in_progress):
                if not result_code_message:
                    pass
                result_code, message = json.loads(result_code_message)
                if int(result_code) == ResultCode.OK:
                    self.device_events[self.command_in_progress_id][
                        dev_name
                    ] = result_code
                # propogate the error when long running command
                # result event with result code not OK is received
                elif result_code in [
                    ResultCode.REJECTED,
                    ResultCode.FAILED,
                    ResultCode.NOT_ALLOWED,
                ]:
                    self.logger.debug(
                        "Processing exception event on device: %s for "
                        + "command %s with id: %s and value: %s",
                        dev_name,
                        self.command_in_progress,
                        self.command_in_progress_id,
                        value,
                    )
                    self.error_dict[self.command_in_progress_id][
                        dev_name
                    ] = message
                    self.device_events[self.command_in_progress_id][
                        dev_name
                    ] = message
        except StateModelError as state_model_error:
            self.logger.error(
                "State Model Error occurred for "
                + "command id: %s and error: %s",
                self.command_in_progress_id,
                state_model_error,
            )
        if (
            len(self.device_events.get(self.command_in_progress_id, []))
            != self.device_events_to_consider_for_aggregation
        ):
            # Not enough events.
            pass
        elif self.error_dict.get(self.command_in_progress_id):
            exception_message = "Exception occurred on the following devices: "
            for devname, error_value in sorted(
                self.error_dict[self.command_in_progress_id].items()
            ):
                exception_message += f"{devname}: {error_value}\n"

            self.long_running_result_callback(
                self.command_id,
                ResultCode.FAILED,
                exception_msg=exception_message,
            )
            self.observable.notify_observers(command_exception=True)
        elif (
            len(self.device_events.get(self.command_in_progress_id, []))
            == self.device_events_to_consider_for_aggregation
        ):
            if self.command_in_progress == "Scan":
                self.all_lrcr_ok = True
                self.observable.notify_observers(attribute_value_change=True)

        self.logger.debug(
            "The Csp Sdp Mccs events dictionary is: %s",
            self.device_events,
        )

    # This method is overridden from common component manager as MCCS SLN also
    # needs to be considered for Configure.
    def check_successive_configure_revise_obsstate(
        self,
        command_in_progress_id: int,
        device_events: dict,
        error_dict: dict,
    ) -> ObsState:
        """
        This method checks for successive configure resources and
        evaluates final obsstate
        :param command_in_progress_id: command in progress id
        :type command_in_progress_id: str
        :param device_events: events for each device
        :type device_events: dict
        :param error_dict: error dict
        :type error_dict: dict
        :return: ObsState
        :rtype: ObsState
        """
        # Check if aggregation is triggred by command execution and both
        # csp and sdp longRunningCommandResult events received with
        # ResultCode.OK
        # TODO :: We need to revisit aggragation logic in near future
        # TMC currently uses aggragation logic to decide final Obsstate .
        # But then based on exceptional cases , TMC is not considering it
        # as final state and performing revisions.
        # This might needs to be revisited

        if (
            command_in_progress_id
            and (
                len(device_events.get(command_in_progress_id, []))
                == NUMBER_OF_DEVICE_EVENTS_COUNT_LOW
            )
            and not error_dict.get(command_in_progress_id)
        ):
            self.logger.info(
                "Returning to obsState READY from successive configure logic"
            )
            return ObsState.READY

        return ObsState.CONFIGURING

    def stop_aggregation_process(self):
        """Stop aggregation process"""
        self.aggregation_process.stop_aggregation_process()
