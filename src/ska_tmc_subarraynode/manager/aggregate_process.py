"""
This module contain process for aggregation
"""
import logging
import queue
from dataclasses import asdict
from multiprocessing import Event, Process, Queue

from ska_ser_logging import configure_logging

from ska_tmc_subarraynode.manager.aggregators import ObsStateAggregator
from ska_tmc_subarraynode.manager.transition_rules.obs_state_rules import (
    OBS_STATE_RULES_LOW,
    OBS_STATE_RULES_MID,
)

configure_logging("DEBUG")

LOGGER = logging.getLogger(__name__)


class AggregationProcess:
    """Aggregation Process Class"""

    def __init__(
        self,
        event_data_queue: Queue,
        aggregated_obs_state,
        aggregate_update_event,
        callback=None,
    ):
        """
        :param event_data_queue: Queue to track event data
        :type event_data_queue: multiprocessing queue
        :param aggregated_obs_state: Aggregated obsState updated after
        aggregation
        this is shared variable between process
        :type aggregated_obs_state: list
        :param callback: Callback method reference used when aggregated
        obsState is updated
        """
        self.obs_state_aggregator = None
        self.event_data_queue = event_data_queue
        self.aggregated_obs_state = aggregated_obs_state
        # Test this callback if it works with process as a part of HM-497
        # For now provided provision to provide callback in aggregation process
        self.callback = callback
        self.aggregate_update_event = aggregate_update_event
        self.aggregation_process = Process(
            target=self.run_aggregation_process, name="aggregation_process"
        )
        self.aggregation_process_alive_event = Event()

    def run_aggregation_process(self):
        """This process run when subarray node start
        and check if any data receive in queue then get
        data from queue and call aggregate method
        """
        LOGGER.info(
            "Aggregation process started with process id %s",
            self.aggregation_process.pid,
        )
        while self.aggregation_process_alive_event.is_set() is False:
            try:
                event_data = self.event_data_queue.get(block=True, timeout=0.1)
                event_data_dict = (
                    self._convert_event_data_to_dict_for_rule_engine(
                        event_data
                    )
                )
                event_data_dict["event_data"] = asdict(event_data)
                self.aggregated_obs_state[
                    0
                ] = self.obs_state_aggregator.aggregate(event_data_dict)
                self.aggregate_update_event.set()

                if self.callback:
                    # Call the callback with aggregated obsState
                    self.callback(self.aggregated_obs_state[0])
                LOGGER.info(
                    "Aggregated ObsState %s", self.aggregated_obs_state
                )
            except queue.Empty:
                pass

    def _convert_event_data_to_dict_for_rule_engine(self, event_data):
        """Override this method in child classes
        :param event_data: Event data object contain data required for
        aggregation
        :type event_data: EventDataStorage
        :return: event data dict with all unique values required for obsState
         aggregation
        """
        raise NotImplementedError

    def start_aggregation_process(self):
        """Start aggregation process"""
        self.aggregation_process.start()

    def stop_aggregation_process(self):
        """Delete the running aggregation process"""
        LOGGER.debug("Stopping aggregation process")
        if self.aggregation_process.is_alive():
            self.aggregation_process_alive_event.set()
            self.aggregation_process.join()
        LOGGER.debug("Aggregation process stopped")


class AggregationProcessMid(AggregationProcess):
    """Aggregation Process for Mid"""

    def __init__(
        self,
        event_data_queue: Queue,
        aggregated_obs_state,
        aggregate_update_event,
        callback=None,
    ):
        """
        :param event_data_queue: Queue to track event data
        :type event_data_queue: multiprocessing queue
        :param aggregated_obs_state: Aggregated obsState updated after
        aggregation
        this is shared variable between process
        :type aggregated_obs_state: list
        :param callback: Callback method reference used when aggregated
        obsState is updated
        """
        super().__init__(
            event_data_queue,
            aggregated_obs_state,
            aggregate_update_event,
            callback,
        )
        self.obs_state_aggregator = ObsStateAggregator(OBS_STATE_RULES_MID)

    def _convert_event_data_to_dict_for_rule_engine(self, event_data) -> dict:
        """Extract all necessary data from event and create dictionary
        :param event_data: Event data object contain data required for
        aggregation
        :type event_data: EventDataStorage
        :return: event data dict with all unique values required for obsState
         aggregation
        """
        event_data_dict = {}
        all_obs_state = [
            obs_data.obs_state
            for device, obs_data in event_data.obs_state_data.items()
        ]
        event_data_dict["all_unique_obs_states"] = list(set(all_obs_state))

        all_pointing_state_data = [
            pointing_state_data.pointing_state
            for device, pointing_state_data in (
                event_data.pointing_state_data.items()
            )
        ]
        event_data_dict["all_unique_pointing_states"] = list(
            set(all_pointing_state_data)
        )

        all_dish_mode_data = [
            dish_mode_data.dish_mode
            for device, dish_mode_data in (event_data.dish_mode_data.items())
        ]
        event_data_dict["all_unique_dish_modes"] = list(
            set(all_dish_mode_data)
        )

        all_command_result_data = [
            command_result_data.result
            for device, command_result_data in (
                event_data.command_result_data.items()
            )
        ]
        event_data_dict["all_unique_command_results"] = list(
            set(all_command_result_data)
        )

        event_data_dict["command_in_progress"] = event_data.command_in_progress
        event_data_dict["command_timestamp"] = event_data.command_timestamp
        event_data_dict[
            "is_partial_configuration"
        ] = event_data.is_partial_configuration

        return event_data_dict


class AggregationProcessLow(AggregationProcess):
    """Aggregation Process for Low"""

    def __init__(
        self,
        event_data_queue: Queue,
        aggregated_obs_state,
        aggregate_update_event,
        callback=None,
    ):
        """
        :param event_data_queue: Queue to track event data
        :type event_data_queue: multiprocessing queue
        :param aggregated_obs_state: Aggregated obsState updated after
        aggregation
        this is shared variable between process
        :type aggregated_obs_state: list
        :param callback: Callback method reference used when aggregated
        obsState is updated
        """
        super().__init__(
            event_data_queue,
            aggregated_obs_state,
            aggregate_update_event,
            callback,
        )
        self.obs_state_aggregator = ObsStateAggregator(OBS_STATE_RULES_LOW)

    def _convert_event_data_to_dict_for_rule_engine(self, event_data) -> dict:
        """Extract all necessary data from event and create dictionary
        :param event_data: Event data object contain data required for
        aggregation
        :type event_data: EventDataStorage
        :return: event data dict with all unique values required for obsState
         aggregation
        """
        event_data_dict = {}
        all_obs_state = [
            obs_data.obs_state
            for device, obs_data in event_data.obs_state_data.items()
        ]
        event_data_dict["all_unique_obs_states"] = list(set(all_obs_state))

        all_command_result_data = [
            command_result_data.result
            for device, command_result_data in (
                event_data.command_result_data.items()
            )
        ]
        event_data_dict["all_unique_command_results"] = list(
            set(all_command_result_data)
        )

        event_data_dict["command_in_progress"] = event_data.command_in_progress
        event_data_dict["command_timestamp"] = event_data.command_timestamp

        return event_data_dict
