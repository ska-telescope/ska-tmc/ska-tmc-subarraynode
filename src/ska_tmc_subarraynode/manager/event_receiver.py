"""
This Module is used to Receive events from devices
"""

import copy
from datetime import datetime
from logging import Logger
from threading import Lock
from typing import TYPE_CHECKING, Callable, Dict, List, Union

import tango
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.v1.event_receiver import EventReceiver

from ska_tmc_subarraynode.model.input import InputParameterMid

if TYPE_CHECKING:
    from ska_tmc_subarraynode.manager import (
        SubarrayNodeComponentManagerLow,
        SubarrayNodeComponentManagerMid,
    )


class SubarrayNodeEventReceiver(EventReceiver):
    """
    The SubarrayNodeEventReceiver class has the
    responsibility to receive events
    from the sub devices managed by the Subarray node.

    The ComponentManager uses the handle events methods
    for the attribute of interest.
    For each of them a callback is defined.

    TBD: what about scalability? what if we have 1000 devices?

    """

    def __init__(
        self,
        component_manager,
        logger: Logger,
        max_workers: int = 1,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
    ):
        super().__init__(
            component_manager=component_manager,
            logger=logger,
            max_workers=max_workers,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
        )
        self._component_manager: Union[
            SubarrayNodeComponentManagerLow, SubarrayNodeComponentManagerMid
        ] = component_manager
        self.event_id_modification_lock: Lock = Lock()
        self.__device_attribute_event_ids: Dict[str, Dict[str, int]] = {}
        self.cm = self._component_manager
        self._event_enter_exit_time: List[datetime] = []

    @property
    def device_attribute_event_ids(self) -> Dict[str, Dict[str, int]]:
        """A property that stores the device names, the attributes they have
        subscribed to and the corresponding event ids."""
        return self.__device_attribute_event_ids

    def set_device_attribute_event_ids(
        self, dev_name: str, attribute_name: str, event_id: int
    ) -> None:
        """Stores the value for attribute event id.

        :param dev_name: Device name for which the event id is being stored.
        :type dev_name: `str`
        :param attribute_name: Attribute name for which the event id has to be
            stored.
        :type attribute_name: `str`
        :param event_id: Event id for the event subscription
        :type event_id: `int`

        :return: None
        """
        self._logger.debug(
            "Updating the device_attribute_event_ids dictionary with "
            + "following parameters: Device name - %s, attribute name - %s"
            + ", event id - %s",
            dev_name,
            attribute_name,
            event_id,
        )
        if not self.device_attribute_event_ids.get(dev_name):
            self.__device_attribute_event_ids[dev_name] = {}

        self.__device_attribute_event_ids[dev_name][attribute_name] = event_id

    def subscribe_events(
        self, dev_info: DeviceInfo, attribute_dictionary=None
    ) -> None:
        """Method for subscribing events"""

        try:
            proxy = self._dev_factory.get_device(dev_info.dev_name)
        except Exception as e:
            self._logger.exception(
                "Exception occurred while creating proxy for %s: %s",
                dev_info,
                e,
            )
        else:
            try:
                self.subscribe_to_event(
                    proxy, "healthState", self.handle_health_state_event
                )
                self.subscribe_to_event(
                    proxy, "State", self.handle_state_event
                )

                if (
                    "subarray" in dev_info.dev_name
                    and "leaf" not in dev_info.dev_name
                ):
                    if "sdp" in dev_info.dev_name:
                        self.subscribe_to_event(
                            proxy,
                            "receiveAddresses",
                            self.handle_receive_addresses_event,
                        )
                    if "low-mccs" in dev_info.dev_name:
                        self.subscribe_to_event(
                            proxy,
                            "assignedResources",
                            self.handle_assigned_resources_event,
                        )

                elif "leaf" in dev_info.dev_name:
                    self.subscribe_to_event(
                        proxy,
                        "longRunningCommandResult",
                        self.handle_lrcr_event,
                    )
                    self.subscribe_to_event(
                        proxy,
                        "isSubsystemAvailable",
                        self.handle_subsystem_availability_event,
                    )

                    if "csp" in dev_info.dev_name:
                        self.subscribe_to_event(
                            proxy,
                            "cspSubarrayObsState",
                            self.obs_state_event_callback,
                        )
                        self.subscribe_to_event(
                            proxy,
                            "cspSubarrayAdminMode",
                            self.admin_mode_event_callback,
                        )

                    if "sdp" in dev_info.dev_name:
                        self.subscribe_to_event(
                            proxy,
                            "sdpSubarrayObsState",
                            self.obs_state_event_callback,
                        )
                        self.subscribe_to_event(
                            proxy,
                            "sdpSubarrayAdminMode",
                            self.admin_mode_event_callback,
                        )

                    if "mccs" in dev_info.dev_name:
                        self.subscribe_to_event(
                            proxy,
                            "obsState",
                            self.obs_state_event_callback,
                        )
                        self.subscribe_to_event(
                            proxy,
                            "mccsSubarrayAdminMode",
                            self.admin_mode_event_callback,
                        )

                    if (
                        isinstance(
                            self._component_manager._input_parameter,
                            InputParameterMid,
                        )
                        and dev_info.dev_name
                        in self.cm.input_parameter.tmc_dish_ln_device_names
                    ):
                        self.subscribe_to_event(
                            proxy, "dishMode", self.handle_dish_mode_event
                        )
                        self.subscribe_to_event(
                            proxy,
                            "pointingState",
                            self.handle_pointing_state_event,
                        )

            except Exception as e:
                self._logger.exception(
                    "Events not working for device %s: %s", proxy.dev_name, e
                )

    def subscribe_to_event(
        self,
        device_proxy: tango.DeviceProxy,
        attribute_name: str,
        event_callback: Callable[[tango.EventData], None],
    ) -> None:
        """Subscribe to the attribute event for the device with provided
        callback function. If the event is already subscribed to, return. If
        not, add the entry to the device_attribute_event_ids dictionary, with
        the subscription id.

        :param device_proxy: Device proxy of the device for which event has to
            be subscribed to.
        :type device_proxy: `tango.DeviceProxy`

        :param attribute_name: Attribute name for which the event has to be
            subscribed to.
        :type attribute_name: `str`

        :param event_callback: Callback to be used for processing event data
        :type event_callback: `Callable[[tango.EventData], None]`

        :return: None
        """
        with self.event_id_modification_lock:
            if self.device_attribute_event_ids.get(
                device_proxy.dev_name(), {}
            ).get(attribute_name):
                return

            event_id = device_proxy.subscribe_event(
                attribute_name,
                tango.EventType.CHANGE_EVENT,
                event_callback,
                stateless=True,
            )

            self.set_device_attribute_event_ids(
                device_proxy.dev_name(), attribute_name, event_id
            )

    def unsubscribe_to_events_with_prefix(self, prefix: str) -> None:
        """Method for unsubscribing to events based on a prefix in device
        name."""
        with self.event_id_modification_lock:
            device_attribute_event_ids = copy.copy(
                self.device_attribute_event_ids
            )
            self._logger.debug(
                "The devices with their subscribed events are: %s",
                device_attribute_event_ids,
            )
            device_names_and_dicts: List[tuple[str, Dict[str, int]]] = [
                (device_name, value)
                for device_name, value in device_attribute_event_ids.items()
                if prefix in device_name
            ]
            for device_name, device_dict in device_names_and_dicts:
                for attribute, event_id in device_dict.items():
                    try:
                        self._logger.debug(
                            "Unsubscribing to the event for device: %s and "
                            + "attribute: %s with event id: %s",
                            device_name,
                            attribute,
                            event_id,
                        )
                        proxy = self._dev_factory.get_device(device_name)
                        proxy.unsubscribe_event(event_id)
                    except Exception as exception:
                        self._logger.exception(
                            "Event unsubscription failed for device: %s on "
                            + "attribute: %s, with exception: %s",
                            device_name,
                            attribute,
                            exception,
                        )

            # with self.event_id_modification_lock:
            for device_name in list(
                map(lambda device: device[0], device_names_and_dicts)
            ):
                self._logger.debug(
                    "Resetting device_attribute_event_ids dictionary entry"
                    + " for device: %s",
                    device_name,
                )
                self.device_attribute_event_ids[device_name] = {}

    def unsubscribe_dish_events(self) -> None:
        """Method for unsubscribing dish events"""
        if isinstance(
            self._component_manager.input_parameter, InputParameterMid
        ):
            dish_prefix = (
                self._component_manager.input_parameter.dish_master_identifier
            )
            self._logger.debug("Dish Prefix is: %s", dish_prefix)
            self.unsubscribe_to_events_with_prefix(dish_prefix)

    def unsubscribe_dish_leaf_events(self):
        """Method for unsubscribing dish leaf events."""
        if isinstance(
            self._component_manager.input_parameter, InputParameterMid
        ):
            dish_ln_prefix = (
                self._component_manager.input_parameter.dish_ln_prefix
            )
            self._logger.debug("DishLeafNode Prefix is: %s", dish_ln_prefix)
            self.unsubscribe_to_events_with_prefix(dish_ln_prefix)

    def obs_state_event_callback(self, event) -> None:
        """Put the obsState event received in a Queue for processing."""

        self.log_event_data(event, "obsState_Callback")
        self._component_manager.event_queues["obsState"].put(event)
        self.log_event_exit("obsState_Callback")

    def admin_mode_event_callback(self, event) -> None:
        """Put the adminMode event received in a Queue for processing"""
        self.log_event_data(event, "adminMode_Callback")
        self._component_manager.event_queues["adminMode"].put(event)
        self.log_event_exit("adminMode_Callback")

    def handle_dish_mode_event(self, event):
        """Method for handling dish mode event."""

        self.log_event_data(event, "dishMode_Callback")
        self._component_manager.event_queues["dishMode"].put(event)
        self.log_event_exit("dishMode_Callback")

    def handle_pointing_state_event(self, event):
        """Method for handling PointingState event."""

        self.log_event_data(event, "pointingState_Callback")
        self._component_manager.event_queues["pointingState"].put(event)
        self.log_event_exit("pointingState_Callback")

    def handle_receive_addresses_event(self, event):
        """Method for handling and receiving addresses events.
        and to subscribe to pointing_offsets of sdpqc
        """
        self._logger.info(
            "Handle receive_address_event with event %s and "
            " sdp_subarray %s",
            event.device.dev_name(),
            self._component_manager.get_sdp_subarray_dev_name(),
        )
        if not self.check_event_error(event, "ReceiveAddresses_Callback"):
            if (
                event.device.dev_name()
                in self._component_manager.get_sdp_subarray_dev_name()
            ) and ("mid" in event.device.dev_name()):
                self._component_manager.invoke_pointing_calibration(
                    event.attr_value.value
                )

            self._component_manager.update_receive_addresses(
                event.device.dev_name(), event.attr_value.value
            )

    def handle_assigned_resources_event(self, event):
        """Method for handling assigned resources events."""
        if not self.check_event_error(event, "AssignedResources_Callback"):
            self._component_manager.update_assigned_resources(
                event.device.dev_name(), event.attr_value.value
            )

    def handle_subsystem_availability_event(self, event_data):
        """Method for handling isSubarrayAvailable attribute events."""
        if not self.check_event_error(event_data, "Availability_Callback"):
            self._component_manager.update_subarray_availability_status(
                event_data.device.dev_name(), event_data.attr_value.value
            )

    def handle_lrcr_event(self, event):
        """Method for handling longRunningCommandResult events."""

        self.log_event_data(event, "longRunningCommandResult_Callback")
        self._component_manager.event_queues["longRunningCommandResult"].put(
            event
        )
        self.log_event_exit("longRunningCommandResult_Callback")

    def check_event_error(self, event: tango.EventData, callback: str):
        """Method for checking event error."""
        if event.err:
            error = event.errors[0]
            self._logger.error(
                "Error occurred on %s for device: %s - %s, %s",
                callback,
                event.device.dev_name(),
                error.reason,
                error.desc,
            )
            self._component_manager.update_event_failure(
                event.device.dev_name()
            )
            return True
        return False

    def log_event_data(
        self, event_data: tango.EventData, callback_name: str
    ) -> None:
        """Log the event data for later use."""
        if event_data.attr_value:
            attribute_name = event_data.attr_value.name
            attribute_value = event_data.attr_value.value
            reception_time: datetime = event_data.attr_value.time.todatetime()
            current_time = datetime.utcnow()
            self._event_enter_exit_time.append(current_time)
            current_time = current_time.strftime("%d/%m/%Y %H:%M:%S:%f")
            self._logger.debug(
                "Enter time for the callback: %s is %s. Event data is - "
                + "Attribute: %s, Value: %s, Reception time: %s",
                callback_name,
                current_time,
                attribute_name,
                attribute_value,
                reception_time.strftime("%d/%m/%Y %H:%M:%S:%f"),
            )

    def log_event_exit(self, callback_name: str) -> None:
        """Log the time of exiting the event."""
        self._event_enter_exit_time.append(datetime.utcnow())
        if len(self._event_enter_exit_time) == 2:
            time_diff = (
                self._event_enter_exit_time[1] - self._event_enter_exit_time[0]
            ).total_seconds()
        else:
            time_diff = datetime.utcnow().strftime("%d/%m/%Y %H:%M:%S:%f")
        self._event_enter_exit_time.clear()
        self._logger.debug(
            "Exit time for the callback: %s is %s", callback_name, time_diff
        )
