"""
This module is inherited from SubarrayNodeComponentManager.

It is component Manager for Mid Telecope.

It is provided for explanatory purposes, and to support testing of this
package.
"""
import json
import re
from datetime import datetime
from typing import List, Tuple, Union

import tango
from ska_control_model.faults import StateModelError
from ska_tango_base.commands import ResultCode
from ska_tmc_common import PointingState
from ska_tmc_common.device_info import DishDeviceInfo
from tango import Database

from ska_tmc_subarraynode.manager.aggregate_process import (
    AggregationProcessMid,
)
from ska_tmc_subarraynode.manager.aggregators import (
    HealthStateAggregatorMid,
    SubarrayAvailabilityAggregatorMid,
)
from ska_tmc_subarraynode.manager.subarray_node_component_manager import (
    SubarrayNodeComponentManager,
)
from ska_tmc_subarraynode.model.component import SubarrayComponent
from ska_tmc_subarraynode.utils.subarray_node_utils import get_dish_ln_adapters

SLEEP_TIME = 0.2
TIMEOUT = 80
# pylint:disable=use-implicit-booleaness-not-comparison


class SubarrayNodeComponentManagerMid(SubarrayNodeComponentManager):
    """
    Initialise a new ComponentManager instance for mid.
    """

    def __init__(
        self,
        op_state_model,
        obs_state_model,
        _command_tracker,
        _input_parameter,
        dev_family,
        logger=None,
        _component=None,
        _liveliness_probe: bool = True,
        _event_receiver: bool = True,
        component_state_changed_callback=None,
        communication_state_changed_callback=None,
        _update_device_callback=None,
        _update_subarray_health_state_callback=None,
        _update_assigned_resources_callback=None,
        _update_subarray_availability_status_callback=None,
        command_timeout: int = 30,
        abort_command_timeout: int = 40,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
        liveliness_check_period: int = 1,
        csp_assign_interface: str = "https://schema.skao.int/"
        + "ska-csp-assignresources/2.0",
        csp_scan_interface: str = "https://schema.skao.int/"
        + "ska-csp-scan/2.0",
        sdp_scan_interface: str = "https://schema.skao.int/"
        + "ska-sdp-scan/0.4",
    ):
        """
        Initialise a new ComponentManager instance for mid.

        :param op_state_model: the op state model used by this component
            manager
        :param logger: a logger for this component manager
        :param _component: allows setting of the component to be
            managed; for testing purposes only
        :param _input_parameter : specify input parameter for low and mid.
        :param _liveliness_probe:allows to enable/disable
            LivelinessProbe usage
        :param _event_receiver : allows to enable/disable EventReceiver usage
        :param max_workers: Optional. Maximum worker threads for
            monitoring purpose.
        :param proxy_timeout: Optional. Time period to wait for
            event and responses.
        :param event_subscription_check_period: (int) Time in seconds for sleep
            intervals in the event subsription thread.
        :param liveliness_check_period: (int) Period for the liveliness probe
            to monitor each device in a loop
        :param timeout : Optional. Time period to wait for
            intialization of adapter.
        """
        super().__init__(
            op_state_model,
            obs_state_model=obs_state_model,
            _command_tracker=_command_tracker,
            _input_parameter=_input_parameter,
            dev_family=dev_family,
            _event_receiver=_event_receiver,
            _liveliness_probe=_liveliness_probe,
            command_timeout=command_timeout,
            abort_command_timeout=abort_command_timeout,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
            _component=_component or SubarrayComponent(logger),
            logger=logger,
            component_state_changed_callback=(
                component_state_changed_callback
            ),
            _update_device_callback=_update_device_callback,
            _update_subarray_health_state_callback=(
                _update_subarray_health_state_callback
            ),
            _update_assigned_resources_callback=(
                _update_assigned_resources_callback
            ),
            _update_subarray_availability_status_callback=(
                _update_subarray_availability_status_callback
            ),
            csp_assign_interface=csp_assign_interface,
            csp_scan_interface=csp_scan_interface,
            sdp_scan_interface=sdp_scan_interface,
        )
        self.dev_family = dev_family
        self._pointing_calibrations = []
        self._sdpqc_keys_required_for_getting_fqdn = []
        self._is_kvalue_validation_failed = False
        self.trajectory_dish_adapter_list: list = []
        self.pointing_dish_adapter_list: list = []

        # start the aggregation process
        self.aggregation_process = AggregationProcessMid(
            self.event_data_queue,
            self.aggregated_obs_state,
            self.aggregate_value_update_event,
        )
        self.aggregation_process.start_aggregation_process()

    @property
    def kvalue_validation_failed(self) -> bool:
        "Gives the bool value for kvalue validation result"
        return self._is_kvalue_validation_failed

    @kvalue_validation_failed.setter
    def kvalue_validation_failed(self, value):
        self._is_kvalue_validation_failed = value
        # aggreate healthstate call
        self._aggregate_health_state()

    @property
    def pointing_calibrations(self) -> List[List[Union[str, float]]]:
        """Gives the current pointing calibrations informed by
        SDP Queue Connector device."""
        return self._pointing_calibrations

    @pointing_calibrations.setter
    def pointing_calibrations(
        self, pointing_calibrations: List[List[Union[str, float]]]
    ) -> None:
        """Update the pointing calibration data

        :param pointing_calibrations: The list containing dish id,
        cross_elevation, elevation offsets values.
        :pointing_calibrations dtype: list
        """
        with self.lock:
            self._pointing_calibrations = pointing_calibrations
            self.logger.info(
                "The updated pointing calibration values are: %s",
                self._pointing_calibrations,
            )

    def get_keys_required_for_getting_sdpqc_fqdn(self) -> List:
        """Returns the keys that are required to get sdpqc fqdn
        from dictionary received from SDP subarray receivedAddresses attribute
        at runtime
        """
        return self._sdpqc_keys_required_for_getting_fqdn

    def set_keys_required_for_getting_sdpqc_fqdn(
        self, set_the_keys: List
    ) -> None:
        """Set the values that are required to get sdpqc fqdn
        from dictionary received from SDP subarray receivedAddresses attribute
        at runtime
        """
        self._sdpqc_keys_required_for_getting_fqdn = set_the_keys

    def get_dish_dev_names(self) -> List[str]:
        """
        Return the names of the dishes assigned to Subarray
        """
        return self.input_parameter.dish_dev_names

    def get_tmc_dish_ln_device_names(self) -> List[str]:
        """
        Return the names of the dishes assigned to Subarray
        """
        return self.input_parameter.tmc_dish_ln_device_names

    def remove_dish_devices(self) -> None:
        """
        Removes dish devices from component from component
        """
        dishes_to_be_removed = []

        devices = self.devices
        for dev_info in devices:
            device_name = dev_info.dev_name
            self.logger.info(f"Device name is: {device_name}")
            if self.input_parameter.dish_ln_prefix in device_name:
                self.logger.info(
                    f"Added Dish Leaf Node {device_name} for removal."
                )
                dishes_to_be_removed.append(device_name)
            elif self.input_parameter.dish_master_identifier in device_name:
                self.logger.info(
                    f"Added Dish Master {device_name} for removal."
                )
                dishes_to_be_removed.append(device_name)
        if dishes_to_be_removed != []:
            self.logger.info(f"Removing Dish devices: {dishes_to_be_removed}")
            self.component.remove_device(dishes_to_be_removed)

    def set_dish_device_names(self, dish_dev_names: List) -> None:
        """
        Update the names of the dishes assigned to Subarray

        :param dish_dev_names: name of the dish devices
        :type list: list[str]
        """
        if dish_dev_names != []:
            for dish in dish_dev_names:
                if dish not in self.input_parameter.dish_dev_names:
                    self.input_parameter.dish_dev_names.append(dish)
        else:
            self.input_parameter.dish_dev_names = []
        self.update_input_parameter()

    def set_tmc_leaf_dish_device_names(self, tmc_dish_ln_device_names: List):
        """
        Update the names of the dish leaf nodes assigned to Subarray

        :param tmc_dish_ln_device_names: name of the dish devices
        :type list: list[str]
        """
        if tmc_dish_ln_device_names != []:
            for dish_leaf_node in tmc_dish_ln_device_names:
                if (
                    dish_leaf_node
                    not in self.input_parameter.tmc_dish_ln_device_names
                ):
                    self.input_parameter.tmc_dish_ln_device_names.append(
                        dish_leaf_node
                    )

                    self.event_data_manager.update_event_data(
                        device=dish_leaf_node,
                        data=PointingState.NONE,
                        received_timestamp=datetime.now(),
                        data_type="PointingState",
                    )
        else:
            self.input_parameter.tmc_dish_ln_device_names = []

    def set_trajectory_dish_device_names(self, dish_ln_names: List):
        """Update the trajectory dish device names
        :param dish_ln_names: dish leaf node name list
        :type dish_ln_names: list
        """
        if dish_ln_names:
            for dish_ln_name in dish_ln_names:
                if dish_ln_name not in self.trajectory_dish_adapter_list:
                    self.trajectory_dish_adapter_list.append(dish_ln_name)
        else:
            self.trajectory_dish_adapter_list = []

    def get_trajectory_dish_device_names(self):
        """Get Trajectory dish device names"""
        return self.trajectory_dish_adapter_list

    def get_dish_leaf_node_prefix(self) -> str:
        """
        Return the Dish Leaf Node prefix
        """
        return self.input_parameter.dish_ln_prefix

    def set_pattern_scan_data(self, group_list):
        """Set up Pattern scan data"""
        dish_ln_prefix = self.get_dish_leaf_node_prefix()
        dish_ln_fqdns = []
        for group_info in group_list:
            trajectory_info = group_info.get("trajectory")
            if trajectory_info:
                self.pattern_scan_data = {
                    "receptors": group_info["receptors"],
                    "attrs": trajectory_info["attrs"],
                }
                for receptor_id in group_info["receptors"]:
                    if receptor_id[:3] == "MKT":
                        continue
                    # Generate FQDNs for SKA dishes only as
                    # MeerKAT dishes are not present
                    dish_ln_fqdn = f"{dish_ln_prefix}{receptor_id[3:]}"
                    dish_ln_fqdns.append(dish_ln_fqdn)
                self.logger.info("Trajectory Dishes are %s", dish_ln_fqdns)
                self.set_trajectory_dish_device_names(dish_ln_fqdns)
                break

    def handle_mapping_scan_callback(self):
        """This callback is implemented to handle TMC internal
        obs state
        """
        if self.is_mapping_scan:
            self.scan_obj.stop_scan_timeout_timer()
            self.scan_obj.execute_mapping_scan()

    def add_device_to_lp(self, device_name: str) -> None:
        """
        Add device to the liveliness probe

        :param device_name: device name
        :type device_name: str
        """
        if device_name is None:
            return
        if ("sdp" in device_name.lower()) or ("csp" in device_name.lower()):
            self.add_similar_low_mid_device(device_name)

        else:
            if self.input_parameter.dish_master_identifier in device_name:
                dev_info = DishDeviceInfo(device_name, False)
                self.component.update_device(dev_info)
            if (
                self.input_parameter.dish_ln_prefix.lower()
                in device_name.lower()
            ):
                dev_info = DishDeviceInfo(device_name, False)
                try:
                    db = Database()
                    db_device_info = db.get_device_info(device_name)
                    if db_device_info.exported:
                        dev_info.device_availability = tango.DeviceProxy(
                            device_name
                        ).isSubsystemAvailable
                except Exception as exception:
                    self.logger.exception(exception)
                self.component.update_device(dev_info)
                if self.liveliness_probe:
                    self.liveliness_probe.add_device(device_name)

    def _aggregate_health_state(self) -> None:
        """
        Aggregates all health states
        and call the relative callback if available
        """
        self._subarray_health_state_aggregator = HealthStateAggregatorMid(
            self, self.logger
        )
        super()._aggregate_health_state()

    def get_devices_availability_dict(self):
        """Returns the dictionary with the devices and it's availabiltiy"""
        return self._subarray_availability_aggregator.devices_availability_dict

    def _aggregate_subarray_device_availability(self) -> None:
        """
        Aggregates subarrays availability
        and call the relative callback if available
        """

        if self._subarray_availability_aggregator is None:
            self._subarray_availability_aggregator = (
                SubarrayAvailabilityAggregatorMid(self, self.logger)
            )

        return super()._aggregate_subarray_device_availability()

    def clear_assigned_resources(self) -> None:
        """Removes Dish Leaf Node and Dish Master
        devices from Input Parameter"""
        # Remove devices from Liveliness Probe
        self.remove_devices_from_lp(self.input_parameter.dish_dev_names)
        self.remove_devices_from_lp(
            self.input_parameter.tmc_dish_ln_device_names
        )
        self.set_tmc_leaf_dish_device_names([])
        self.set_dish_device_names([])

    def clear_mapping_scan_data(self) -> None:
        """Clear mapping scan data"""
        self.logger.debug("clearing mapping scan data")
        self.is_mapping_scan = False
        self.event_data_manager.update_aggragation_queue()

    def stop_aggregation_process(self):
        """Stop aggregation process"""
        self.aggregation_process.stop_aggregation_process()

    def invoke_pointing_calibration(self, event_attribute_value) -> None:
        """This method get sdpQueueConnectorFqdn from event attribute value
        and set on dish leaf node
        :param event_attribute_value: receiveAddresses attribute value
        :type event_attribute_value: string
        """
        try:
            (
                scan_type,
                beam_id,
            ) = self.get_keys_required_for_getting_sdpqc_fqdn()
            if isinstance(event_attribute_value, str):
                receive_addresses = json.loads(event_attribute_value)
                sdpqc_attribute_fqdn = receive_addresses[scan_type][beam_id][
                    "pointing_cal"
                ]
                dish_adapters, errors = get_dish_ln_adapters(
                    self.input_parameter.tmc_dish_ln_device_names,
                    self.adapter_factory,
                    self.logger,
                )
                if errors:
                    self.logger.exception(
                        "Errors in creating adapter for %s", errors
                    )
                else:
                    for dish_leaf_node_adapter in dish_adapters:
                        dish_leaf_node_adapter.sdpQueueConnectorFqdn = (
                            sdpqc_attribute_fqdn
                        )
                        self.logger.info(
                            "SDP queue connector fqdn %s set on dish %s",
                            sdpqc_attribute_fqdn,
                            dish_leaf_node_adapter.dev_name,
                        )
        except Exception as exception:
            self.logger.warning(
                "Unexpected exception occurred while processing SDP queue "
                "connector FQDN. "
                "AttributeValue: %s, Exception: %s",
                event_attribute_value,
                exception,
            )

    def process_lrcrevent(
        self, dev_name: str, value: Tuple[str, str], timestamp: datetime
    ):
        """
        Process Long Running Command Result
        """
        unique_id = value[0]
        result_code, message = json.loads(value[1])

        command_name = re.findall(r"([A-Za-z]+)", unique_id)[0]
        self.logger.info("Command name is: %s", command_name)
        dish_ln_prefix = self.get_dish_leaf_node_prefix()
        self.logger.info(
            "self.command_in_progress is: %s", self.command_in_progress
        )

        self.logger.info("command_id %s", self.command_id)
        # Ensure that only the event for command in progress is processed.
        # For dish leaf node device, command name from event is
        # TrackStop in case of SubarrayNode End command. # In other cases,
        # command in progress and command name from event
        # are same
        if dish_ln_prefix in dev_name and self.command_in_progress == "End":
            if command_name != "TrackStop":
                return
        elif command_name != self.command_in_progress:
            self.logger.debug(
                "Ignoring event for command '%s' as no "
                "matching command is in progress.",
                command_name,
            )
            return

        if not value == ("", ""):
            value_to_be_stored = ([value[0]], value[1])
            self.event_data_manager.update_event_data(
                device=dev_name,
                data=value_to_be_stored,
                received_timestamp=timestamp,
                data_type="CommandResultData",
            )
        try:
            if not self.device_events.get(self.command_in_progress_id):
                self.device_events[self.command_in_progress_id] = {}
                self.error_dict[self.command_in_progress_id] = {}
            if not value[1]:
                # no instructions when no event
                # for longrunningcommandresult command tuple
                pass
            if unique_id.endswith(self.supported_commands):
                # Below statement will extract command name
                # from long-running command result

                if int(result_code) == ResultCode.OK and (
                    "csp" in dev_name
                    or "sdp" in dev_name
                    or self.input_parameter.dish_ln_prefix in dev_name
                ):
                    self.device_events[self.command_in_progress_id][
                        dev_name
                    ] = value[1]
                elif int(result_code) in [
                    ResultCode.REJECTED,
                    ResultCode.FAILED,
                    ResultCode.NOT_ALLOWED,
                ]:
                    self.logger.debug(
                        "Processing exception for "
                        + "command %s with id: %s "
                        + "and resultcode: %s",
                        command_name,
                        self.command_in_progress_id,
                        result_code,
                    )
                    if (
                        "sdp" in dev_name
                        or "csp" in dev_name
                        or self.input_parameter.dish_ln_prefix in dev_name
                    ):
                        self.error_dict[self.command_in_progress_id][
                            dev_name
                        ] = message
                        self.device_events[self.command_in_progress_id][
                            dev_name
                        ] = value[1]
                    self.logger.error(
                        "Exception occurred with value: %s "
                        + "for %s command for device: %s",
                        value,
                        command_name,
                        dev_name,
                    )
                    self.logger.debug(
                        "The updated Csp Sdp events " + "dictionary is: %s",
                        self.device_events,
                    )
        except StateModelError as state_model_error:
            # pylint: disable=used-before-assignment
            self.logger.error(
                "State Model Error occurred for "
                + "command id: %s and error: %s",
                self.command_in_progress_id,
                state_model_error,
            )

    def update_long_running_command_result(
        self, dev_name: str, value: Tuple[str, str], timestamp: datetime
    ) -> None:
        """Stores the longRunningCommandResult values for devices and Updates
        the long_running_result_callback if the required criteria is met (i.e
        received events from all the expected devices for a given command).

        :param dev_name: Device name for which the longRunningCommandResult
            event was received
        :type dev_name: `str`
        :param value: longRunningCommandResult event value
        :type value: `Tuple[str, str]`

        :returns: None
        """
        self.logger.info(
            "Received longRunningCommandResult event for"
            + "device: %s, with value: %s",
            dev_name,
            value,
        )

        with self.lock:
            if self._should_ignore_lrcr_event(value):
                return

            self.process_lrcrevent(dev_name, value, timestamp)
            command_including_dishes = [
                "Configure",
                "End",
                "Scan",
                "EndScan",
            ]
            if self.command_in_progress in command_including_dishes:
                if self.is_partial_configuration:
                    # considers only dish LRCR events received after partial
                    # configure command
                    device_events_len = len(self.assigned_resources)

                else:
                    # considers csp, sdp and dish LRCR events
                    device_events_len = 2 + len(self.assigned_resources)
            else:
                # considers only csp and sdp LRCR events
                device_events_len = 2
            if len(
                self.device_events.get(self.command_in_progress_id, [])
            ) == device_events_len and self.error_dict.get(
                self.command_in_progress_id
            ):
                exception_message = (
                    "Exception occurred on the following devices: "
                )
                for devname, error_value in sorted(
                    self.error_dict[self.command_in_progress_id].items()
                ):
                    exception_message += f"{devname}: {error_value}\n"
                self.logger.info(exception_message)

                self.long_running_result_callback(
                    self.command_id,
                    ResultCode.FAILED,
                    exception_msg=exception_message,
                )

                self.observable.notify_observers(command_exception=True)

            elif (
                len(self.device_events.get(self.command_in_progress_id, []))
                == device_events_len
            ):
                if self.command_in_progress == "Scan":
                    self.all_lrcr_ok = True

                    self.observable.notify_observers(
                        attribute_value_change=True
                    )

            self.logger.debug(
                "The Csp Sdp events dictionary is: %s", self.device_events
            )

    @property
    def configured_capabilities(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def deconfigure(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def obsreset(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def release(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def standby(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def start_communicating(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """

    def stop_communicating(self):
        """This method serves as a placeholder to address linting warnings or
        errors related to missing or unused functions. It is intentionally left
        empty to satisfy linting requirements without affecting the program's
        behavior.
        """
