"""
Enum Constant Values for Subarray Node
"""
from enum import IntEnum, unique


@unique
class PointingState(IntEnum):
    """
    An enumeration class representing the
    different pointing states of a device.
    """

    NONE = 0
    READY = 1
    SLEW = 2
    TRACK = 3
    SCAN = 4
    UNKNOWN = 5
