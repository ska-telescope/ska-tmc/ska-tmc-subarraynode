# Project makefile for a ska-tmc-subarraynode project. You should normally only need to modify
# PROJECT below.

# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST (=artefact.skao.int) and overwrites
# PROJECT to give a final Docker tag of
# artefact.skao.int/ska-telescope/ska-tmc-subarraynode

CAR_OCI_REGISTRY_HOST:=artefact.skao.int
PROJECT = ska-tmc-subarraynode
KUBE_APP = ska-tmc-subarraynode
TELESCOPE ?= SKA-mid
PYTHON_SWITCHES_FOR_FLAKE8=--ignore=W503,F401 --max-line-length=100
PYTHON_SWITCHES_FOR_PYLINT=--disable=W0612
TANGO_HOST ?= tango-databaseds:10000 ## TANGO_HOST connection to the Tango DS
PYTHON_LINE_LENGTH ?= 79
CI_PROJECT_PATH_SLUG ?= ska-tmc-subarraynode
CI_ENVIRONMENT_SLUG ?= ska-tmc-subarraynode
$(shell echo 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gilab_values.yaml)
CLUSTER_DOMAIN ?= cluster.local
XRAY_TEST_RESULT_FILE = "build/cucumber.json"
## override so that this picks up setup.cfg from the project root
PYTHON_TEST_FILE ?=

SKA_TANGO_OPERATOR = false
# Set the specific environment variables required for pytest
PYTHON_VARS_BEFORE_PYTEST ?= PYTHONPATH=.:./src TANGO_HOST=$(TANGO_HOST) CLUSTER_DOMAIN=$(CLUSTER_DOMAIN) TELESCOPE=$(TELESCOPE)

MARK ?= ## What -m opt to pass to pytest
EXIT_AT_FAIL = true
# run one test with FILE=acceptance/test_subarray_node.py::test_check_internal_model_according_to_the_tango_ecosystem_deployed
FILE ?= tests## A specific test file to pass to pytest
ADD_ARGS ?= ## Additional args to pass to pytest

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-tmc-subarraynode

# HELM_RELEASE is the release that all Kubernetes resources will be labelled
# with
HELM_RELEASE ?= test
HELM_CHARTS_TO_PUBLISH=

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART=test-parent
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/
K8S_CHARTS ?= ska-tmc-subarraynode test-parent## list of charts
K8S_CHART ?= $(HELM_CHART)
K8S_TIMEOUT ?= 600s

CI_REGISTRY ?= gitlab.com
CUSTOM_VALUES = --set tmc_subarray_node.subarray_node.image.tag=$(VERSION)
K8S_TEST_IMAGE_TO_TEST=$(CAR_OCI_REGISTRY_HOST)/$(PROJECT):$(VERSION)
ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set tmc_subarray_node.subarray_node.image.image=$(PROJECT) \
	--set tmc_subarray_node.subarray_node.image.registry=$(CI_REGISTRY)/ska-telescope/ska-tmc/$(PROJECT) \
	--set tmc_subarray_node.subarray_node.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/ska-tmc/$(PROJECT)/$(PROJECT):$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
endif

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
TARANTA ?= false
MINIKUBE ?= false ## Minikube or not
FAKE_DEVICES ?= true ## Install fake devices or not
COUNT ?= 1 ## Number of times the tests should run
PYTHON_TEST_COUNT ?= 1
ITANGO_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.9

# Test runner - run to completion job in K8s
# name of the pod running the k8s_tests
K8S_TEST_RUNNER = test-runner-$(HELM_RELEASE)
ADDMARK ?= #additional markers
# override for python-test - must not have the above --true-context
ifeq ($(MAKECMDGOALS),python-test)
ADD_ARGS +=  --forked --count=$(PYTHON_TEST_COUNT)
MARK = not post_deployment and not acceptance $(ADDMARK)
endif
ifeq ($(MAKECMDGOALS),k8s-test)
ADD_ARGS +=  --true-context --count=$(COUNT)
MARK = $(shell echo $(TELESCOPE) | sed s/-/_/) and (post_deployment or acceptance) $(ADDMARK)
endif

ifeq ($(EXIT_AT_FAIL),true)
ADD_ARGS += -x
endif

PYTHON_VARS_AFTER_PYTEST ?= -m '$(MARK)' $(ADD_ARGS) $(FILE)

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_HOST) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	--set tmc_subarray_node.telescope=$(TELESCOPE) \
	--set tmc_subarray_node.deviceServers.mocks.enabled=$(FAKE_DEVICES) \
	--set ska-taranta.enabled=$(TARANTA) \
	--set global.exposeAllDS=false \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set global.operator=$(SKA_TANGO_OPERATOR) \
	$(CUSTOM_VALUES)

K8S_TEST_TEST_COMMAND = $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) \
						pytest \
						$(PYTHON_VARS_AFTER_PYTEST) ./tests \
						| tee pytest.stdout
-include .make/k8s.mk
-include .make/python.mk
-include .make/helm.mk
-include .make/oci.mk
-include .make/base.mk
-include .make/xray.mk
-include PrivateRules.mak

# flag this up for the oneshot /Dockerfile


OCI_IMAGES=ska-tmc-subarraynode

clean:
	@rm -rf .coverage .eggs .pytest_cache build */__pycache__ */*/__pycache__ */*/*/__pycache__ */*/*/*/__pycache__ charts/ska-tmc-subarraynode/charts \
			charts/build charts/test-parent/charts charts/ska-tmc-subarraynode/Chart.lock charts/test-parent/Chart.lock code-coverage \
			tests/.pytest_cache

unit-test: python-test

PYTHON_BUILD_TYPE = non_tag_setup

test-requirements:
	@poetry export --without-hashes --with dev --format requirements.txt --output tests/requirements.txt

k8s-pre-test: python-pre-test test-requirements

requirements: ## Install Dependencies
	poetry install
cred:
	make k8s-namespace
	curl -s https://gitlab.com/ska-telescope/templates-repository/-/raw/master/scripts/namespace_auth.sh | bash -s $(SERVICE_ACCOUNT) $(KUBE_NAMESPACE) || true

# .PHONY is additive
.PHONY: unit-test